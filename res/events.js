module.exports = [{
	"title":"King Cannabis Expo",
	"description":"King Cannabis is a two-day seed to sale expo. Meet producers, processors, greenhouse manufactures, security companies, investors, marketing groups and the numerous other b2b businesses from around the globe that support the marijuana industry. It’s time to grow your business! Get your exhibitor booth today, ask us about our early bird specials. Spokane County since 2014 has done over 260 million in Marijuana sales! And the state has done well over 1 Billion in sales.",
	"dates": ["09-29-17", "09-30-17"],
	"venue" : "Spokane Convention Center"
},
{
	"title":"M.J.A.Y",
	"description":"M.J.A.C is the premier cannabis conference for retail investors brought to you by InvestorsHub. A live two day cannabis investing symposium with cannabis industry exhibitors, key note and guest speakers, industry expert panels and educational workshops. The future of the legal cannabis industry is looking bright and with that, new opportunities for investors are appearing daily. m.j.a.c 2017 is bringing together industry giants from around the world to exhibit their business, meet qualified potential investors and market themselves on the world’s most significant retail investor network… InvestorsHub! m.j.a.c 2017 will showcase the industry’s very best opportunities for investment. The best minds in the market will educate and advocate for this unique and rapidly growing sector. We will look at the latest amendments in state law for both medical and recreational use and our expert panels will answer the questions and voice their opinions on the current state and potential future of this budding industry. Exhibitors and experts from the Hemp, CBD and Wellness sphere will also be there. ",
	"dates": ["09-2-17", "09-03-17", "09-04-17"],
	"venue" : "JW Marriot Los Angeles 900 W.Olympic Blvd.  Los Angeles, CA 90015 United States"
}, 
{
	"title":"M.J.A.D",
	"description":"M.J.A.C is the premier cannabis conference for retail investors brought to you by InvestorsHub. A live two day cannabis investing symposium with cannabis industry exhibitors, key note and guest speakers, industry expert panels and educational workshops. The future of the legal cannabis industry is looking bright and with that, new opportunities for investors are appearing daily. m.j.a.c 2017 is bringing together industry giants from around the world to exhibit their business, meet qualified potential investors and market themselves on the world’s most significant retail investor network… InvestorsHub! m.j.a.c 2017 will showcase the industry’s very best opportunities for investment. The best minds in the market will educate and advocate for this unique and rapidly growing sector. We will look at the latest amendments in state law for both medical and recreational use and our expert panels will answer the questions and voice their opinions on the current state and potential future of this budding industry. Exhibitors and experts from the Hemp, CBD and Wellness sphere will also be there. ",
	"dates": ["09-2-17", "09-03-17", "09-04-17"],
	"venue" : "JW Marriot Los Angeles 900 W.Olympic Blvd.  Los Angeles, CA 90015 United States"
}]
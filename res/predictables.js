module.exports = {
	'hi': function(req,res) {
    	res.say('Hello buddy!').shouldEndSession(true);
	},
	'hello': function(req,res) {
    	res.say('Hello buddy!').shouldEndSession(true);
	},
	'where do babies come from': function(req,res) {
    	res.say('Dude. Seriously!').shouldEndSession(true);
	}
};

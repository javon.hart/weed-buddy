module.exports = `
When invoking the skill remember to say my name. Weed Buddy<break time="2000ms"/>
At any time you can say, alexa, stop. <break time="800ms"/>
For help say, help <break time="800ms"/>
To search, For example you could say. Search purple haze<break time="500ms"/>\n
For PotCoin price, Ask Weed Buddy for pot coin price.<break time="500ms"/>\n
For flavors, For example you'd say. The flavor of purple haze.<break time="500ms"/>\n
To list all recipes say, list all recipes.<break time="500ms"/>\n
For the most popular strain you'd say. Alexa, Ask Weed Buddy for The most popular strain.<break time="500ms"/>\n
For strain benefits, for example say. Alexa, Ask Weed Buddy for The benefit of White Widow.<break time="500ms"/>\n
For recipes, For example say. Alexa, Ask Weed Buddy how to make brownies.<break time="500ms"/>\n
For all recipes say, list all recipes.<break time="500ms"/>\n
For jokes say, tell me a joke.<break time="500ms"/>\n
For facts say, tell me a fact.<break time="500ms"/>\n
For all strains say, list all strains.<break time="500ms"/>\n
For all hybrids say, list all hybrids.<break time="500ms"/>\n
For all sativas say, list all sativas.<break time="500ms"/>\n
For a random list of strains say, list random.<break time="500ms"/>\n
For a wake and bake suggestion say, what should I wake and bake\n
For other suggestions. For example, say Search strains good for pain<break time="500ms"/>\n
say, list all symptoms for more categories.<break time="500ms"/>\n
To search by flavor. For example, say Alexa, Ask Weed Buddy to Search berry flavored strains<break time="500ms"/>\n
For all flavors say, list all flavors.<break time="500ms"/>\n
To search by effect. For example, say Alexa, Ask Weed Buddy to Search strains with euphoric effects<break time="500ms"/>\n
For all effects say, list all effects.<break time="500ms"/>\n
\n
`.split('\n').filter(function(it){ return it.length > 1 })

// Tell Weed Buddy to list effects \n
// Tell Weed Buddy to list flavors \n
// Tell Weed Buddy to list recipes \n
// Ask Weed Buddy if (d) is open\n
// Ask Weed Buddy if (d) is closed\n
// Ask Weed Buddy if (d) has (x) in stock\n
// Ask Weed Buddy how much is (x) at (d)\n
// Ask Weed Buddy if (d) delivers\n
// Ask Weed Buddy to tell me more about (d) \n
// Ask Weed Buddy for the word of the day \n
// Ask Weed Buddy for the value of PotCoin \n
// Ask Weed Buddy for deals at (d) \n
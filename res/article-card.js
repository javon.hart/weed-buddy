module.exports = [
   {
        type: "Standard",
        title: "Hemp Fact - From Ganjily",
        text: 'Annually, 1 acre of hemp will produce as much fiber as 2 to 3 acres of cotton.',
        image: {
            smallImageUrl: "https://i1.wp.com/ganjly.com/wp-content/uploads/2017/02/business.png?resize=413%2C500&ssl=1",
        }
    },
   {
        type: "Standard",
        title: "Canna-Business News - From Ganjily",
        text: 'With demand rising and business booming, lawful canna-businesses are investing in creating new products and services.',
        image: {
            smallImageUrl: "https://i1.wp.com/ganjly.com/wp-content/uploads/2017/02/business.png?resize=413%2C500&ssl=1",
        }
    },
   {
        type: "Standard",
        title: "Hemp Fact - From Ganjily",
        text: 'On an annual basis, 1 acre of hemp will produce as much paper as 2 to 4 acres of trees.',
        image: {
            smallImageUrl: "https://i1.wp.com/ganjly.com/wp-content/uploads/2017/02/business.png?resize=413%2C500&ssl=1",
        }
    },
   {
        type: "Standard",
        title: "Cannabis Trends - From Ganjily",
        text: 'After several decades in which only high-THC cannabis has been available, CBD-rich strains are now being grown by and for patients.',
        image: {
            smallImageUrl: "https://i1.wp.com/ganjly.com/wp-content/uploads/2017/02/business.png?resize=413%2C500&ssl=1",
        }
    },
   {
        type: "Standard",
        title: "Cannabis Law - From Ganjily",
        text: 'Recreational marijuana is officially legal in Maine. Adults 21 years and older can now use and possess two-and-a-half ounces of weed.',
        image: {
            smallImageUrl: "https://i1.wp.com/ganjly.com/wp-content/uploads/2017/02/business.png?resize=413%2C500&ssl=1",
        }
    },


];
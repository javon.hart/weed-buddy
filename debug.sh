#!/bin/bash

if [ "$ENABLE_DEBUG" == "true" ]; then
	echo "Starting with debugger on port $AUX_PORT"
	exec with_ngrok node --debug=$AUX_PORT app.js
else
	echo "Starting without debugger"
	exec node app.js
fi

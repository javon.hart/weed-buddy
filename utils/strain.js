'use strict';
let Strains = require('../res/strains'),
    filters = require('./filters'),
    _ = require('underscore');
    
class StrainUtil {
    static name(q) {
        return filters.name(q);
    }

    static all() {
        return Strains;
    }

    static getStrain(id) {
        return _.findWhere(Strains, {
            name: id
        });
    }
    static hasStrain(id) {

        let strain = _.findWhere(Strains, {
            name: id
        });
        return strain ? true : false;
    }
    static allStrains() {
        return _.uniq(_.pluck(Strains, "name"));
    }
    static strains(list) {
        let count = 0,
            numKeys = 0,
            chain = [],
            index = {};
        _.each(list, function(o) {
            count += 1;
            chain.push(o);
            if (count == 15) {
                index[numKeys] = chain;
                chain = [];
                count = 0;
                numKeys += 1;
            }
        });
        if (chain.length > 0) {
            index[numKeys] = chain;
        }
        return index;
    }
    static listOfStrains(q) {
        q = q[0].toUpperCase() + q.substring(1);
        let strains = _.filter(Strains, {
            type: q
        });
        return _.uniq(_.pluck(strains, "name"));
    }

    static match(test) {
        let match;
        let all = Strains.map((strain) => { strain.name })
        for (let i = 0; i < all.length; i++) {
            let x = all[i].toLowerCase(),
            control = test.toLowerCase();
            if (control.indexOf(x) != -1) {
                match = (x);
                break;
            }
        }
        return match || test;
    }
    static byIncludes(property, match) {
        match = match.toLowerCase();
        let results = _.filter(Strains, (strain) => {
            return strain[property] && strain[property].toLowerCase().includes(match);
        });
        return _.pluck(results, "name");
    }

    static suggest(list) {
       let arr = [], out = '';
       for(let i = 0; i <= 25; i++) {
            arr.push(_.shuffle(list).pop());
       }
       arr = _.uniq(arr);
       _.each(arr,function(o){
            out = out.concat(" ").concat(o).concat(',');
       });
       return out.slice(0, out.length-1);
    }

    static colloquial(str) {
        let isms = {};
        isms.spirits = 'uplifted';
        isms.sleep = 'sleepy';
        isms.relaxation = 'relaxed';
        isms.focus = 'focused';
        isms.euphoria = 'euphoric';
        isms.creativity = 'creative';
        isms.energy = 'energetic';
        isms.happiness = 'happy';
        isms.hunger = 'hungry';
        isms.paranoia = 'paranoid';
        isms.anxiety = 'anxious';
        isms.apetite = "loss of apetite";
        isms.depressed = "depression";
        isms.dizzyness = "depression";
        isms["dry mouth"] = "mouth";
        isms["dry eye"] = "eye";
        isms["muscle spasms"] = "muscle";
        isms["dry eye"] = "eye";
        isms["dry eyes"] = "eye";
        isms["lack of sleep"] = "Lackof";
        let current = isms[str] || str;
        return current;
    }


}

module.exports = StrainUtil;

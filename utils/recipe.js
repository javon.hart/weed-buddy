'use strict';
let Recipes = require('../res/all_recipes'),
Core = require('../utils/core'),
LogUtil = require('../utils/logger'),
_ = require('underscore'),
filters = require('../utils/filters'),
d = require('didyoumean');
let nameService;
let Name = require('../models/name');
try {
    Name.findOne({}, function(err, id) {
        if(!id) {
            nameService = false;
        } else {
            nameService = id.index;
        }
    });
} catch (e) {
    console.log(e);
}
let allNames = require('../res/recipe_names');

    
class RecipeUtil {
    static getRecipe(id) {
        return _.findWhere(Recipes, {
            title: id
        });
    }
    static hasRecipe(id) {
        let recipe = _.findWhere(Recipes, {
            title: id
        });
        return recipe ? true : false;
    }
    static allRecipes() {
        return allNames;
    }


    static recipes(list) {
        let count = 0,
            numKeys = 0,
            chain = [],
            index = {};
        _.each(list, function(o) {
            count += 1;
            chain.push(o);
            if (count == 50) {
                index[numKeys] = chain;
                chain = [];
                count = 0;
                numKeys += 1;
            }
        });
        if (chain.length > 0) {
            numKeys += 1;
            index[numKeys] = chain;
        }
        return index;
    }
    static listOfRecipes() {
        return _.uniq(_.pluck(Recipes, "title"));
    }
    static main(r){
        let recipe = RecipeUtil.getRecipe(r);
        LogUtil.log(recipe);
        let success = ['Fantastic Idea!','Good Choice!', 'Yummy choice!', 'Scrumptious!', 'Exquisite choice', 'Tasty!', 'Delish!', 'Mouthwatering!', 'Nice!'];
        let speech = _.shuffle(success).pop();
        
        speech = speech
        .concat('<break time="1000ms"/>')
        .concat(recipe.title)
        .concat('<break time="1000ms"/>');

        if(recipe.ingredients.length) {
            speech = speech.concat(_.shuffle(['this recipe calls for.','to make this you\'ll need.','ingredients.', 'collect the following ingredients.']).pop());
            _.each(recipe.ingredients,function(o){
                let ingredient = filters.ingredient(o);
                speech = speech.concat('<break time="500ms"/>');
                speech = speech.concat(ingredient);
            });            
        }
        speech = speech.concat(_.shuffle(['<break time="700ms"/>Let\'s begin!','<break time="700ms"/>Let\'s get started!','<break time="700ms"/>Ready? Let\'s cook!']).pop())
        .concat(recipe.directions)
        .concat(_.shuffle(['<break time="400ms"/>All Done! like, share, enjoy!','<break time="400ms"/>That does it, enjoy!','<break time="400ms"/>I hope you enjoy!']).pop());
        return speech;
    }
    static getRealRecipe(r) {
        r = r.trim();
        // Title Case
        r = Core.capitalize(r);
        // check recipe garbled text for closest all recipe match via didyoumean
        r = (d(r, allNames) || r);
        // now check recipe against nameservice for matches
        return r; // probably need it's own name service
    }
    static randomName() {
        return _.shuffle(Recipes).pop().title;
    }
    static cardString(r) {
        let out = "";
        let ingredients = r.ingredients;
        _.each(ingredients, function(it){
            out = out.concat(it).concat('\n');
        });
        return out.concat(r.directions);
    }
}

module.exports = RecipeUtil;

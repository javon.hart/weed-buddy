let _ = require('underscore');
let Session = require('./session');
User = require('../models/user');

// Play behavior types: ENQUEUE, REPLACE_ALL, REPLACE_ENQUEUED
// Play behavior types: CLEAR_ENQUEUED, CLEAR_ALL

class AudioPlayer {

    static play(req, response, url, playBehavior, expectedToken, offset) {
        try {

            let stream = {
                url: url,
                token: _.random(1000000, 9999999),
                offsetInMilliseconds: offset || 0
            };

            response.audioPlayerPlayStream(playBehavior || "REPLACE_ALL", stream);
            User.findOne({ amzn_id: req.userId }, function(err,user){
                user.streamUrl = stream.url;
                user.streamToken = stream.token;
                user.lastPlaybackStart = new Date().getTime();
                user.save()
            })
            return stream.token;
        }
        catch (e) {
            console.log(e)
        }

    }

    static clearQueue(response, clearBehavior) {
        response.audioPlayerClearQueue(clearBehavior);
    }

    static resume(req, res, user) {
        try {
            console.log(req)
            let currentUser = user;
            if(currentUser) {
                res.audioPlayerPlayStream('REPLACE_ALL', {
                  'url': currentUser.streamUrl,
                  'token': currentUser.streamToken,
                  'offsetInMilliseconds': currentUser.lastPlaybackStop - currentUser.lastPlaybackStart
                });
                res.send();
            }

        } catch (e) {
            console.log(e)
        }

    }

    static pause(req,res) {
       try{
            res.audioPlayerStop();
            res.send();
            User.findOne({ amzn_id: req.userId }, function(err,user){
               user.lastPlaybackStop = new Date().getTime();
               user.save()
             })

       } catch(e) {
        console.log(e)
       }

    }

}

module.exports = AudioPlayer;

let _ = require("underscore"),    
Community = require("../models/community").community,
Twit = require("twit"), 
T = new Twit({
    consumer_key: "PmU1y3YKkBxbbx8NqKg22p5Pb",
    consumer_secret: "q08ewyexJj6mBvMFTqgEm6cWuznnsNHwYcFR4wUHExrJzSxAXL",
    app_only_auth:  true,
    timeout_ms: 60*1000,
});

function reduce(array) {
   let out = "";
   _.each(array, function(str){
        str = str.replace(/(?:https?|ftp):\/\/[\n\S]+/g, "");
        str = str.replace(/\//g, " ");
        out +=  str.replace("#weedbuddy", "<break time='500ms'/> ").replace(/#/g, "<break time='180ms'/>");
   });
   return out;
}

function getCommunityPost(req,res) {
    Community.findOne({}, {}, {
        sort: {
            "created_at": -1
        }
    }, function(err, post) {
        res.json(post);
    });
}

function updateCommunity(req,res) {
	Community.remove({}, function(err) {
        if (err) {
            console.log(err);
        }
    });
    T.get("search/tweets", {
        q: "#weedbuddy since:2016-12-19",
        count: 100
    }, function(err, data) {
        data = _.where(data.statuses, function(o) {
            return o.user.screen_name == "AlexaWeedBuddy" || "GoogleWeedBuddy";
        });
        let text = _.uniq(_.pluck(data, "text"));
        let json = JSON.stringify(reduce(text));
        json = json.replace(/RT @AlexaWeedBuddy:/g, " ");
        let community = new Community();
        community.text = json;
        community.created_at = new Date();
        community.save(function(err) {
            if (err) {
                res.json(500);
            }
            res.json(200);
        });
    });
}

class Twitter {
    static update(req,res) {
        updateCommunity(req,res);
    }
    static post(req,res) {
    	getCommunityPost(req,res);
    }
}

module.exports = Twitter;

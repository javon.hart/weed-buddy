'use strict';
let LogUtil = require('../utils/logger'),
commands = require('../res/commands'),
_ = require('underscore');

class CommandsUtil {

    static commands(list) {
        let count = 0,
            numKeys = 0,
            chain = [],
            index = {};
        _.each(list, function(o) {
            count += 1;
            chain.push(o);
            if (count == 50) {
                index[numKeys] = chain;
                chain = [];
                count = 0;
                numKeys += 1;
            }
        });
        if (chain.length > 0) {
            index[numKeys] = chain;
        }
        return index;
    }
    static listOfCommands() {
        return commands;
    }
}

module.exports = CommandsUtil;



'use strict';

let Session = require('../utils/session'),
    _ = require('underscore');

class UserUtil {
    static allUsers(session) {
        return session.session().getStore()['currentUsers'];
    }
    static closestUsers(address, session) {
        console.log('closestUsers');
        console.log(session.session().getStore())
        let users = session.session().getStore()['localUsers'];
        let localUsers = users.filter(function(item){
            return item.address.postalCode == address.postalCode || item.address.city == address.city || item.address.districtOrCounty == address.districtOrCounty
        });
        return localUsers;
    }
    static getUser(id, session) {
        let users = this.allUsers(session);
        return _.filter(users, function(user) {
            return user.amzn_id == id;
        }).shift();
    }
    static isPotentialConnection(user,buddy) {
        if(buddy.potentials.includes(user.amzn_id)) {
            return true;
        }
        return false;
    }
    static getMatches(currentUser, session) {
        let localPeople = this.closestUsers(currentUser.address, session);
        return _.filter(localPeople, function(person) {
            return person.myAge >= currentUser.minimumAge
                && person.myAge <= currentUser.maximumAge
                && person.gender
                && person.gender.toLowerCase() == currentUser.genderBuddy.toLowerCase()
                && !(currentUser.potentials.includes(person.amzn_id))// recently passed
                && !(currentUser.passed.includes(person.amzn_id))// recently passed
            // && currentUser.connections.indexOf(person._id) == 0 // is already a connection
        }).slice(0,20).map(function(item){
            return { name: item.name, amzn_id: item.amzn_id }
        });
    }
}

module.exports = UserUtil;

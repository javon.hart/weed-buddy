'use strict';
let LogUtil = require('../utils/logger'),
symptoms = require('../res/benefits'),
_ = require('underscore');

class SymptomsUtil {

    static symptoms(list) {
        let count = 0,
            numKeys = 0,
            chain = [],
            index = {};
        _.each(list, function(o) {
            count += 1;
            chain.push(o);
            if (count == 50) {
                index[numKeys] = chain;
                chain = [];
                count = 0;
                numKeys += 1;
            }
        });
        if (chain.length > 0) {
            index[numKeys] = chain;
        }
        return index;
    }
    static listOfSymptoms() {
        return symptoms;
    }
}

module.exports = SymptomsUtil;



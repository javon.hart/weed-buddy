class Logger {
    static pre(request,response,type) {
        console.log( '\n   /**  \n'  + 
        '    * Pre-Request Logger  \n'  + 
        '    * Request Type type  \n'  + 
        '    * @logger  \n'  + 
        '    * @time '  +  (new Date()) + '\n' + 
        '   */  \n');  
        console.log(type);
        console.log( '\n   /**  \n'  + 
        '    * Pre-Request Logger  \n'  + 
        '    * Request Object request  \n'  + 
        '    * @logger  \n'  + 
        '   */  \n');  
        console.log(request);
        console.log( '\n   /**  \n'  + 
        '    * Pre-Request Logger  \n'  + 
        '    * Response Object response  \n'  + 
        '    * @logger  \n'  + 
        '   */  \n');  
        console.log(response);
    }
    static log(v) {
        console.log( '\n/**\n' + 
        '* Info Logger\n' + 
        '* @logger  \n'  + 
        '* @time '  +  (new Date()) + '\n' + 
        '*/\n');
        console.log(v);
    }

}

module.exports = Logger;
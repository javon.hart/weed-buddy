let Logger = require('./logger');
let _ = require('underscore');
let names;
let ns = require('../res/all_names')

let all =  require('../res/all_strains');
let Core =  require('../utils/core');
let Session =  require('../utils/session');
let d = require('didyoumean');


// clean a strings know issues i.e effects context
function clean(speech) {
	let out = speech.replace('Lackof', 'Lack of sleep');
	return out;
}

// add and before the last speech item i.e effects context.
function and(list) {
	let speech = "";
	_.each(list, function(o,i){
		if(i==list.length-1) {
			speech += ' and ' + o;
		} else {
			speech += " " + o + ",";
		}
	});
	return speech;
}

function filter(speech) {
	let list = speech.match(/[A-Z][a-z]+/g);
	let res = and(list);
	res = clean(res);	
	return res;
}

function effects(item) {
	let speech = filter(item);
	speech = speech.match(/[A-Z][a-z]+/g);
 	
 	let relaxation = _.indexOf(speech, 'Relaxed');
 	if(relaxation!=-1) speech[relaxation] = 'Relaxation';
 	

	let sleepyness = _.indexOf(speech, 'Sleepy');
 	if(sleepyness!=-1) speech[sleepyness] = 'Sleepyness'; 	

	let happiness = _.indexOf(speech, 'Happy');
 	if(happiness!=-1) speech[happiness] = 'Happiness'; 	

	let euphoria = _.indexOf(speech, 'Euphoric');
 	if(euphoria!=-1) speech[euphoria] = 'Euphoria'; 	

	let stimulated = _.indexOf(speech, 'Uplifted');
 	if(stimulated!=-1) speech[stimulated] = 'Stimulation'; 	

 	speech = and(speech);
	Logger.log(speech);
	return speech;
}

function medical(item) {
	let speech = filter(item);
	Logger.log(speech);
	return speech;
}

function flavor(item) {
	let speech = filter(item);
	Logger.log(speech);
	return speech;
}

// name service filter
function name(data, app) {
	// let nameService = app.session().getStore()['nameService'];
	// let nameService = undefined
    var clean = data
    .replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) // title case
    out =  ns[clean] || clean
    console.log(out)
    return out.replace(/Og$/,'OG') // Og filter 
    .replace(/^Og/,'OG') // Og filter 
    .replace(/\sOf\s/,' of ') // of filter
    .replace(/\sIn\s/,' in '); // of filter
  //   // The following names brake mongodb key rules.
  //   let addNames = {
  //       'G. 13': 'G13',
		// "Eletric Women G.": "Electric Lemon G",
		// "File G.": "Fire OG",
		// "Help K. Ultra": "MK Ultra",
		// "Mr. Nice Guy": "Mr. Nice",
		// "Mr. Nice Guy Kis": "Mr. Nice",
		// "$100 Og": "$100 OG",
		// "Help K. Ultra": "MK Ultra",
		// "Hundred Dollar Og": "$100 OG",
		// "G. 13 Hayes":"G13 Haze",
		// "M. 39":"M-39",
		// "Lemon G.":"Lemon G",
		// "Cheerio G.":"Cherry OG",
		// "A. Train":"A-Train",
		// "B. 52":"B-52",
		// "K. Train":"K-Train",
  //   };
  //   if(!nameService) { // if no name service exists just use previous names
  //       names = addNames; 
  //   } else {
  //       names = Object.assign(nameService,addNames); // or else blend lists together
  //   }
  //   if(names[data]) return names[data]; // if a correct match is found for a broken name return it
  //   let out = d(data,all) || data; // make final assumptions about name corrections.
  //   return out;
}

function ingredient(r) {
	let numbers = r.match(/[1-9][0-9]*(?:\/[1-9][0-9])*/g);
	if(numbers) numbers = parseInt(numbers[0]) > 1;
	if(r.indexOf('tbsp')!=-1){
		if(numbers){ r = r.replace('tbsp', 'tablespoons'); }
		else {
			r = r.replace('tbsp', 'tablespoon');
		};
	}
	if(r.indexOf('tsp')!=-1){
		if(numbers){ r = r.replace('tsp', 'teaspoons'); }
		else {
			r = r.replace('tbsp', 'tablespoon');
		};
	}
	r = r
	.replace('lb', 'pound')
	.replace('oz', 'ounce');
	let paren = r.indexOf('(');
	if(paren != -1) r = Core.insert(r, paren,'<break time="100ms"/>');
	return r;
}

exports.ingredient = ingredient;
function intent(q) {
	q = q.toLowerCase();
	let intent = undefined;
	if(q.indexOf('recipe')!=-1) intent = 'recipeIntent';
	if(q.indexOf('find')!=-1) intent = 'searchIntent';
	if(q.indexOf('describe')!=-1) intent = 'searchIntent';
	if(q.indexOf('benefit')!=-1) intent = 'medicalIntent';
	if(q.indexOf('business')!=-1) intent = 'businessIntent'; 
	if(q.indexOf('community')!=-1) intent = 'communityIntent'; 
	if(q.indexOf('effect')!=-1) intent = 'effectsIntent'; 
	if(q.indexOf('fact')!=-1) intent = 'factIntent'; 
	if(q.indexOf('flavor')!=-1) intent = 'flavorIntent'; 
	if(q.indexOf('help')!=-1) intent = 'helpIntent'; 
	if(q.indexOf('joke')!=-1) intent = 'jokeIntent'; 
	if(q.indexOf('lucky')!=-1) intent = 'randomIntent'; 
	if(q.indexOf('news')!=-1) intent = 'communityIntent'; 
	if(q.indexOf('taste')!=-1) intent = 'flavorIntent'; 
	if(q.indexOf('type')!=-1) intent = 'typeIntent'; 
	if(q.indexOf('wake and bake')!=-1) intent = 'randomIntent'; 
	return intent;
}

exports.filter = filter;
exports.effects = effects;
exports.medical = medical;
exports.flavor = flavor;
exports.name = name;
exports.intent = intent;
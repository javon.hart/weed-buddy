'use strict';
let LogUtil = require('../utils/logger'),
effects = require('../res/effects'),
_ = require('underscore');

class EffectsUtil {

    static effects(list) {
        let count = 0,
            numKeys = 0,
            chain = [],
            remainder,
            index = {};
        _.each(list, function(o) {
            count += 1;
            chain.push(o);
            if (count == 5) {
                index[numKeys] = chain;
                chain = [];
                count = 0;
                numKeys += 1;
            }
        });
        if (chain.length > 0) {
            index[numKeys] = chain;
        }
        return index;
    }
    static listOfEffects() {
        return effects;
    }
}

module.exports = EffectsUtil;



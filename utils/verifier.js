let verifier = require("alexa-verifier");

module.exports = function(req, res, next) {
	console.log('req',req)
	 verifier(
        req.headers.signaturecertchainurl,
        req.headers.signature,
        req.rawBody,
        function verificationCallback(err) {
            if (err) {
            	console.log(err)
                res.status(401).json({ message: 'Verification Failure', error: err });
            } else {
                next();
            }
        }
    );
};

function getPrompt(state) {
	try {
		let prompt; 
		switch(state) {
		    case 'Alabama':
		        prompt = `
					In Alabama, marijuana for "personal use only" is a Class A Misdemeanor, 
					punishable by a maximum sentence of 1 year and a maximum fine of $6,000.
					Marijuana possessed for reasons other than "personal use," 
					or if the offender has been previously convicted of marijuana 
					possession for "personal use" only, is unlawful possession of 
					marijuana in the first degree and is a Class C felony, punishable 
					by a prison sentence of a minimum of one year in prison and a maximum 
					sentence of 10 years in prison, along with a maximum fine of $15,000.
					<break time=\"500ms\"/>
					Sale of a controlled substance in Alabama is a Class B felony punishable 
					with a minimum sentence of 2 years and a maximum sentence of 20 years, 
					along with a maximum fine of $30,000. The sale to a minor is a felony which is 
					punishable by a sentence of 10 years-life imprisonment and a maximum fine of $60,000.
					Sale within 3 miles of a school or a public housing project is an additional felony 
					punishable by an additional sentence of 5 years imprisonment.
		        `
		        break;
		    case "Alaska":
		        prompt = `
					Alaskans age 21 and older may legally possess up to one ounce of marijuana, 
					grow as many as six marijuana plants in their homes 
					(with no more than three flowering), and possess any additional 
					marijuana produced by those plants.
		        `
		        break;
	        case "Arizona":
	        	prompt = `
	 				Unless you have a medical marijuana ID card issued by a marijuana doctor \
	 				(a licensed M.D., D.O., M.D.(H), or N.D. from Arizona), cannabis is illegal. 
	 				If you do have a medical marijuana card, you can possess up to 2.5 ounces every two weeks 
	 				(14 days) and grow up to 12 plants.
	        	`
	        break;
	        
	        case "Arkansas":
	        	prompt = `
					The Arkansas Medical Marijuana Amendment legalizes the distribution 
					and possession of medical marijuana. The  amendment is specifically 
					for patients who have any of 17 qualifying conditions, which include 
					cancer, Tourette's syndrome, Chrohn's disease, fibromyalgia, post-traumatic 
					stress disorder and HIV/AIDS. Patients with a written statement from a doctor 
					certifying they have a qualifying condition are able to purchase medical marijuana 
					from dispensaries, and are not permitted to grow their own marijuana plants.
	        	`
	        break;
		            
	        case "California":
	        	prompt = "The Adult Use of Marijuana Act (Proposition 64) was passed on November 8th, 2016, legalizing the recreational use of marijuana for adults over the age of 21"
	        	
	        break;
		            
	        case "Colorado":
	        	prompt = "If you are an adult 21 years of age or older, you can legally possess 1 ounce of THC"
	        break;
		            
	        case "Connecticut":
	        	prompt = `
					the state has decriminalized the possession of small amounts of marijuana. 
					Typically, decriminalization means no prison time or criminal record for 
					first-time possession of a small amount for personal consumption. 
					The conduct is treated like a minor traffic violation. Those younger than 21 
					would face a 60-day driver's license suspension, similar to the existing penalty 
					for possessing alcohol.
	        	`
	        break;
		            
	        case "Delaware":
	        	prompt = `
					Under Delaware law marijuana is a schedule I drug. Legislation was approved 
					in 2015 amending penalties for the possession of up to one ounce (28.35 grams) 
					to a civil penalty, punishable by no more than a $100 fine. The use of marijuana 
					by minors, in public, or in a moving vehicle will remain a criminal offense.
	        	`
	        break;
	       
	        case "Florida":
	        	prompt = "marijuana remains illegal in Florida unless being used in medicinal cases"
	        break;
		            
	        case "Georgia":
	        	prompt = "The possession, sale, trafficking, and cultivation of marijuana is illegal."
	        break;
		            	       
	        case "Hawaii":
	        	prompt = `
					Cannabis in Hawaii is illegal for recreational use. Possession is permitted only 
					for medical use and otherwise remains a criminal infraction
	        	`
	        break;
		            	       
	        case "Idaho":
	        	prompt = `
					Cannabis in Idaho is illegal for all purposes, and possession of small amounts is 
					a misdemeanor crime.
	        	`
	        break;
		            	       
	        case "Illinois":
	        	prompt = `
					Cannabis in Illinois is decriminalized for recreational use and legally permitted 
					for medical use.
	        	`
	        break;
		            	       
		   	case "Indiana":
	        	prompt = "Possession of even small amounts is a misdemeanor crime."
	        break;
		            	       
	        case "Iowa":
	        	prompt = "only CBD only Oil and 3-4% THC oil is legal"
	        break;
		            
	        case "Kansas":
	        	prompt = "it is illegal for all purposes. Possession of even small amounts is a misdemeanor crime."
	        break;
		            
	       
	        case "Kentucky":
	        	prompt = `
					Cannabis is illegal for use as a drug in Kentucky, United States, though 
					non-psychoactive CBD oil is undergoing clinical trials in the state
	        	`
	        break;
		            	       
	        case "Louisiana":
	        	prompt = "it is legal for medical usage since 2015, but illegal for recreational usage."
	        break;
		               
	        case "Maine":
	        	prompt = "recreational marijuana is legal in maine"
	        break;
	       
        	case "Maryland":
	        	prompt = `
					the recreational use is illegal. However, since 2014, 
					the possession of 10 grams or less of marijuana has been decriminalized
	        	`
	        break;
		            
	        case "Massachusetts":
	        	prompt = "Recreational marijuana is legal in Massachusetts as of December 15, 2016"
	        break;
		            
	        case "Michigan":
	        	prompt = 'Cannabis in Michigan is legal for medical purposes since 2008, but illegal for recreational use.'
	        break;
		            
	        case "Minnesota":
	        	prompt = `
					Cannabis in Minnesota is illegal for recreational use, but permitted for medical use, 
					and possession of personal amounts was decriminalized in 1976. Several measures for recreational 
					legalization have been submitted and will carry over to the next legislative session.
	        	`
	        break;
		            
	        case "Mississippi":
	        	prompt = `
					Cannabis in Mississippi is illegal for recreational and medical purposes, 
					but use of low-THC/high-CBD products is allowed for severe seizure disorders under
					2014 legislation. Possession of small amounts was decriminalized in 1978.
	        	`
	        break;
		            
	        case "Missouri":
	        	prompt = `
					Cannabis in Missouri is illegal for recreational and medical purposes, 
					but use of low-THC CBD oil is allowed for certain conditions under 2014 legislation. 
					Possession of small amounts was decriminalized in 2014.
	        	`
	        break;
		            
	        case "Montana":
	        	prompt = `
					Cannabis in Montana is legal for medical uses, but illegal for recreational use since 1929. 
					Medical cannabis was legalized by ballot initiative in 2004. The senate and congress passed a 
					repeal to tighten MMJ laws which failed getting the Governor's signature. However, with the new 
					provisions, providers could not provide for more than three patients. In November of 2016 Bill I-182 
					was passed, this revised the 2004 law and allowed providers to give to more than three patients.
	        	`
	        break;
		            
	        case "Nebraska":
	        	prompt = `
					In the U.S. state of Nebraska, cannabis is illegal for all purposes, but first 
					offense for possession of small amounts was reduced to a civil infraction in 1979.
	        	`
	        break;
		            
	        case "Nevada":
	        	prompt = "Cannabis in Nevada is legal for recreational and medical uses."

	        break;
	       
	        case "New Hampshire":
	        	prompt = `
					Cannabis in New Hampshire is illegal for recreational use and is decriminalized 
					for possession of up to three-quarters of an ounce as of July 18, 2017. 
					It is permitted for medical use.
	        	`
	        break;
		            
	        case "New Jersey":
	        	prompt = "Cannabis in New Jersey is illegal and criminalized for recreational use, but permitted for medical use."
	        break;
	       
	        case "New Mexico":
	        	prompt = `
					Cannabis in New Mexico is illegal for recreational use and remains a criminal offense, 
					but is allowed for medical purposes.
	        	`
	        break;
	       
        	case "New York":
	        	prompt = `
					Cannabis in New York is illegal for recreational use, but is permitted for medical use, 
					and possession of small amounts is decriminalized.
	        	`
	        break;
		            
	        case "North Carolina":
	        	prompt = "Cannabis in North Carolina is illegal for recreational and medical use."
	        break;
		            
	        case "North Dakota":
	        	prompt = `
					cannabis was legalized for medical purposes in 2016 but remains illegal for 
					recreational purposes. Possession of small recreational amounts is a misdemeanor 
					crime. The cultivation of hemp is currently legal in North Dakota.
	        	`
	        break;
		            
	        case "Ohio":
	        	prompt = `
					In the of Ohio, cannabis is illegal for recreational use, but possession 
					of up to 100 grams is decriminalized.
	        	`
	        break;
	       
	        case "Oklahoma":
	        	prompt = `
					Marijuana in Oklahoma is illegal for all purposes, and Oklahoma is recognized for 
					having some of the strictest laws on cannabis in the United States. Medical marijuana 
					is not legal, but in April 2014 limited trials for medical use of CBD oil were legalized.
	        	`
	        break;
		            
	        case "Oregon":
	        	prompt = "Cannabis in Nevada is legal for recreational and medical uses."
	        break;
	       
	        case "Pennsylvania":
	        	prompt = `
					Cannabis in Pennsylvania is illegal for recreational use, but possession of small 
					amounts is decriminalized in a few major cities. Medical usage is permitted.
	        	`
	        break;
	       
	        case "Rhode Island":
	        	prompt = `
					In the state of Rhode Island, marijuana has been legal statewide for medical use, within 
					state regulations, since 2006. However, the possession of marijuana for recreational use 
					remains illegal. Several proposals to legalize the drug have been advanced in the Rhode 
					Island General Assembly, but none have succeeded.
	        	`
	        break;
	       
	        case "South Carolina":
	        	prompt = `
					Cannabis in South Carolina, United States, is illegal for recreational and medical 
					purposes, but use of low-THC CBD oil is allowed for certain conditions.
	        	`
	        break;
	       
	        case "South Dakota":
	        	prompt = `
					Cannabis in South Dakota is illegal for all purposes, and possession of small amounts 
					is a misdemeanor crime.
	        	`
	        break;
		            
	        case "Tennessee":
	        	prompt = `
					It is illegal to use or possess the drug cannabis in Tennessee. 
					with possession of even small amounts being a criminal misdemeanor, 
					but there are limited legal allowances for non-psychoactive CBD oil as medical 
					cannabis.
	        	`
	        break;	

	        case "Texas":
	        	prompt = `
					Cannabis in Texas  is illegal for medical and recreational 
					purposes, though low-THC CBD oil is allowed for the treatment of epilepsy since 2015. 
					In 1919 cannabis was restricted to prescription-only in Texas, prohibited in 1923, and 
					was declared a "narcotic" with potential life-sentences from 1931–1973.
	        	`
	        break;	

	        case "Utah":
	        	prompt = `
					Cannabis in Utah is illegal with the exception of non-psychoactive medical CBD oil 
					to treat severe epilepsy, and even possession of small amounts is a misdemeanor crime.
					Utah banned cannabis in 1915, making it one of the first states to do so. Utah legalized 
					CBD oil in March 2014, making it the first state to legalize only CBD oil without 
					legalizing other forms of cannabis.
	        	`
	        break;	

	        case "Vermont":
	        	prompt = `
					Cannabis in Vermont as of July 2015 is legal for medical usage, and decriminalized 
					but not legal for recreational usage.
	        	`
	        break;	
	        	            
	        case "Virginia":
	        	prompt = `
					Cannabis in Virginia is illegal for all purposes, and possession of even small 
					amounts is a criminal misdemeanor, but per 2015 law possession of CBD oil or 
					THC-A oil entails an affirmative defense for patients who have a doctor's 
					recommendation for those substances to treat severe epilepsy.
	        	`
	        break;
		            	        	            
	        case "Washington":
	        	prompt = `
					Cannabis in Washington is legal for medical purposes and for any purpose by adults 
					over 21. Medical marijuana was legalized in Washington in 1998, and recreational 
					use and sale of marijuana was legalized in 2012, through the Washington Initiative 
					502.
	        	`
	        break;
		            	        	            
	        case "West Virginia":
	        	prompt = `
					Cannabis in West Virginia concerns is illegal except for medical use. Non-medical 
					possession of even small amounts is a misdemeanor crime.
	        	`
	        break;
		            	        	            
	        case "Wisconsin":
	        	prompt = `
					Cannabis in Wisconsin is illegal with the exception of non-psychoactive medical CBD 
					oil, and even first-time possession of any quantity is a misdemeanor with up to six 
					months of incarceration, and repeated possession a felony. CBD oil was legalized in 
					2014, but under tight controls and for a very limited number of conditions, primary 
					seizures.
	        	`
	        break;
		            	        	            
	        case "Wyoming":
	        	prompt = `
					Wyoming has some of the strictest marijuana laws in the United States. Cannabis 
					itself is not allowed for medical purposes, but a 2015 law allows limited use of 
					non-psychoactive CBD oil. Possession of under three ounces of cannabis is a 
					misdemeanor that can be punished with up to a year in prison and a $1000 fine; 
					possession of over three ounces is a felony.
	        	`
	        break;

		    default:
		        throw 'unrecognized territory'
		}
		return prompt;

	} catch(e) {
		throw e 
	}
	
}

module.exports = getPrompt;

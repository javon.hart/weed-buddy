let User = require('../models/user');

class ProfileUtil {
	static isLinked(profile){
		try {
			if(!profile.fb_id) {
				return false;
			}
			return true;
		} catch(e) {
			return false;
		}
		
	}
	
	static isComplete(profile){
		try {
			if(!profile.country) {
				return false;
			}
			return true;


		} catch(e) {
			return false;
		}
	}

	static set(id,key,value) {
		User.findOne({ amzn_id: id }, function(err,user){
		    if(err) return false;
		    if(!user){
		        let newPerson = new User();
		        newPerson.amzn_id = id;
		        newPerson.save();
		    } else {
		        user[key] = value;
		        user.save();
		    }
		});
	}
}

module.exports = ProfileUtil;

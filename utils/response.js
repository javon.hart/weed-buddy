let Core = require('./core'),
    EventUtil = require('./event'),
    StrainUtil = require('./strain'),
    UserUtil = require('./users'),
    Session = require('./session'),
    Search = require('./search'),
    Profile = require('./profile'),
    Coin = require('./coin'),
    recipeUtil = require('./recipe'),
    Strings = require('../res/strings'),
    symptomsUtil = require('./symptoms'),
    effectsUtil = require('./effects'),
    flavorsUtil = require('./flavors'),
    Analytics = require('./analytics'),
    Filters = require('./filters'),    
    images = require('../res/images'),
    predict = require('../res/predictables'),
    Response = require('../models/response'),
    Strain = require('../models/strain'),
    User = require('../models/user'),
    _ = require('underscore');
let model, buddies, currentBuddy, nextBuddy;

function setBuddies(session) {
    console.log('setting buddies');
    buddies = JSON.parse(model.attrs.buddies);
    if(buddies.length <= 2) {
        console.log('getting next matches');
        buddies = UserUtil.getMatches(model.currentUser, session)
    } if(buddies.length >= 2) {
        currentBuddy = UserUtil.getUser(buddies[0].amzn_id, session);
        buddies.shift();
        nextBuddy = UserUtil.getUser(buddies[0].amzn_id, session);
    } else if(buddies.length == 1){
        currentBuddy = UserUtil.getUser(buddies[0].amzn_id, session);
        buddies.shift();
    }
    model.res.session('buddies', JSON.stringify(buddies));
}
function nextCard() {
    if(nextBuddy) {
        model.res.card({
            type: "Standard",
            title: nextBuddy.name,
            text: 'user profile information',
            image: {
                smallImageUrl: nextBuddy.fb_image || 'https://s3.amazonaws.com/weedbuddy/logo.png' // required
            }
        });
        model.res.say('<break time="600ms"/><audio src="https://s3.amazonaws.com/weedbuddy/connect.mp3" /> To connect with ' + nextBuddy.name + ' say connect<break time="500ms"/> or say next.').reprompt('You can say next or connect. Do you need more time?').shouldEndSession(false);
    } else {
        model.res.say('<break time="2100ms"/>I could not find any potential buddies at this time; try adjusting your discovery settings for more results.').shouldEndSession(true)
    }
}
function connect() {
    setBuddies(model.res, model.attrs);
    console.log('connect ' + currentBuddy.name);
    let potential = UserUtil.isPotentialConnection(model.currentUser, currentBuddy);
    if (potential) {
        model.res.say('You are now connected with' + currentBuddy);
        // add to connection
    } else {
        // no then add to potential buddies
        User.findOne({
            amzn_id: model.currentUser.amzn_id
        }, function(err, u) {
            if (err) return false;
            console.log(u);
            if (u) {
                try {
                    u.potentials.push(currentBuddy.amzn_id);
                    u.save(function(err) {
                        if (err) console.log(err);
                        console.log(200);
                        return true;
                    });
                } catch (e) {
                    console.log(e)
                }
                return true;
            } else {
                console.log('no user');
            }
        });
    }
    nextCard();
    return true;
}
function pass() {
    setBuddies(model.res, model.attrs);
    console.log('pass ' + currentBuddy.name);
    User.findOne({
        amzn_id: model.currentUser.amzn_id
    }, function(err, u) {
        if (err) return false;
        console.log(u);
        if (u) {
            try {
                u.passed.push(currentBuddy.amzn_id);
                u.save(function(err) {
                    if (err) console.log(err);
                    console.log(200);
                    return true;
                });
            } catch (e) {
                console.log(e)
            }
            return true;
        } else {
            console.log('no user');
        }
    });
    nextCard(model.res);
    return true;
}
function router() {
    let attrs = model.attrs, yes = model.yes,
        no = model.no,
        input = model.input,
        currentUser = model.currentUser,
        res = model.res,
        req = model.req,
        app = model.app,
        user = model.user,
        token;

    if(attrs.action == 'alexaApp.purchaseDecision') {
        if(yes) {
            Coin.btc()
            Coin.pot()
            res
            .say('would you like to pay with bitcoin?')
            .session('action', 'alexaApp.bitcoinPayment')
            .shouldEndSession(false)
        } else {
            res
            .say("<break time=\'200ms\'/>Will that be all?")
            .session('shopping', false)
            .session('action', 'alexaApp.shouldEndSession')
            .shouldEndSession(false)
        }
        return false;
    }

    if(attrs.action == 'alexaApp.like') {
        res.clearSession();        
        if(yes) { 
            User.findOne({
                amzn_id: currentUser.amzn_id
            }, function(err, user) {
                if (user) {
                    console.log(attrs.strain)
                    user.likes.push(attrs.strain);
                    user.likes = _.uniq(user.likes);
                    console.log(user)
                    user.save();
                    console.log(200)
                }
                return false;
            });
            let script = `I've added ${attrs.strain} to your likes`;
            res
                .say(script)
                .session('action', 'alexaApp.shouldEndSession')
                .say("<break time=\'200ms\'/>Will that be all?")
                .shouldEndSession(false)
            app.track(req, 'alexaApp.like', attrs.strain);
        }
        else {
            res                             
                .session('action', 'alexaApp.shouldEndSession')
                .say("<break time=\'200ms\'/>Will that be all?")
                .shouldEndSession(false)
        }
        return true;
    }

    if(attrs.action === 'alexaApp.fuzzySearch') {
        res.clearSession();        
        if(input) {
            
                if(yes) { 
                    if (StrainUtil.hasStrain(attrs.strain)) {
                        let x = new Strain(attrs.strain);
                        x.info(req, res, user);
                        app.track(req, token, attrs.strain);
                    } else {
                        res.say('try again later')
                    }
                    // Search.search(req, res, attrs.strain, app, user); 
                    return true;
                }
                else if (no){
                    res                             
                        .session('action', 'alexaApp.shouldEndSession')
                        .say("<break time=\'200ms\'/>Will that be all?")
                        .shouldEndSession(false)
                        return true
                }
            
        }
    }

    if(attrs.action === 'alexaApp.shouldEndSession') {
        res.clearSession();
        if (yes) {
             res
            .say('Okay! Goodbye.')    
            .shouldEndSession(true)
            return 
        } else if (no) {
             res
            .say(Strings.start)    
            .shouldEndSession(false)
            return
        }
    }
    if(attrs.action === 'discovery') {
        if(yes) {
            User.findOne({
                amzn_id: currentUser.amzn_id
            }, function(err, u) {
                if (err) return false;
                console.log(u);
                if (u) {
                    try {
                        u.discovery = true;
                        u.save(function(err){
                            console.log(err);
                            console.log('discovery initialized');
                            return true;
                        });
                    } catch(e) {
                        console.log(e);
                    }
                    return true;
                } else {
                    console.log('no');
                }
            });
            res.say('You are now discoverable. Try your request again.').shouldEndSession(false)
            return true;
        } else if(no) {
            res.say('this feature is unavailable unless your discoverable.').shouldEndSession(true);
            return true;
        }
    }
    if (attrs.action === 'editDiscovery') {
        let age;
        function set(req,type,input) {
            User.findOne({
                amzn_id: app.currentUser(req).amzn_id
            }, function(err, u) {
                if (err) return false;
                console.log(u);
                if (u) {
                    try {
                        u[type] = input;
                        u.save(function(err){
                            console.log(err);
                            console.log(200);
                            return true;
                        });
                    } catch(e) {
                        console.log(e)
                    }
                    return true;
                } else {
                    console.log('no')
                }
            });
        }
        try {
            switch(attrs.index) {
                case 0:
                    console.log('here');
                    age = parseInt(input);
                    if(age==NaN){
                        res.say('please say a number').shouldEndSession(false);
                        return;
                    }
                    set(req,'minimumAge',age);
                    res.session('index', 1)
                    res.say("what's your maximum age buddy").shouldEndSession(false)

                    break;
                case 1:
                    age = parseInt(input);
                    if(age==NaN){
                        res.say('please say a number').shouldEndSession(false);
                        return;
                    }
                    set(req,'maximumAge',age);
                    res.session('index', 2)
                    res.say("What gender buddy are you looking for? You can say male female or both").shouldEndSession(false)

                    // block
                    break;
                case 2:
                    let allowed = ['male','female','both'];
                    if(allowed.includes(input)) {
                        set(req,'genderBuddy',input);
                        res.session('index', 3)
                        res.say("All done. Now you can say find me a weed buddy.").shouldEndSession(false)

                    } else {
                        res.say('you can say male female or both')
                        return
                    }


                    // block
                    break;
                default:
                    res.session('index', 3)
                    res.say("all done").shouldEndSession(false)
                    // block
                    break;
            }
        } catch(e) {
            console.log(e)
        }
        return;
    }
    if (attrs.action === 'eventIntent') {
        token = "eventIntent";
        let case_1 = (attrs.ecomm === 'ticketInquiry' && yes);
        let case_2 = (attrs.ecomm === 'ticketInquiry' && no);
        let case_3 = (attrs.ecomm === 'ticketConsent' && yes);
        let case_4 = (attrs.ecomm === 'ticketConsent' && no);
        let case_5 = (attrs.ecomm === 'questionaire' && !input);
        let case_6 = (attrs.ecomm === 'questionaire' && input);
        if (case_1) {
            token = "eventIntent#ticketing";
            EventUtil.ticketWorkflow(req, res, attrs)
        } else if (case_2) {
            EventUtil.continueAsk(req, res, attrs)
        } else if (case_3 || case_6) {
            EventUtil.questionaire(req, res, attrs, input)
        } else if (case_4) {
            EventUtil.apologize(req, res, attrs)
        } else if (case_5) {
            res.say('Can you repeat that?').shouldEndSession(false)
        } else {
            EventUtil.continue(req, res, attrs);
        }
        app.track(req, token);
        return;
    }
    if (attrs.action === 'findBuddy') {
        this.findBuddy();
        return true;
    }
    if (attrs.action === 'readMessages') {
        if(yes) {
            res.say(`From blank`).shouldEndSession(true);
            return;
        }
    }
    // if (attrs.lastIntent === 'launch') {
    //     Search.search(req, res, input, app, user);
    //     return;
    // }
    if (attrs.action === 'listConnections') {
        if(yes) {
            res.say('listing all connections. You can say message remove or block any of those connections.').shouldEndSession(true);
            return;
        }
    }
    if(attrs.action === 'listIntent'){
        if (yes) {
            token = 'listResponse';
            listIntent(req, res, attrs);
            app.track(req, token);
            return;
        } else if (no) {
            res.say('Okay. Goodbye')
            app.track(req, token);
            return;
        }
    }
    if (attrs.action === 'profileIntent') {
        token = "profileResponse";
        profileIntent(req, res, attrs, input);
        app.track(req, token);
        return;
    }
    if (attrs.action === 'verifyAge') {
        console.log('age verification in progress');
        try {
            if (input) {
                console.log(input);
                let myAge = parseInt(input);
                if(!myAge) throw 400;
                if(myAge > 18 ) {
                    let person = app.currentUser(req);
                    User.findOne({
                        amzn_id: person.amzn_id
                    }, function(err, user) {
                        console.log(err);
                        if (err) return false;
                        if (!user) return false;
                        user.age = true;
                        user.myAge = myAge;
                        console.log(user);
                        user.save(function(err, u){
                            console.log(u);
                            console.log(err);
                            if(err){ console.log(err); }
                            else {
                                console.log(200);
                                return true;
                            }
                        });
                    });
                } else {
                    throw 401;
                }
                res.say("You're now ready to use this skill.").clearSession();
                return;
            } else {
                throw 400;
            }
        } catch(e) {
            if(e === 401 ) res.say(Strings.ofAge).shouldEndSession(true);
            if(e === 400 ) res.say("Please repeat that more clearly.").shouldEndSession(false);
            return true;
        }
    }
    if (no) {
        res.say('Okay Goodbye.');
        app.track(req, 'alexaApp.reponse'.concat('#goodbye'));
    }
    if (yes) {
        res.say('Yes what').shouldEndSession(false);
    }
    if(input && Object.keys(predict).includes(input)) {
        predict[input](req,res);
        return true;
    }
    else if (attrs.fail) {
        if (attrs.clarify) {
            token = 'clarify';
            res.say(Strings.notFound + (input || attrs.clarify).concat('<break time=\'300ms\'/> Give me some time to work on that.'))
            .session('action', 'alexaApp.shouldEndSession')
            .say("<break time=\'200ms\'/>Will that be all?")
            .shouldEndSession(false)
            app.track(req, token);
            return;
        } else if (input) {
            Search.search(req, res, input, app, user);
            return;
        }
    } 
    if(input) {
        Search.search(req, res, input, app, user);
    } else {
        res.say('come again?').shouldEndSession(true)            
    }
}

exports.router = router;
exports.findBuddy = function() {
    try {
        if (model.input === 'pass') {
            console.log('passing');
            pass()
        } else if (model.input === 'connect') {
            console.log('connecting');
            connect();
        } else if (model.yes) { // do you need more time
            model.res.say('Okay!').reprompt('You can say pass or connect. Do you need more time?').shouldEndSession(false)
        } 
    } catch (e) {
        console.log(e)
    }
};
function listIntent(req, res, attrs) {
    console.log('continue listing')
    console.log(attrs)
    let i = parseInt(attrs.index) + 1,
        index, prompt, list, type = attrs.type;
    if (type == 'all' || type == undefined) {
        type = 'all'
        console.log('listing all')
        list = StrainUtil.allStrains();
        index = StrainUtil.strains(list);
    }
    if (type == 'hybrid' || type == 'sativa' || type == 'indica') {
        list = StrainUtil.listOfStrains(type);
        res.session('type', type);
        index = StrainUtil.strains(list);
    }
    if (type == 'random') {
        list = _.shuffle(StrainUtil.allStrains());
        index = StrainUtil.strains(list);
    }
    if (type == 'recipes') {
        console.log('listing recipes')
        list = recipeUtil.listOfRecipes();
        res.session('type', type);
        index = recipeUtil.recipes(list);
    }
    if (type == 'symptoms') {
        console.log('listing symptoms')
        list = symptomsUtil.listOfSymptoms();
        res.session('type', type);
        index = symptomsUtil.symptoms(list);
    }
    if (type == 'effects') {
        console.log('listing effects')
        list = effectsUtil.listOfEffects();
        console.log(list)
        res.session('type', type);
        index = effectsUtil.effects(list);
        console.log(index)
    }
    if (type == 'flavors') {
        console.log('listing flavors')
        list = flavorsUtil.listOfFlavors();
        res.session('type', type);
        index = flavorsUtil.flavors(list);
    }
    console.log(list)
    console.log(index)
    prompt = Core.reduce(index[i]);
    res.card({
        type: "Standard",
        title: "List Cont'd",
        text: prompt,
        image: {
            smallImageUrl: _.shuffle(images).pop(),
        }
    }).say('Okay!<break time=\'300ms\'/>').say(prompt).session('action', 'listIntent').session('index', i + "");
    Analytics.event("alexaApp.list#" + type, Session.id(req, res), Session.time()).send();
    if (Object.keys(index).length <= i) {
        res.say('All finished. Thank you. Goodbye.').shouldEndSession(true);
    } else {
        res.say('<break time=\'100ms\'/>would you like to continue?').shouldEndSession(false).reprompt('would you like to continue?');
    }
};

function profileIntent(req, res, attrs, input) {
    let id = Session.id(req);
    try {
        switch (attrs.step) {
            case 'zip_code':
                Profile.set(id, 'zip_code', input);
                res.say('Thank you for completing your profile, enjoy!').shouldEndSession(true);
                break;
            case 'country':
                Profile.set(id, 'country', input);
                res.session('step', 'zip_code').say('What is your postal code?').shouldEndSession(false);
                break;
            default:
                console.log('default');
        }
    } catch (e) {
        console.log(e);
    }
};

exports.listIntent = listIntent;
exports.passBuddy = pass;
exports.profileIntent = profileIntent;
exports.response = function(currentUser, req, res, attrs, input, yes, no, app, user) {
    model = new Response(currentUser, req, res, attrs, input, yes, no, app, user);
};
exports.rewardIntent = function(req, res) {
    Analytics.event("alexaApp.rewards#2", Session.id(req, res), Session.time()).send();
    res.say("You didn't win. Please try again.").shouldEndSession(true);
};

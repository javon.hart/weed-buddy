let _ = require('underscore'),
	didyoumean = require('didyoumean'),
	StrainUtil = require('./strain'),
	Session = require('./session'),
	Filters = require('./filters'),
	Strain = require('../models/strain'),
	RecipeUtil = require('./recipe'),
	BusinessUtil = require('./business'),
	Strings = require('../res/strings'),
	Business = require('../res/business'),
	token = 'alexaApp.search';
let fuzzy = require('fuzzy');
let all = require('../res/all_strains');
let request = require('request-promise')
let cheerio = require('cheerio')

function search(req, res, q, app, user) {
	try {
		if (q) {
			let u = q;
			q = q.replace(/\w\S*/g, function(txt) {
				return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
			})
			q = Filters.name(q, app);
			console.log(u, q)
			if (StrainUtil.hasStrain(q)) {
				let x = new Strain(q);
				x.info(req, res, user);
				app.track(req, token, q);
				return
			} else {
				if (q.split(' ')[0].toLowerCase() != u.split(' ')[0].toLowerCase()) {
					let fuzz = fuzzy.filter(u, all)
					let closest;
					if (fuzz.length) {
						closest = fuzz[0].string
					} else {
						fuzz = fuzzy.filter(q.split(' ')[0], all)
						if (fuzz.length) {
							closest = fuzz[0].string
						}
					}
					if (!closest) {
						closest = q;
					}
					console.log('trigger', closest.toLowerCase(), q.toLowerCase())
					if (closest.toLowerCase() == q.toLowerCase()) {
						let x = new Strain(closest);
						x.info(req, res, user);
						app.track(req, token, closest);
						return
					} else {
						res.say("The closest I could find to " + u + " is " + closest).say("<break time=\'500ms\'/>Would you like to search for it?").session('action', 'alexaApp.fuzzySearch').session('strain', closest).shouldEndSession(false)
					}
					return true;
				}
				q = q.toLowerCase();
				console.log('searching ', q, u)
				if (q.includes('the flavor')) {
					res.say("For help say help. For flavors, ask me to describe the flavor of strain.");
				} else if (q.includes('the benefits')) {
					res.say("For help say help. For flavors, ask me to describe the benefits of strain.");
				} else {
					let fuzz = fuzzy.filter(q, all)
					let closest;
					if (fuzz.length) {
						closest = fuzz[0].string
					} else {
						fuzz = fuzzy.filter(q.split(' ')[0], all)
						if (fuzz.length) {
							closest = fuzz[0].string
						}
					}
					if (closest) {
						console.log('trigger', closest.toLowerCase(), q.toLowerCase())
						if (closest.toLowerCase() == q.toLowerCase()) {
							let x = new Strain(closest);
							x.info(req, res, user);
							app.track(req, token, closest);
							return
						} else {
							res.say("The closest I could find to " + u + " is " + closest).say("<break time=\'300ms\'/>Would you like to search for it?").session('action', 'alexaApp.fuzzySearch').session('strain', closest).shouldEndSession(false)
						}
						return true;
					} else {
						let attrs = req.sessionDetails.attributes;
						if (attrs) {
							if (!attrs.clarify) {
								res.say(Strings.notFound + q + ".").session('clarify', q).session('fail', true).session('lastIntent', 'search').say(Strings.clarify).reprompt("for help with what you can say, say help.").shouldEndSession(false);
							} else if (attrs.clarify) {
								res.clearSession();
								res.say(Strings.notFound + attrs.clarify + '<break time=\'300ms\'/> Give me some time to work on that.').session('action', 'alexaApp.shouldEndSession').say("<break time=\'200ms\'/>Will that be all?").shouldEndSession(false)
								return;
							} else {
								res.clearSession();
								res.say('I could not find what you were looking for. <break time=\'300ms\'/> Give me some time to work on that.').session('action', 'alexaApp.shouldEndSession').say("<break time=\'200ms\'/>Will that be all?").shouldEndSession(false)
								return
							}
						} else {
							res.clearSession();
							res.say('I could not find what you were looking for. <break time=\'300ms\'/> Give me some time to work on that.').session('action', 'alexaApp.shouldEndSession').say("<break time=\'200ms\'/>Will that be all?").shouldEndSession(false)
							return
						}
					}
				}
				app.trackFail(req, res, token, q);
			}
		} else if (!q) {
			res.say('What strain are you searching for?').shouldEndSession(false);
			return true;
		} else {
			if (q) res.say('I could not find ' + q + '. My apologies.').shouldEndSession(true);
			if (!q) res.say("I didn't know what to do with that request").shouldEndSession(true)
			app.trackFail(req, res, token, Strings.noQuery);
		}
	} catch (e) {
		app.fail(req, res, token, Strings.criticalError, e);
	}
}

function businessSearchFail(req, res, q, app) {
	if (!q) q = req.slot('q')
	if (!q) q = req.slot('n')
	if (q == undefined) {
		app.trackFail(req, res, 'businessSearch', Strings.noQuery);
		res.say("I didn't know what to do with that request")
		return;
	} else {
		app.trackFail(req, res, 'businessSearch', q);
		res.say('I couldn\'t find ' + q + '. please try that again later').shouldEndSession(true);
	}
}

function businessSearch(req, res, app) {
	try {
		console.log('request what happened? ', res)
		let q = req.slot('n');
		let strain = q;
		let name = q,
			bNames = _.pluck(Business, "name");
		name = didyoumean(name, bNames);
		if (name) {
			let biz = _.findWhere(Business, {
				name: name
			});
			if (biz) {
				let script = biz.name.concat(' is a dispensary located in ').concat(biz.city).concat(', ').concat(biz.state);
				res.say(script).shouldEndSession(true);
				Session.track(req, 'businessSearch', biz.name);
			} else businessSearchFail(req, res, name, Session);
		} else businessSearchFail(req, res, q, Session);
	} catch (e) {
		Session.fail(req, res, token, Strings.criticalError, e);
	}
}
exports.businessSearch = businessSearch;
exports.search = search;
let _ = require('underscore'),
Business = require('../res/business'),
Strings = require('../res/strings'),
ZipMap  = require('../res/zip_code_map'),
Core  = require('./core'),
Session  = require('./session'),
Filters = require('./filters'),
didyoumean = require('didyoumean');

function cleanName(id) {
    let bNames = _.pluck(Business, "name");
    id = didyoumean(id, bNames) || id;
    return id;
}

function currentState(zip) {
   let currentState = undefined; 
    _.each(Object.keys(ZipMap), function(it){
        if(zip >= ZipMap[it][0] && zip <= ZipMap[it][1]){
            currentState = it;
        }
    });
    return currentState;
}

function getBusiness(id) {
    id = cleanName(id);
    id = _.findWhere(Business, {
        name: id
    });        
    return id;
}

function localDispensaries(address) {
    let dList = Business;
    let state = currentState(address.postalCode);
    let sameRegion = Business.filter(function(b){
        let bZip = parseFloat(b.zip.substr(0,5));
        let bState = currentState(bZip)
        return state == bState;
    })
    return sameRegion;
}

class BusinessUtil {
    static all() {
        return Business;
    }
    static localDispensaries(address) {
        return localDispensaries(address)
    }
    static cleanName(id) {
        let bNames = _.pluck(Business, "name");
        id = didyoumean(id, bNames) || id;
        return cleanName(id);
    }
    static getBusiness(id) {        
        return getBusiness(id);
    }
    static hasBusiness(id) {
        id = this.cleanName(id) ;
        id = _.findWhere(Business, {
            name: id
        });
        return id ? true : false;
    }
    static reserve(req,res) {
        let q = req.slot('q');
        let n = req.slot('n');
//        res.say("Say yes to reserve ".concat(q).concat(' from ').concat(n)).shouldEndSession(true);
        res.say("This feature is unavailable at this time.").shouldEndSession(true);
    }
    static locate(req,res,token, user) {
        try {

            let q = req.slot('q');
            q = Filters.name(q, undefined).trim();
            let currentUser = user;
            let address = (currentUser.address)
            let noAddress = (address == undefined || address.type == 'FORBIDDEN')

            if(noAddress){
                res.say(Strings.allowAddress).shouldEndSession(true);
                return true;
                Session.track(req, token, 'noAddress');                
            }
            let dispensaries = localDispensaries(address);


            if(!dispensaries.length) {
                res.say("I could not find any dispensaries in your area at this time. Reach out to your local dispensaries and tell them about weed buddy!");
                Session.track(req, token, address.postalCode);
                return;
            } else {
                _.each(dispensaries,function(it){
                    it.distance = Math.abs(parseInt(it.zip) - parseInt(address.postalCode))
                })
                dispensaries = _.sortBy(dispensaries, function(it) { return it.distance; })

                console.log(dispensaries)

                let d = _.find(dispensaries, function(it) {
                    if(it.menu) return Object.keys(it.menu[q]).length > 1;
                });

                console.log(d)
                console.log(address)
                if( typeof d == 'object' ) {
                    if (d.length) {
                        d = d[0]
                    }
                    res.say("You can find " + q + " at " + d.name + ' in ' + d.city )
                    Session.track(req, token, d.name.concat('#').concat(q));
                } else {
                    Session.track(req, token, address.postalCode.concat('#').concat(q));
                    res.say("I could not find any " + q + " in your area at this time. Please try again later.");
                    return;
                }
            }

        }
        catch(e){
            throw e
        }

    }
    static isOpen(q,user) {
        let current = getBusiness(q);
        let day = Core.day();
        if(current) {
            if(!current.hours) {
                return undefined;
            }
            let open = parseInt(current.hours[day + '_open'].replace(':',''));
            let close = parseInt(current.hours[day + '_close'].replace(':',''));
            let bTime = Core.localTime(current.zip)
            console.log(bTime);
            if(bTime>open && bTime<close) {
                return true;
            } else {
                return false;
            }
        } else {
            return undefined;
        }
    }   
    static currentState(zip) {
        let local =  currentState(zip);
        return local;
    }

}

module.exports = BusinessUtil;

'use strict';

let Events = require('../res/events');
let Strings = require('../res/strings');
let moment = require('moment')
let _ = require('underscore');



function resumeSpeech(req,res,attrs) {
	let list = listAll()
	let i = parseInt(attrs.index) + 1
	let script = listAll()[i]
    res.session('action', 'eventIntent');
    res.session('index', i);
    res.session('event', JSON.stringify(list[i]))    
  	res.say(script)
  	console.log(list.length)
  	if(i < list.length - 1) {
	  	res.say('Would you like to continue')
		    .shouldEndSession(false);
  	} else if(i = list.length - 1)
		res.say('Thank you, goodbye.')
		    .shouldEndSession(true);
}

function listAll() {
	let validEvents = [], p = []
	_.each(Events, function(it){
		// filter by valid dates		
		// console.log(it)
		let valid = validDates(it)

		// console.log(valid)
		if(valid.length)validEvents.push(it)
	})
	validEvents.map(function(it) {
		p.push(prompt(it))
	})
	return p
	// say prompt 
	// ask about directions

}

function ordinal(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}


function validDates(it) {
	let valid = _.filter(it.dates, function(date){
		let day = new Date(date).toISOString()
		let bool = moment(day).format('Y-M-D') >= moment().format('Y-M-D')
		return (bool)
	})
	return valid
}


function prompt(event) {
	
	let weekday = {
		0: 'monday',
		1: 'tuesday',
		3: 'wednesday',
		4: 'thursday',
		5: 'friday',
		6: 'saturday'
	}
	let month = {
		0: 'january',
		1: 'february',
		3: 'march',
		4: 'april',
		5: 'june',
		6: 'july',
		7: 'august',
		8: 'september',
		9: 'october',
		10: 'november',
		11: 'december',
	}	
	let date = validDates(event)
	let datePrompt = ""

	
	_.each(date, function(day, i) {
		if(date.length > 0 && i == date.length -1) datePrompt =  datePrompt.concat(' and ')

		let theDay = moment(day)
		datePrompt = datePrompt
			.concat(weekday[theDay.weekday()])
			.concat(" ")
			.concat(month[theDay.month()])
			.concat(" ")
			.concat(ordinal(theDay.format('D')) + ',')
	})
	return datePrompt + Strings.quarterSecondBreak + event.venue +  Strings.quarterSecondBreak  + event.title + Strings.quarterSecondBreak + event.description
}

exports.continue = function(req,res,attrs) {
	try {
		resumeSpeech(req,res,attrs)
	} catch(e) {
		console.log(e)
	}

	
}

exports.allEvents = function() {
    return _.pluck(Events,  "title")    
}

exports.events = function(list) {
    let count = 0,
        numKeys = 0,
        chain = [],
        index = {};
    _.each(list, function(o) {
        count += 1;
        chain.push(o);
        if (count == 1) {
            index[numKeys] = chain;
            chain = [];
            count = 0;
            numKeys += 1;
        }
    });
    if (chain.length > 0) {
        numKeys += 1;
        index[numKeys] = chain;
    }
    return index;
}

exports.continueAsk = function(req, res) {
	res.say('Would you like to continue?').shouldEndSession(false);
}

exports.listAll = listAll;

exports.ticketWorkflow = function(req,res,attrs) {
	res.session('ecomm', 'ticketConsent')	
	res.say('Okay')
	.say('To complete this request I\'ll need to collect some personal information. Are you okay with that?')
	.shouldEndSession(true)
}


function ticketSession(req,res,key,value) {
    res.session(key, value);	
}

exports.ticketSession = ticketSession; 

exports.apologize = function(req,res,attrs) {
	let session = req.getSession();
  	session.clear('ecomm');
    res.session('action', 'eventIntent');  	
	res.say('Sorry I couldn\'t complete your request at this time.')
	res.say('Would you like to continue?').shouldEndSession(false);
}

exports.questionaire = function(req,res,attrs,input) {
	let keyMap = {
		0:'first_name'
	}
	let valueMap = {
		0: 'What is your first name' 
	}
	let qIndex = attrs.qIndex;
	if(!qIndex) {
		qIndex = 0
	}	
	let item = { value: valueMap[qIndex], key: keyMap[qIndex] } 

	if(qIndex > 0) {
		console.log(input)
		res.session(item.key, input)	
	} 

	res.say(item.value).shouldEndSession(false)
	res.session('qIndex', qIndex + 1);	
	res.session('ecomm', 'questionaire');	
}





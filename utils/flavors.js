'use strict';
let LogUtil = require('../utils/logger'),
flavors = require('../res/flavors'),
_ = require('underscore');

class FlavorsUtil {

    static flavors(list) {
        let count = 0,
            numKeys = 0,
            chain = [],
            index = {};
        _.each(list, function(o) {
            count += 1;
            chain.push(o);
            if (count == 50) {
                index[numKeys] = chain;
                chain = [];
                count = 0;
                numKeys += 1;
            }
        });
        if (chain.length > 0) {
            index[numKeys] = chain;
        }
        return index;
    }
    static listOfFlavors() {
        return flavors;
    }
}

module.exports = FlavorsUtil;



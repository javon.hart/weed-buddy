let LogUtil = require('./logger'),
	moment = require('moment'),
	fs = require('fs'),
	_ = require('underscore'),
	User = require('../models/user'),
	Update = require('../models/update'),
	Strings = require('../res/strings'),
	Analytics = require('./analytics');
let FB = require('fb');
let Local = require('../models/local');
let Session = require('../models/session');
let request = require('request');
let request_promise = require('request-promise')

let session = new Session();
let mongoose = require("mongoose");
const uuid = require('uuid/v1');
let  AWS = require('aws-sdk');
let child = require('child_process');

const options = {
	params: {
		Bucket: 'echobuddy'
	},
	accessKeyId: 'AKIAIB72SHX4XWLK7IQA',
	secretAccessKey: "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"
};

AWS.config.update(options);
let s3Bucket = new AWS.S3({
	params: {
		Bucket: 'echobuddy'
	}
});

let download = function(uri, filename, callback) {
	request.head(uri, function(err, res, body) {
		// request(uri).pipe(fs.createWriteStream(__dirname + '../tmp/' + filename)).on('close', callback);
	});
};

function isLinked(req) {
	let person = currentUser(req);
	if(!person.fb_id) return false;
	return (person.fb_id.length !== 0);
}

function facebook(req) {
    let person = currentUser(req);
    if(person) {
        let refresh = moment().diff(moment(person.fb_archive), 'minutes') > 15;
        if(!refresh && person.fb_archive) return;
    	try {
    		let session = req.sessionDetails;
    		let id = session.userId;
    		let attrs = session.attributes;
    		let active = undefined;
    		let token = session.accessToken;
    		let name, fbID;
    		if (attrs) active = attrs.active;
    		if (token) {
    			FB.setAccessToken(token);
    			FB.api('/me', function(res) {
    				if (!res || res.error) {
    					console.log(!res ? 'error occurred' : res.error);
    					return;
    				}
    				name = res.name;
    				fbID = res.id;
    				User.findOne({
    					amzn_id: id
    				}, function(err, user) {
    					if (user) {
    						user.name = name;
    						user.fb_id = fbID;
    						user.save();
    					}
    					return false;
    				});
    			});
    			let url = `/me/?fields=picture.width(720).height(480)`;
    			FB.api(url, function(res) {
    				if (!res || res.error) {
    					console.log(!res ? 'error occurred' : res.error);
    					return;
    				}
    				User.findOne({
    					amzn_id: id
    				}, function(err, user) {
    					if (user) {
    						let rawImage = res.picture.data.url;
    						user.fb_image = (uuid()).concat('.jpeg');
    						// download image then upload to aws
    						download(rawImage, user.fb_image, function() {
    							let path = '../tmp/' + user.fb_image;
    							fs.stat(path, function(err, file_info) {
    								let bodyStream = fs.createReadStream(path);
    								let params = {
    									Bucket: 'echobuddy',
    									Key: user.fb_image,
    									ContentLength: file_info.size,
    									ContentType: 'image/jpeg',
    									Body: bodyStream,
    									ACL: 'public-read',
    									'StorageClass': 'STANDARD'
    								};
    								s3Bucket.putObject(params, function(err) {
    									if (err) //handle
    										console.log(err)
    								});
    								user.fb_image = `https://s3.amazonaws.com/echobuddy/${user.fb_image}`
    								user.fb_archive = moment(new Date());
                                    user.save();
    							});
    							console.log('done');
    						});
    					}
    					return false;
    				});
    			});
    			return;
    		} else {
    		    console.log('no token')
    		    return;
    		}
    	} catch (e) {
    		console.log(e);
    	}
    }

};

function signed(req, res) {
	let whitelist = ['amzn1.ask.skill.351bdd42-e300-4d8b-90c0-8f55da4d05a4', // ian
		'amzn1.ask.skill.b079b986-0a78-4edd-a285-2832b155c612', // live
		'amzn1.ask.skill.6ccf28b0-eb0a-45cc-ab39-1115f4b40313' // test
	];
	try {
		let session = req.sessionDetails;
		if (session) {
			let id = session.application.applicationId;
			if (!whitelist.includes(id)) throw 'access denied';
		} else throw 'invalid session';
	} catch (e) {
		Analytics.event('INVALID_SESSION', new Date()).send();
		console.log(e);
		res.json(500);
	}
}

function currentUser(req) {
	let id = userId(req);
	let out = session.getStore()[id];
	// console.log('i am ', out)
	return out || false;
}

function getUser(id) {
	let out = session.getStore()[id];
	return out || false;
}

function getTime(req, user) {
	// GET /v2/devices/{deviceId}/settings/System.timeZone
	console.log('getTime');
	try {
		if (!req.context.System.apiAccessToken) return undefined;
		let token = req.context.System.apiAccessToken;
		if (!token) return false;
		let options = {
			uri: 'https://api.amazonalexa.com/v2/devices/' + user.deviceId + '/settings/System.timeZone',
		};
		request(options, function(error, response, data) {
			if (error) console.log(error);
			console.log(data);
		});
		return true
	} catch (e) {
		Analytics.event('GET_ADDRESS_FAIL', e).send();
		console.log(e);
	}
}

function getAddress(req, user) {
	console.log('getAddress');
	console.log(req.context.System);
	try {
		if (!req.context.System.apiAccessToken) return undefined;
		let token = req.context.System.apiAccessToken;
		if (!token) return false;
		let options = {
			hostname: 'api.amazonalexa.com',
			uri: 'https://api.amazonalexa.com/v1/devices/' + user.deviceId + '/settings/address',
			method: 'GET',
			headers: {
				'Host': 'api.amazonalexa.com',
				'Accept': 'application/json',
				'Authorization': 'Bearer ' + token
			}
		};
		request(options, function(error, response, data) {
			if (error) console.log(error);
			console.log(data);
			if (data && data.type != 'FORBIDDEN') {
				user.address = data;
				user.markModified('address');
				user.save(function(err){
				    console.log(err);
				    console.log(200)
				});
			}
		});
		return true
	} catch (e) {
		Analytics.event('GET_ADDRESS_FAIL', e).send();
		console.log(e);
	}
}

function registerDevice(req, res, user) {
	try {
	    console.log('geoLocate', user);
		user.deviceId = req.context.System.device.deviceId;
		let outcome = getAddress(req, user);
		getTime(req, user)
		console.log('register device', outcome)
	} catch (e) {
		console.log(e);
		trackFail(req, res, 'alexaApp.deviceRegistration', Strings.criticalError, 'failure to register device', user);
	}
}

function time() {
	return moment().format('MMMM Do YYYY, h:mm:ss a');
}

function userId(req) {
	try {
		let id = req.userId;
		return id;
	} catch (e) {
		LogUtil.log(e);
		return false;
	}
}

function trackFail(req, res, token, q, e, user) {
	let person = user;
	if (!person) return false;
	if (!token) return false;
	try {
		Analytics.event(token.concat("#fail?").concat(q), userId(req), time()).send();
		token = token.replace('alexaApp.', '');
		User.findOne({
			amzn_id: person.amzn_id
		}, function(err, user) {
			if (err) return false;
			if (!user) return false;
			if (user.fails == undefined) user.fails = {};
			let val = user['fails'][token] || 0;
			user['fails'][token] = (val + 1);
			user.markModified('fails');
			let stamp = time();
			user.lastFail = {
				token: token,
				stamp: stamp
			};
			user.markModified('lastFail');
			if (q) {
				if (user.fail_terms == undefined) user.fail_terms = {};
				user.fail_terms[token] = (user.fail_terms[token] || []);
				user.fail_terms[token].push(q);
				user.fail_terms[token] = _.uniq(user.fail_terms[token]);
				user.markModified('fail_terms');
				if (q.includes("criticalError")) {
					if (user.critical_errors == undefined) user.critical_errors = {};
					user.critical_errors[token] = (user.critical_errors[token] || []);
					user.critical_errors[token].push(e.toString());
					user.critical_errors[token] = _.uniq(user.critical_errors[token]);
					user.markModified('critical_errors');
				}
			}
			user.save();
		});
	} catch (e) {
		LogUtil.log(e);
	}
}

function fail(req, res, token, q, e, user) {
	console.log(q, e)
	res.say("I didn't get that, please try that again.").shouldEndSession(false);
	trackFail(req, res, token, q, e, user);
}

function verifyAge(req, res) {
	return true;
	let user = currentUser(req);
	LogUtil.log(user);
	if (user && !user.age) {
		res.session('action', 'verifyAge');
		res.say('<audio src="https://s3.amazonaws.com/weedbuddy/click.mp3" />' + Strings.quarterSecondBreak + ' How old are you').shouldEndSession(false);
		return true;
	}
}

exports.initialize = function(req, res, callback) {
	signed(req, res);
	try {
		let session = req.sessionDetails;
		if (!session) return false;
  		var promise = User.findOne({
            amzn_id: req.userId
        }).exec();
        return promise.then(function(user) {
        	if(user) callback.call(this, req, res, user)
        	if(!user) { 
                let newPerson = new User();
                newPerson.amzn_id = req.userId;
                newPerson._id = mongoose.Types.ObjectId();
                newPerson.save()
        		res.say("I'm getting all setup. Please try your request again in a few seconds.")  
        	}     
        })
	} catch (e) {
		console.log(e);
		throw 'cannot initialize';
	}
};
exports.isLinked = isLinked;
exports.time = time;
exports.isActive = function(req) {
	try {
		let session = req.sessionDetails;
		if (session) {
			let attrs = session.attributes,
				active = false;
			if (attrs) {
				active = attrs.active;
			}
			console.log(session);
			if (active) return true;
			return false;
		} else {
			return false;
		}
	} catch (e) {
		console.log(e);
		return false;
	}
};
exports.signedSession = signed;
exports.id = userId;
exports.activityTracker = function(req, res) {
	try {
		let session = req.sessionDetails;
		if (session) {
			res.session('active', true);
		}
	} catch (e) {
		console.log(e);
	}
	return false;
};
exports.geoLocate = function(req, res) {
    if(req.type() != 'LaunchRequest') return;
	User.findOne({
		amzn_id: req.userId
	}, function(err, user) {
	    console.log(err);
		if (err) return false;
		if (user) registerDevice(req, res, user); // do other sort of check
	});
};
exports.fb = facebook;
exports.session = function() {
	return session;
};
exports.currentUser = currentUser;

exports.saveUpdates = function(req, updates) {
	try {
		User.findOne({ amzn_id: req.userId }, function(err,usr){
			if(!usr) {
				return false
			} else {
				usr.updates = usr.updates || []
				usr.updates = _.uniq(usr.updates.concat(updates))
				usr.save()
			}
			console.log('saving updates')
		})
	} catch (e) {
		LogUtil.log(e);
		LogUtil.log(req);
	}
}

exports.track = function(req, token, query, intent) {
	try {
		Analytics.event(token, req.userId, time()).send();
		if (!token) return false;
		token = token.replace('alexaApp.', '');
		User.findOne({
			amzn_id: req.userId
		}, function(err, user) {
			if (err) return false;
			if (!user) return false;
			if (user.activity == undefined) user.activity = {};
			if (query && token != 'clarify') {
				if (user.search_history == undefined) user.search_history = {};
				user.search_history[token] = (user.search_history[token] || []);
				user.search_history[token].push(query);
				user.search_history[token] = _.uniq(user.search_history[token]);
				user.markModified('search_history');
			} else if (token == 'clarify') {
				if (user.clarify_terms == undefined) user.clarify_terms = {};
				user.clarify_terms[intent] = (user.clarify_terms[intent] || []);
				user.clarify_terms[intent].push(query);
				user.clarify_terms[intent] = _.uniq(user.clarify_terms[intent]);
				user.markModified('clarify_terms');
			}
			let val = user['activity'][token] || 0;
			user['activity'][token] = (val + 1);
			user.markModified('activity');
			let stamp = time();
			user.lastAttempt = {
				token: token,
				stamp: stamp
			};
			user.markModified('lastAttempt');
			user.save();
		});
	} catch (e) {
		LogUtil.log(e);
		LogUtil.log(req);
	}
};
exports.clear = function(req, key) {
	req.getSession().clear(key)
};
exports.trackFail = trackFail;
exports.fail = fail;
exports.getUser = getUser;
exports.address = getAddress;
exports.verifyAge = verifyAge;
exports.addressConsent = function(currentUser) {
	if (!currentUser) return false;
	let address = currentUser.address;
    if (!address){
        return false;
	} else if (address.postalCode) {
    	return true;
    } else {
    	return false;
	}
};
exports.save = function() {
	session.save();
}



"use strict";

/*** Mongo */
let mongoose = require("mongoose"),
url = process.env.MONGODB_URI || "mongodb://localhost/buddy";
mongoose.connect(url);


/*** Dependencies */
let API = require('./routes/api')(),    
    bodyParser = require("body-parser"),
    express = require("express"),
    app = express(),
    http = require("http"),
    LogUtil = require("./utils/logger"),
    store = undefined,
    // verifier = require("./utils/verifier"),
    opbeat = require('opbeat').start(),
    Session = require("./utils/session"),
    User = require("./models/user"),
    Name = require("./models/name"),
    Word = require("./models/word"),
    Words = require("./res/words"),
    session = Session.session(),
    CronJob = require('cron').CronJob,
    _ = require('underscore'),
    child = require('child_process');
let verifier = require('alexa-verifier-middleware');


/*** Syncing Application ***/
// setInterval(function(){
//     Session.initStorage()
// }, 3000)
/*** End of Syncing Application ***/


/*** Express */


/***  Initialize with Storage  */
store = undefined;
/*** Alexa Application */
app = require("./controllers/alexa-controller")(app,Session);
/** Google */
app = require('./controllers/google-controller').app(app,store);

if(process.env.NODE_ENV == 'production') { app.use(verifier); }
app.use(express.static("public"));
app.use(opbeat.middleware.express());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use('/', API);


/***  Port  */
const PORT = process.env.PORT || 5000;
app.listen(PORT);
LogUtil.log("Listening on port " + PORT);

new CronJob('0 23 * * *', function() {
Word.remove({}, function(err) {
    if (err) {
        console.log(err);
    }      
    let word = new Word()
    let w = _.shuffle(Words).pop()
    word.title = w.title;
    word.content = w.content;
    word.save()     
});

}, null, true, 'America/Los_Angeles');

Word.remove({}, function(err) {
    if (err) {
        console.log(err);
    }      
    let word = new Word()
    let w = _.shuffle(Words).pop()
    word.title = w.title;
    word.content = w.content;
    word.save()     
});

// new CronJob('*/20 * * * *', function() {
//     let sys = require('sys');
//     let exec = require('child_process').exec;
//     function puts(error, stdout) { sys.puts(stdout); };
//     let command  = "node news";
//     console.log(command.concat('\n'));
//     exec(command, puts); 
// }, null, true, 'America/Los_Angeles');

/***  Keep Alive Heroku */
function startKeepAlive() {
    setInterval(function() {
        let options = {
            host: "lit-journey-82818.herokuapp.com",
            port: 80,
            path: "/"
        };
        http.get(options, function(res) {
            res.on("data", function() {
                try {
                    // console.log(data);
                } catch (err) {
                    console.log(err.message);
                }
            });
        }).on("error", function(err) {
            console.log("Error: " + err.message);
        });
    }, 20 * 60 * 1000);
}

function startSubServices() {
    /*** Twitter */
    child.fork(__dirname + '/bin/up')
    .on('message', function(m) {
        console.log(m);
    });
    /*** Potcoin */

    /*** Initialize ***/
    // child.fork(__dirname + '/bin/initialize')
    // .on('message', function(m) {
    //     console.log(m);
    // });
}

startKeepAlive();

if(process.env.NODE_ENV == 'production') {
    startSubServices(); 
}

process.on('uncaughtException', (err) => {
  console.error(err)
})

angular.module('HelloWorldApp', [])
   .controller('HelloWorldController', function($scope) {
       $scope.count = 0; 
       $scope.strain = strains[$scope.count];
       $scope.description = $scope.strain.description


       $scope.next = function() {
       	if($scope.count<strains.length)$scope.count++
       	$scope.strain = strains[$scope.count];
       }
       $scope.previous = function() {
       	if($scope.count!=0)$scope.count--;
       	$scope.strain = strains[$scope.count];
       }    
       $scope.save = function(strain) {
   			var data = $scope.strain 
   			data.description = $('#description').val();
   			$.post({
   				url: '/editStrain',
   				data: data
   			})
       	$scope.words = $('#description').val().split(' ').length
       }  
       $scope.$watch('count',function(){
       	$scope.strain = strains[$scope.count];
       	$('#description').val($scope.strain.description)
       	$scope.words = $('#description').val().split(' ').length
         if(window.location.pathname === '/new') {
          $scope.strain = {}
          $scope.description = ''
          $('#description').val('')
         }

       }) 
       $scope.$watch('strain.description',function(){
       })  
       $scope.create = function() {
        console.log($scope.strain)
        $scope.strain.description = $('#description').val();
        $.post({
          url: '/strain',
          data: $scope.strain
        }, function(data) {
          console.log(data)
        })
       }      


});
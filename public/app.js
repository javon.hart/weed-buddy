'use strict'

let Strings = require('../res/strings');
let StrainUtil = require('../utils/strain');
let _ = require('underscore');
let bot = require('./bot');
let moment = require('moment');

class Demo {
	static intent(req) {
		let intent = bot.intent(req);
		if(intent) {
			return bot.action(intent,req)
		} 
		else {
			let q = req.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) // title case
			if(StrainUtil.hasStrain(q)) {
	  			let x = new bot.strain(q);
	  			return x.info(); // search reply todo:// make more dynamic
		  	} else {
				return "Sorry. I'm not sure how to handle the action " + req.concat('. Please try that again later.');
		  	}
		}
	}
	static time () {
    	return moment().format('MMMM Do YYYY, h:mm:ss a');
	}
	static where() {
		return _.shuffle(['Game Room','Dining Room', 'Hyundai Genesis','Dispensary Counter', 'Showcase Table','Living Room', 'Kitchen', 'Basement', 'Garage', 'Master Bedroom', 'Office', 'Showroom Floor']).pop();
	}
	static background() {
		return _.shuffle(['patterna.png','patternb.png','patternc.png','patternd.png','patterne.png','patternf.png','patterng.png','https://s3.amazonaws.com/weedbuddy/pattern-1.png', 'https://s3.amazonaws.com/weedbuddy/pattern.png']).pop();
	}
	static prompt() {
		return  _.shuffle(['Ahoy matey!','Hi!', 'I\'m glad you can chat!', 'Hello!', 'Greetings!', "How\'s it going?", "What\'s up!", 'How do you do?', 'Howdy!', 'Yo!', 'Alright mate?']).pop()
+ " You can now ask for suggestions by saying, suggest weed good for, followed by a symptom or effect." + 
" For help with what you can say, say help. " + _.shuffle(Strings.starter).pop().replace('<break time=\"500ms\"/>','')  
	}
	static talk() {
		return _.shuffle(["Alexa, start Weed Buddy.", "Alexa, launch Weed Buddy.", "Alexa, talk to Weed Buddy.", "Alexa, open Weed Buddy."]).pop()
	}

}

// try {
// 	window.app = Demo;


// } catch(e){}


// module.exports = Demo;




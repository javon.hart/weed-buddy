'use strict';
let StrainUtil = require('../../utils/strain'),
  _ = require('underscore'),
  images = require('../../res/images')
class Strain {
  constructor(id) {
    this.body = StrainUtil.getStrain(id);
    this.info = function(res) {
      let before = res || ""
      return before + " " + this.body.type.trim() + '\n ' + this.body.description.trim().replace(/&/g, ' and '); 
    };
    this.effects = function(res) {

      let speech;
      speech = filter(this.body.effects);
      speech = speech.trim();
      if (!speech) {
        speech = "I could not find those effects. But I am always learning. Please try again later!";
      } else {
        speech = 'Consumer reviews describe ' + speech + ' effects.';
      }

      return speech 

    };
    this.flavors = function(res) {
      let speech;
      speech = filter(this.body.flavors);
      speech = speech.trim();
      if (!speech) {
        speech = "I could not find those flavors. But I am always learning. Please try again later!";
      }

      return speech;

    };
    this.medical = function(res) {
      let speech;
      speech = filter(this.body.medical);
      speech = speech.trim();
      if (!speech) {
        speech = "I could not find that information. But I am always learning. Please try again later!";
      } else {
        speech = 'Consumer reviews say ' + this.body.name + ' is good for ' + speech;
      }
      return speech;
    };
    this.type = function(res) {
      let speech, type;
      type = this.body.type;
      if (type) {
        speech = this.body.name + ' ' + type;
      } else {
        speech = "I could not find that information. But I am always learning. Please try again later!";
      }

      return speech;

    };
  }
  setAssistant(assistant) {
    this.assistant = assistant;
  }
}
// clean a strings know issues i.e effects context
function clean(speech) {
  let out = speech.replace('Lackof', 'Lack of sleep');
  return out;
}

// add and before the last speech item i.e effects context.
function and(list) {
  let speech = "";
  _.each(list, function(o,i){
    if(i==list.length-1) {
      speech += ' and ' + o;
    } else {
      speech += " " + o + ",";
    }
  });
  return speech;
}

function filter(speech) {
  let list = speech.match(/[A-Z][a-z]+/g);
  let res = and(list);
  res = clean(res); 
  return res;
}
module.exports = Strain;
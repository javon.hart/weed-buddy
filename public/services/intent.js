 function botIntent(q) {
	q = q.toLowerCase();
	let intent = undefined;
	if(q.indexOf('find')!=-1) intent = 'searchIntent';
	if(q.indexOf('describe')!=-1) intent = 'searchIntent';
	if(q.indexOf('lookup')!=-1) intent = 'searchIntent';
	if(q.indexOf('benefit')!=-1) intent = 'medicalIntent';
	if(q.indexOf('business')!=-1) intent = 'businessIntent'; 
	if(q.indexOf('community')!=-1) intent = 'communityIntent'; 
	if(q.indexOf('effect')!=-1) intent = 'effectsIntent'; 
	if(q.indexOf('fact')!=-1) intent = 'factIntent'; 
	if(q.indexOf('flavor')!=-1) intent = 'flavorIntent'; 
	if(q.indexOf('help')!=-1) intent = 'helpIntent'; 
	if(q.indexOf('joke')!=-1) intent = 'jokeIntent'; 
	if(q.indexOf('lucky')!=-1) intent = 'randomIntent'; 
	if(q.indexOf('news')!=-1) intent = 'communityIntent'; 
	if(q.indexOf('taste')!=-1) intent = 'flavorIntent'; 
	if(q.indexOf('type')!=-1) intent = 'typeIntent'; 
	if(q.indexOf('order')!=-1) intent = 'purchaseIntent'; 
	if(q.indexOf('purchase')!=-1) intent = 'purchaseIntent'; 
	if(q.indexOf('wake and bake')!=-1) intent = 'randomIntent'; 
	if(q.indexOf('talk')!=-1) intent = 'launchIntent'; 
	if(q.indexOf('launch')!=-1) intent = 'launchIntent'; 
	if(q.indexOf('list')!=-1) intent = 'listIntent'; 
	if(q.indexOf('yes')!=-1) intent = 'responseIntent'; 
	if(q.indexOf('no')!=-1) intent = 'responseIntent'; 
	if(q.indexOf('recipe')!=-1) intent = 'recipeIntent';
	if(q.indexOf('cook')!=-1) intent = 'recipeIntent';
	if(q.indexOf('how to make')!=-1) intent = 'recipeIntent';

	return intent;
}

module.exports = botIntent;
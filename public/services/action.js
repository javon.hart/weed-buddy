let StrainUtil = require('../../utils/strain');
let bot = require('../bot');
let Jokes = require('../../res/jokes');
let Facts = require('../../res/facts');
let Strings = require('../../res/strings');
let allStrains = require('../../res/all_strains');
let _ = require('underscore');
let Recipes = require('../../res/recipe_names');
let allRecipes = require('../../res/all_recipes');
let out;

function botAction(intent,req) {
	switch (intent) {
		case 'searchIntent':
  			return search(req); 
			break;
		case 'listIntent':
			return list(req)
			break;			
		case 'medicalIntent':
			return medical(req)
			break;
		case 'responseIntent':
			console.log(window.intent);
			if(window.intent=='listIntent') return listResponse();
			return 'responseIntent'
			break;
		case 'businessIntent':
			return business();
			break;			
		case 'communityIntent':
			return community();
			break;
		case 'effectsIntent':
			return effects(req);
			break;
		case 'factIntent':
			return fact();
			break;
		case 'flavorIntent':
			return flavors(req);
			break;
		case 'helpIntent':
			return help();
			break;
		case 'jokeIntent':
			return joke();
			break;
		case 'randomIntent':
			return random();
			break;
		case 'typeIntent':
			return type(req);
			break;
		case 'recipeIntent':
			return recipe(req);
			break;
		case 'purchaseIntent':
			return "purchaseIntent"
			break;
		case 'launchIntent':
			return launch()
			break;
		default:
			return false;
	}
}


function getRecipe(id) {
    return _.findWhere(allRecipes, {
        title: id
    });
}
function recipe(req) {
	let q = match(req,Recipes);
	if(q){
		console.log(q)
		q = q.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})
		

		let recipe = getRecipe(q);
        let success = ['Fantastic Idea!','Good Choice!', 'Yummy choice!', 'Scrumptious!', 'Exquisite choice.', 'Tasty!', 'Delish!', 'Mouthwatering!', 'Nice!'];
        let speech = _.shuffle(success).pop();
        
        speech = speech
        .concat(' ')
        .concat(recipe.title)
        .concat('. ');

        if(recipe.ingredients.length) {
            speech = speech.concat(_.shuffle(['This recipe calls for: ','To make this you\'ll need: ','Ingredients: ', 'Collect the following ingredients: ']).pop());
            _.each(recipe.ingredients,function(o){
                let ingredient = (o);
                speech = speech.concat('\n');
                speech = speech.concat(ingredient);
            });            
        }
        speech = speech.concat(_.shuffle(['. Let\'s begin! ','. Let\'s get started! ','. Ready? Let\'s cook! ']).pop())
        .concat(recipe.directions)
        .concat(_.shuffle([' All Done! like, share, enjoy!',' That does it, enjoy!',' I hope you enjoy!']).pop());
        return speech;


		// let x = new bot.strain(q);
		// if(x) return x.info();
		return done;
	}
	return "Okay! Try asking me how to make " + _.shuffle(Recipes).pop().concat('.');	
}
function help() {
	return "You can ask me how to cook! You can ask me to describe any particular strain. You may also ask for a strains flavor, effects, or benefits. For jokes say, tell me a joke, for facts, say, tell me a fact. For all strains, say, list all strains. For all hybrid strains say, list all hybrids, for all sativa strains say list all sativa, for random say list random. For what’s happening in the community, ask me for a community update.";
}

function random() {
	let strain = _.shuffle(StrainUtil.allStrains()).pop(), 
	x = new bot.strain(strain),
	res = 'I have something in mind. '.concat(strain);
    return x.info(res);
}
function joke() {
	let success = _.shuffle(Strings.listening).pop(),
	j = _.shuffle(Jokes).pop();
	j = (success).concat(" ").concat(j);
	j = j.replace(/<break time='900ms'\/>/g, '');
	j = j.replace(/<break time='500ms'\/>/g, '');
	return j.replace(/<break time='300ms'\/>/g, '')
}
function fact() {
	let success = _.shuffle(Strings.listening).pop(),
	j = _.shuffle(Facts).pop();
	j = (success).concat(" ").concat(j);
	j = j.replace(/<break time='900ms'\/>/g, '');
	j = j.replace(/<break time='500ms'\/>/g, '');
	return j.replace(/<break time='300ms'\/>/g, '')
}
function community() {
	let update = [
 		"Start-up company Sto-ny Hill Corporation, led by Damian Marley, has raised US$750,000 to finance its sale of cannabis products.",
		"Orlando's first medical marijuana dispensary is set to open in just a few weeks.",
		"More than a year after federal drug agents seized just over $100,000 in cash from James Slatic and his family following a raid on his Clairemont Mesa medical marijuana business, criminal charges have yet to be filed.",	
		"Colorado's Governor, John Hickenlooper opposed Pot Legalization in 2012.  Now he is ready to defend it. Hickenlooper says he has a duty to resist federal interference.",
		"Cannabis possession and sales are illegal under federal law, but Nevada voters decided in November to allow people age 21 or older to use it recreationally, becoming one of eight states to do so.",
		"Kansas backward, mean-spirited stance on medicinal cannabis has gone on too long. It’s time to stop dismissing the possible good and show more compassion for suffering patients in our midst - Garden City Telegram",
		"The legality of cannabis varies from state to state in the U.S. In some states, both medical marijuana and recreational use of the drug is legal. In other states, both uses are illegal.",
	]
	return "Coming soon to Google Assistant. ".concat(_.shuffle(update).pop().concat(' This community update was brought to you by Ganjly.com! Ganjly is your #1 source for news on the cannabis industry.'))
}
function business() {
	let phrase = [
		'Thank you for using my CCBExpo Demo. I hope you are excited about powering your Cannabis Business with my Business Conversation API.',
		'Alexa, the voice service that powers Echo, can now power your Cannabis Business.',
	]
	return _.shuffle(phrase).pop()
}
function list(req) {
	let types = ['hybrid','sativa','indica', 'all', 'random'];
	let out, type = match(req, types);
	if(!type) return 'You can try saying list '.concat(_.shuffle(types).pop()).concat(' strains.')
	if(type == 'all') { out = _.shuffle(StrainUtil.allStrains()); }
	else if(type == 'random') { out = _.shuffle(StrainUtil.allStrains()); }
	else { out = _.shuffle(StrainUtil.listOfStrains(type)); }
	if(type != 'all') out = out.slice(0,26);
	return out.toString().concat('.').replace(/,/g, ', ');	
}
function launch() {
	return "Hi! You can now ask for suggestions by saying, suggest weed good for, followed by a symptom or effect. For help with what you can say, say help. Listening Mode Enabled.";
}
function search(req) {
	let q = match(req,allStrains);
	if(q){
		q = q.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})
		let x = new bot.strain(q);
		if(x) return x.info();
	}
	return "I could not find that information. But I am always learning. Please try again later!";	
}
function medical(req) { 
	let q = match(req, allStrains);
	if(q){
		q = q.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})
		let x = new bot.strain(q);
		return x.medical();
	}
	return "I could not find that information. But I am always learning. Please try again later!";
}
function effects(req) { 
	let q = match(req,allStrains);
	if(q){
		q.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})
		let x = new bot.strain(q);
		return x.effects(); 
	}
	return "I could not find that information. But I am always learning. Please try again later!";
}
function flavors(req) { 
	let q = match(req,allStrains);
	if(q){
		q = q.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})
		let x = new bot.strain(q);
		return x.flavors();
	}
	return "I could not find that information. But I am always learning. Please try again later!";
}
function type(req) { 
	let q = match(req,allStrains);
	if(q){
		q = q.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})
		let x = new bot.strain(q);
		return x.type();
	}
	return "I could not find that information. But I am always learning. Please try again later!";
}

function match(test,all) {
    let match;
    for (let i = 0; i < all.length; i++) {
        let x = all[i].toLowerCase(),
        control = test.toLowerCase();
        if (control.indexOf(x) != -1) {
            match = (x);
            break;
        }
    }
    return match || false;
}



module.exports = botAction;
var bip39 = require('bip39')
var potcoin = require("./vendor/potcoinjs-lib") 
var bitcoin = require("bitcoinjs-lib") 
var mnemonic = 'tonto collar meta tres soldado moneda macho ayuda sonido retiro invierno lagarto' // code.toString()
var seed = bip39.mnemonicToSeed(mnemonic)

var bitRoot = bitcoin.HDNode.fromSeedBuffer(seed)
var potRoot = potcoin.HDNode.fromSeedBuffer(seed)

// 1st receive address
console.log(bitRoot.derivePath("m/0'/0/0").getAddress(bitcoin.networks.bitcoin).toString())
// 1st change address
console.log(bitRoot.derivePath("m/0'/1/0").getAddress(bitcoin.networks.bitcoin).toString())

// 1st receive address
console.log(potRoot.derivePath("m/0'/0/0").getAddress(potcoin.networks.bitcoin).toString())
// 1st change address
console.log(potRoot.derivePath("m/0'/1/0").getAddress(potcoin.networks.bitcoin).toString())
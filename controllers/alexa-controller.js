"use strict";

let config = require("alexa-app"),
playbackIntents = require("../intents/echo/playbackIntents"),
popular = require("../intents/echo/popularIntent"),
about = require("../intents/echo/aboutIntent"),
near = require("../intents/echo/nearbyIntent"),
article = require("../intents/echo/articleIntent"),
search = require("../intents/echo/searchIntent"),
searchBy = require("../intents/echo/searchByIntent"),
hello = require("../intents/echo/hi"),
flavors = require("../intents/echo/flavorsIntent"),
effects = require("../intents/echo/effectsIntent"),
medical = require("../intents/echo/medicalIntent"),
help = require("../intents/echo/helpIntent"),
list = require("../intents/echo/listIntent"),
response = require("../intents/echo/responseIntent"),
interview = require("../intents/echo/interview"),
stop = require("../intents/echo/stopIntent"),
joke = require("../intents/echo/jokeIntent"),
fact = require("../intents/echo/factIntent"),
random = require("../intents/echo/randomIntent"),
recipe = require("../intents/echo/recipeIntent"),
business = require("../intents/echo/businessIntent"),
type = require("../intents/echo/typeIntent"),
launch = require("../intents/echo/launchIntent"),
profile = require("../intents/echo/profileIntent"),
rewards = require("../intents/echo/rewardsIntent"),
locate = require("../intents/echo/strainLocateIntent"),
reserve = require("../intents/echo/reserveIntent"),
dispensaryList = require("../intents/echo/dispensaryListIntent"),
storeHours = require("../intents/echo/storeHoursIntent"),
deliveryIntent = require("../intents/echo/deliveryIntent"),
dealsList = require("../intents/echo/dealsListIntent"),
localDeals = require("../intents/echo/localDealsIntent"),
hasStrain = require("../intents/echo/hasStrainIntent"),
priceQuantity = require("../intents/echo/priceQuantityIntent"),
legal = require("../intents/echo/legalIntent"),
coin = require("../intents/echo/potcoinIntent"),
connections = require("../intents/echo/connectionsResponse"),
event = require("../intents/echo/eventIntent"),
word = require("../intents/echo/wordIntent"),
anyConnections = require("../intents/echo/anyConnections"),
anyMessages = require("../intents/echo/anyMessages"),
viewProfile = require("../intents/echo/viewProfile"),
dislike = require("../intents/echo/dislikeIntent"),
like = require("../intents/echo/likeIntent"),
editDiscovery = require("../intents/echo/editDiscovery"),
editProfile = require("../intents/echo/editProfile"),
eDiscovery = require("../intents/echo/enableDiscovery"),
dDiscovery = require("../intents/echo/disableDiscovery"),
findBuddy = require("../intents/echo/findBuddy"),
trans = require("../intents/echo/completeTransaction"),
shop = require("../intents/echo/shop"),
cancel = require("../intents/echo/cancelPremiumSubscriptionIntent"),
next = require("../intents/echo/next");
var WooCommerceAPI = require('../vendor/woocommerce-api');
var WooCommerce = new WooCommerceAPI({
  url: 'http://box5665.temp.domains/~weedbud1/',
  consumerKey: 'ck_c880232126cfbf9ac404af79b339d2fc40a1184c',
  consumerSecret: 'cs_415f6ab4ec002a09f6def05f7fa5840397eef52f',
  wpAPI: true,
  version: 'wc/v1'
});

module.exports = function (app,session) {
    let v1 = new config.app("echo/v1");
    launch(v1, session);    
    connections(v1, session);    
    trans(v1, session);    
    shop(v1, session);    
    cancel(v1, session);    
    next(v1, session);
    eDiscovery(v1, session);
    dDiscovery(v1, session);
    findBuddy(v1, session);
    editDiscovery(v1, session);
    editProfile(v1, session);
    dislike(v1, session);
    like(v1, session);
    viewProfile(v1, session);
    anyMessages(v1, session);
    anyConnections(v1, session);
    event(v1, session);
    coin(v1, session);
    word(v1,  session);
    popular(v1, session);
    legal(v1, session);
    article(v1, session);
    dispensaryList(v1, session);
    storeHours(v1, session);
    deliveryIntent(v1, session);
    dealsList(v1, session);
    localDeals(v1, session);
    hasStrain(v1, session);
    priceQuantity(v1, session);
    reserve(v1, session);
    locate(v1, session);
    playbackIntents(v1, session);
//    amazonResume(v1);
//    amazonPause(v1);
    about(v1, session);
    near(v1, session);
    search(v1,session);
    searchBy(v1, session);
    hello(v1, session);
    flavors(v1, session);
    effects(v1, session);
    medical(v1, session);
    v1 = help(v1, session);
    list(v1, session);
    response(v1, session);
    interview(v1, session);
    stop(v1, session);
    joke(v1, session);
    fact(v1, session);
    random(v1, session);
    recipe(v1, session);
    business(v1, session);
    type(v1, session);
    profile(v1, session);
    rewards.action(v1);
    v1.pre = function(req,res) {
        session.activityTracker(req,res);
        session.geoLocate(req,res)
        let intent = req.data.request.intent
        console.log(intent)
        let attrs = req.sessionDetails.attributes
        if(attrs && intent) {
            //console.log('attrs.shopping', attrs.shopping)
            if(attrs.shopping == true && intent.name != 'ResponseIntent' ) {
                if(attrs.action == 'alexaApp.paymentMethod' && intent.name == 'PotCoinIntent') req.session('data', 'potcoin')
                intent.name = 'ResponseIntent'
            }
        }
        

    };

    v1.express({ expressApp: app });

    return app;
};

"use strict";

let ActionsSdkAssistant = require('actions-on-google').ActionsSdkAssistant,
	logger = require("../utils/logger"),
	main = require("../intents/google/launchIntent"),
	search = require("../intents/google/searchIntent").search,
	searchIntent = require("../intents/google/searchIntent").googleAction,
	medical = require("../intents/google/medicalIntent"),
	business = require("../intents/google/businessIntent"),
	community = require("../intents/google/communityIntent"),
	effects = require("../intents/google/effectsIntent"),
	fact = require("../intents/google/factIntent"),
	flavor = require("../intents/google/flavorsIntent"),
	response = require("../intents/google/responseIntent"),
	help = require("../intents/google/helpIntent"),
	joke = require("../intents/google/jokeIntent"),
	random = require("../intents/google/randomIntent"),
	type = require("../intents/google/typeIntent"),
	recipe = require("../intents/google/recipeIntent"),
	list = require("../intents/google/listIntent");


/*** Intent Handler */
function route(key) {
	let intent = undefined;
	switch (key) {
		case 'searchIntent':
			intent = true;
			break;
		case 'launchIntent':
			intent = true;		
			break;
		case 'listIntent':
			intent = true;		
			break;
		case 'medicalIntent':
			intent = true;		
			break;
		case 'businessIntent':
			intent = true;		
			break;
		case 'communityIntent':
			intent = true;		
			break;
		case 'effectsIntent':
			intent = true;		
			break;
		case 'factIntent':
			intent = true;		
			break;
		case 'flavorIntent':
			intent = true;		
			break;
		case 'helpIntent':
			intent = true;		
			break;
		case 'jokeIntent':
			intent = true;		
			break;
		case 'randomIntent':
			intent = true;		
			break;
		case 'typeIntent':
			intent = true;		
			break;
		case 'recipeIntent':
			intent = true;
			break;
		default:
			intent = false;
	}
	return intent;
}
function actionHandler(assistant,intent) {
	switch (intent) {
		case 'searchIntent':
			search(assistant);
			break;
		case 'launchIntent':
			search(assistant);
			break;
		case 'listIntent':
			response(assistant);
			break;			
		case 'medicalIntent':
			medical(assistant);
			break;
		case 'businessIntent':
			business(assistant);
			break;
		case 'communityIntent':
			community(assistant);
			break;
		case 'effectsIntent':
			effects(assistant);
			break;
		case 'factIntent':
			fact(assistant);
			break;
		case 'flavorIntent':
			flavor(assistant);
			break;
		case 'helpIntent':
			help(assistant);
			break;
		case 'jokeIntent':
			joke(assistant);
			break;
		case 'randomIntent':
			random(assistant);
			break;
		case 'typeIntent':
			type(assistant);
			break;
		case 'recipeIntent':
			recipe(assistant);
			break;
		default:
			return false;
	}
}

/*** Raw Input   */
function process(assistant) {
	let intent = assistant.getDialogState().intent;
	logger.log(intent);
	if (intent == null) {
		assistant.tell('Thank you for using Weed Buddy, goodbye!');
	} else {
		actionHandler(assistant,intent);
	}
}

/*** Home Assistant  */
function controller(request, response) {
	const assistant = new ActionsSdkAssistant({
		request: request,
		response: response
	});
	let actions = new Map();
	actions.set(assistant.StandardIntents.MAIN, main);
	actions.set(assistant.StandardIntents.TEXT, process);
	actions.set("RECIPE", recipe);
	actions.set("LIST", list);
	actions.set("SEARCH", searchIntent);
	assistant.handleRequest(actions);
}

/*** Express Context Binding */
function application(app) {
	app.post('/google', controller);
	return app;
}
exports.app = application;
exports.route = route;
exports.action = actionHandler;
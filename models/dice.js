class Dice {
	constructor(sides) {
		this.sides = sides;
	}
	roll() {
		var randomNumber = Math.floor(Math.random() * this.sides) + 1;
		return randomNumber;
	}
}

module.exports = Dice;

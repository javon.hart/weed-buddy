let mongoose = require('mongoose');
mongoose.Promise = global.Promise
let schema = mongoose.Schema({
	user_id: String,
	updates: Array
});
module.exports = mongoose.model('Update', schema);

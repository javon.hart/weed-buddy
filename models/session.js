let child = require('child_process');
let MemoryStorage = require('memorystorage');
let Local = require('./local');
let _ = require('underscore');
let Words = require("../res/words"),
model; 


class Session {
    constructor() {
    	console.log('init session')
        this.mem = new MemoryStorage('weed-buddy');    
        this.init();
    }
    data() {
    	return this.mem.getItem('data');
    }
    getStore() {
        return this.mem.getItem('data');
    }
    resetStore() {
        this.mem = new MemoryStorage('weed-buddy');
    }
    setStore(data) {
		this.mem.setItem('data', data)
    }
    save() {
    	model = this;
    	child.fork(__dirname + '/../bin/initialize')
	    .on('message', function() { model.init() });
    }
    init() {
    	model = this;
		Local.findOne({}, function(err, local) {
	        if (err) {
	            console.log(err);
	        } else if(local) {
	        	console.log('finished init')
	            let data = JSON.parse(local.data);
            	model.mem.setItem('data', data);
    	        model.mem.setItem('word',_.shuffle(Words).pop())
	        }
	    });
    }
}

module.exports = Session;

"use strict";
let StrainUtil = require('../utils/strain'),
  Core = require('../utils/core'),
  filters = require('../utils/filters'),
  _ = require('underscore'),
  Strings = require('../res/strings'),
  Dice = require('./dice'),
  images = require('../res/images'),
  filter = filters.filter;
class Strain {
  constructor(id) {
    this.body = StrainUtil.getStrain(id);
    this.info = function(req, res, user) {
      if (this.body.description) {
        let currentUser = user;
        let assistant = this.assistant;
        let script = ''
        if (this.body.percentType && this.body.percentType != '?') {
          script += this.body.percentType + ' '
        } else {
          script += this.body.type.trim() + ' <break time="600ms"/>'
        }
        if (this.body.time && this.body.time != '?') {
          script += '<break time="250ms"/> Best used in the ' + this.body.time + '. '
        }
        script += this.body.description.trim().replace(/&/g, ' and ') + '<break time="250ms"/>';
        if (this.body.thc && this.body.thc != '?') script += '<break time="250ms"/> THC <break time="250ms"/>' + this.thc(this.body.thc) + '.'
        if (this.body.cbd && this.body.cbd != '?') script += '<break time="250ms"/> CBD <break time="250ms"/>' + this.cbd(this.body.cbd) + '.'
        if (this.body.difficulty && this.body.difficulty != '?' && false) script += '<break time="600ms"/> typically ' + this.body.name + ' is ' + this.body.difficulty + ' to grow, '
        if (this.body.height && this.body.height != '?' && false) {
          if (this.body.height.includes('>')) {
            script += '<break time="600ms"/>On Average it grows greater than ' + this.body.height.replace('>', '') + ' inches tall'
          } else if (this.body.height.includes('<')) {
            script += '<break time="600ms"/>On Average it grows less than ' + this.body.height.replace('>', '') + ' inches tall'
          } else {
            script += '<break time="600ms"/>On Average it grows between ' + this.body.height.split(',')[0] + ' and ' + this.body.height.split(',')[1] + ' inches tall'
          }
        }
        if (this.body.yield && this.body.yield != '?' && false) {
          script += '<break time="600ms"/> and yields between ' + this.body.yield.split(',')[0] + ' and ' + this.body.yield.split(',')[1] + ' ounces per square foot'
        }
        if (this.body.flowering && this.body.flowering != '?' && false) {
          script += '<break time="600ms"/>You can expect ' + this.body.name + ' to flower in ' + this.body.flowering.split(',')[0] + ' to ' + this.body.flowering.split(',')[1] + ' weeks'
        }
        if (assistant) {
          assistant.tell("<speak>".concat(script).concat("</speak>"));
        } else {
          let roll = new Dice(6).roll();
          res.card({
            type: "Standard",
            title: this.body.name.trim() + ' | ' + this.body.type,
            text: this.body.description.trim()
          }).say(script).session('lastIntent', 'search')
          if (currentUser) {
            if (!currentUser.likes.includes(this.body.name.trim())) {
              if (roll != 6) {
                res.session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);
              } else {
                res.session('action', 'alexaApp.like').session('strain', this.body.name.trim()).say(Strings.halfSecondBreak + 'Do you like ' + this.body.name.trim() + '?').shouldEndSession(false);
              }
            } else {
              res.session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);
            }
          } else {
            res.session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);
          }
        }
      } else {
        let speech = "I couldn't find a description of " + this.body.name + ", But I am always learning. Please try again later!";
        res.say(speech).session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);        
      }
    };
    this.effects = function(res) {
      let assistant = this.assistant,
        searchByPrompt = '',
        speech;
      console.log(this.body.effects)
      if (this.body.effects) {
        speech = filter(Core.toProperCase(this.body.effects));
        speech = speech.trim();
        if (!speech) {
          speech = "I could not find the effects of " + this.body.name.trim() + ". But I am always learning. Please try again later!";
        } else {
          speech = 'The most common effects of ' + this.body.name.trim() + ' are ' + speech.concat('.');
          searchByPrompt = ".<break time=\'300ms\'/> To search by effect, ask Weed Buddy to search strains that make me feel, followed by an effect.";
        }
        if (assistant) {
          assistant.tell(speech);
        } else {
          res.say(speech).card({
            type: "Standard",
            title: this.body.name.trim() + ' | ' + this.body.type,
            text: speech.concat(searchByPrompt)
          }).session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);
        }
      } else {
        speech = "I couldn't find the effects of " + this.body.name + ", But I am always learning. Please try again later!";
        res.say(speech).session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);
      }
    };
    this.flavors = function(res) {
      let assistant = this.assistant,
        searchByPrompt = '',
        speech;
      if (this.body.flavors) {
        speech = filter(Core.toProperCase(this.body.flavors));
        speech = speech.trim();
        if (!speech) {
          speech = "I could not find those flavors. But I am always learning. Please try again later!";
        } else {
          speech = 'Consumer reviews describe ' + this.body.name.trim() + ' as having ' + speech.concat(' flavors.');
          searchByPrompt = ".<break time=\'300ms\'/> To search by flavor, ask Weed Buddy to search for sweet flavored strains.";
        }
        if (assistant) {
          assistant.tell(speech);
        } else {
          res.say(speech).card({
            type: "Standard",
            title: this.body.name.trim() + ' | ' + this.body.type,
            text: speech.concat(searchByPrompt)
          }).session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);
        }
      } else {
        speech = "I couldn't find the flavors of " + this.body.name + ", But I am always learning. Please try your search again later!";
        res.say(speech).session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);
      }
    };
    this.medical = function(res) {
      let assistant = this.assistant,
        searchByPrompt = '',
        speech;
      if (this.body.medical) {
        speech = filter(this.body.medical);
        speech = speech.trim();
        if (!speech) {
          speech = "I could not find that information. But I am always learning. Please try again later!";
        } else {
          speech = 'Consumer reviews say ' + this.body.name + ' is good for ' + speech;
          searchByPrompt = "<break time=\'300ms\'/> To search by symptom, ask Weed Buddy to search strains good for, followed by a symptom.";
        }
        if (assistant) {
          assistant.tell(speech);
        } else {
          res.say(speech).card({
            type: "Standard",
            title: this.body.name.trim() + ' | ' + this.body.type,
            text: speech.concat(searchByPrompt)
          }).session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);
        }
      } else {
        speech = "I couldn't find the benefits of " + this.body.name + ", But I am always learning. Please try that one again later!";
        res.say(speech).session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);
      }
    };
    this.type = function(res) {
      let assistant = this.assistant;
      let speech, type;
      type = this.body.type;
      if (type) {
        speech = this.body.name + '<break time="600ms"/>' + type;
      } else {
        speech = "I could not find that information. But I am always learning. Please try again later!";
      }
      if (assistant) {
        assistant.tell("<speak>".concat(speech).concat("</speak>"));
      } else {
        res.say(speech).card({
          type: "Standard",
          title: this.body.name.trim() + ' | ' + this.body.type,
          text: this.body.description.trim()
        }).session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);
      }
    };
  }
  setAssistant(assistant) {
    this.assistant = assistant;
  }
  thc(val) {
    if (typeof val == 'number') val = (val * 100) + '%'
    if (typeof val == 'string') {
      if (val.indexOf('%') == -1) val = val + '%'
    }
    return val
  }
  cbd(val) {
    if (typeof val == 'number') val = (val * 100) + '%'
    if (typeof val == 'string') val = val.replace('<', 'less than ')
    return val
  }
}
module.exports = Strain;
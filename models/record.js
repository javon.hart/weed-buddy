let mongoose = require('mongoose'),
schema = mongoose.Schema({
    name: String,
    type: String,
    flavors: String,
    effects: String,
    medical: String,
    description: String
});
exports.module = mongoose.model('Strain', schema);


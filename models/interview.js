'use strict';

let mongoose = require('mongoose'),
schema = mongoose.Schema({
    url: String,
    created_at: Date,
});
module.exports = mongoose.model('Interview', schema);

class Response {
    constructor(currentUser, req, res, attrs, input, yes, no, app, user) {
        this.currentUser = currentUser;
        this.req = req;
        this.res = res;
        this.attrs = attrs;
        this.input = input;
        this.yes = yes;
        this.no = no;
        this.app = app;
        this.user = user;
    }
}

module.exports = Response;

let mongoose = require('mongoose'),schema = mongoose.Schema({
    text: String,
    created_at: Date
});

module.exports = mongoose.model('Community', schema);


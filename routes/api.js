'use strict';
let Meme = require("../models/meme"),
	Interview = require("../models/interview"),
	Community = require("../models/community"),
	Strains = require("../res/strains"),
	strains = Strains,
	Company = require("../models/company"),
	_ = require("underscore"),
	Name = require("../models/name"),
	router = require('express').Router(),
	Twitter = require("../utils/twitter"),
	basicAuth = require('basic-auth');
let fs = require('fs');	
					
// or more concisely
var sys = require('sys')
var exec = require('child_process').exec;
function puts(error, stdout, stderr) { sys.puts(stdout) }
let auth = function(req, res, next) {
	function unauthorized(res) {
		res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
		return res.send(401);
	}
	let user = basicAuth(req);
	if (!user || !user.name || !user.pass) {
		return unauthorized(res);
	}
	if (user.name === 'admin' && user.pass === 'admin') {
		return next();
	} else {
		return unauthorized(res);
	}
};
let stripe = require("stripe")("sk_test_V85uyqAMQ2QaFJxNZ4YGsJ1r");
module.exports = function() {
	router.get('/strains', function(req,res){
		res.render('strain')
	})
	router.get('/new', function(req,res){
		res.render('new')
	})	
	router.get("/1834b2f6-e860-4007-898d-56515bb6bd46", function(req, res) {
		Company.findOne({}, function(err, doc) {
			if (err) {
				console.log(err);
				res.json(500);
			} else {
				let index;
				if (!doc) index = 200;
				else index = doc.name;
				res.render('businessTest', {
					data: index
				});
			}
		});
	});
	router.get("/expo", function(req, res) {
		res.redirect("https://bitly.com/enableskill");
	});
	router.get("/communityCreate", auth, function(req, res) {
		Twitter.update(req, res);
	});
	router.get("/community", function(req, res) {
		Twitter.post(req, res);
	});
	router.get("/", function(req, res) {
		res.json("200");
	});
	router.get("/purchase", function(req, res) {
		res.render('purchase');
	});
	router.get("/purchase/*", function(req, res) {
		let body = req.query;
		let token = body.stripeToken; // Using Express
		let cost;
		if (token) {
			stripe.customers.create({
				description: 'Customer for '.concat(body.stripeEmail),
				email: body.stripeEmail // obtained with Stripe.js
			}, function(err, customer) {
				let type = req.originalUrl.split('/').pop()[0];
				if (type == 'm') {
					cost = '27500';
					stripe.subscriptions.create({
						customer: customer.id,
						source: token,
						items: [{
							plan: "Regular",
						}, ]
					}, function(err) {
						if (err) {
							console.log(err);
							res.json("Sorry we had trouble processing your payment. Please go back and try again.");
						} else {
							res.redirect('/thankyou');
						}
					});
				} else if (type == 'a') {
					cost = '302500';
					stripe.charges.create({
						amount: cost,
						currency: "usd",
						source: token,
						customer: customer.id
					}, function(err) {
						if (err) {
							console.log(err);
							res.json("Sorry we had trouble processing your payment. Please go back and try again.");
						} else {
							res.redirect('/thankyou');
						}
					});
				}
			});
		} else {
			res.render('purchase');
		}
	});
	router.get("/name", function(req, res) {
		Name.findOne({}, function(err, doc) {
			if (err) {
				console.log(err);
				res.json(500);
			} else {
				let index;
				if (!doc) index = 200;
				else index = doc.index;
				res.render('nameService', {
					ref: JSON.stringify(index)
				});
			}
		});
	});
	router.get("/editCommunity", auth, function(req, res) {
		Community.findOne({}, function(err, doc) {
			if (err) {
				console.log(err);
				res.json(500);
			} else {
				let index;
				if (!doc) index = 200;
				else index = doc.text;
				res.render('communityService', {
					community: index
				});
			}
		});
	});

	router.post("/editStrain", function(req, res) {
		let strain = (req.body)
		console.log(strain._id)
		console.log(Strains.length)
		console.log(strain)
		// strains.filter(x => {return x._id === strain._id })[0] = strain;
		// console.log(strains.filter(x => {return x._id === strain._id })[0])
		for (var i = 0, l = Strains.length; i < l; i++) {
		    if (strains[i]._id === strain._id) {
		        strain.ratified = true;
		        strains[i] = strain;
				fs.writeFile(__dirname + "/../res/strains.json", JSON.stringify(strains), function(err) {
				    if(err) {
				        return console.log(err);
				    }
				    console.log("file 1 was saved!");

				});
				fs.writeFile(__dirname + "/../public/strains.js", "var strains = " + JSON.stringify(strains), function(err) {
				    if(err) {
				        return console.log(err);
				    }
				    console.log("file 2 was saved!");
				    exec("git add .; git commit -m '" + strain.name + "'; git push", puts);
				});				
		        break;
		    }
		}
	    res.json(200)
	})
	
	router.post('/strain', function(req, res) {
		let strain = (req.body) 
		strains.push(strain)
		fs.writeFile(__dirname + "/../res/strains.json", JSON.stringify(strains), function(err) {
		    if(err) {
		        return console.log(err);
		    }
		    console.log("file 1 was saved!");

		});
		fs.writeFile(__dirname + "/../public/strains.js", "var strains = " + JSON.stringify(strains), function(err) {
		    if(err) {
		        return console.log(err);
		    }
		    console.log("file 2 was saved!");
		    exec("git add .; git commit -m '" + strain.name + "'; git push", puts);
		});	
		let names = [];
		_.each(Strains, function(strain) {
			names.push(strain.name)
		})

		fs.writeFile(__dirname + "/../res/all_strains.js", "module.exports = " +  JSON.stringify(names), function(err) {
		    if(err) {
		        return console.log(err);
		    }
		    console.log("all_strains");
		});

		res.json(200)
	});

	router.post("/editInterview", function(req, res) {
		try {
			Interview.remove({}, function(err) {
				if (err) {
					console.log(err);
				}
			});
			let interview = req.body.data;
			console.log(req.body)
			let newI = new Interview();
			newI.url = interview;
			newI.created_at = new Date();

			newI.save(function(err) {
				if (err) {
					res.json(500);
				}
				res.json(200);
			});
		} catch (e) {
			console.log(e);
		}
	});

	router.get("/interview", auth, function(req, res) {
		Interview.findOne({}, function(err, url) {
			if (err) {
				console.log(err);
				res.json(500);
			} else  {
				if(!url) url = { url: ''}
				res.render('interviewLink', {
					interview: url.url
				});
			}
		});
	});

	router.post("/name", function(req, res) {
		let out = 200;
		try {
			let data = req.body.data;
			let name = new Name();
			data = JSON.parse(data);
			if (typeof data == 'object') {
				Name.remove({}, function(err) {
					if (err) {
						console.log(err);
					}
				});
				name.index = data;
				name.save(function(err) {
					if (err) console.log(err);
				});
			} else {
				out = 'Malformed Data';
			}
		} catch (e) {
			console.log(e);
			out = e;
		} finally {
			res.json(out);
		}
	});
	router.post("/createBusiness", function(req, res) {
		try {
			Company.remove({}, function(err) {
				if (err) {
					console.log(err);
				}
			});
			let name = req.body.data,
				business = new Company();
			business.name = name;
			business.save(function(err) {
				if (err) {
					res.json(500);
				}
				res.json(200);
			});
		} catch (e) {
			console.log(e);
		}
	});
	router.post("/editCommunity", function(req, res) {
		try {
			Community.remove({}, function(err) {
				if (err) {
					console.log(err);
				}
			});
			let text = req.body.data;
			let community = new Community();
			community.text = text;
			community.created_at = new Date();
			community.save(function(err) {
				if (err) {
					res.json(500);
				}
				res.json(200);
			});
		} catch (e) {
			console.log(e);
		}
	});
	router.get("/meme", function(req, res) {
		let meme = new Meme();
		Meme.remove({}, function(err) {
			if (err) {
				console.log(err);
			}
		});
		meme.url = req.query.q;
		meme.created_at = new Date();
		meme.save(function(err) {
			console.log(err);
		});
		res.json(200);
	});
	return router;
};
'use strict';

const IntentID = 'googleApp.community';
let Analytics = require('../../utils/analytics'),
LogUtil = require('../../utils/logger'),
Post = require('../../models/community').post,
Session = require('../../utils/session');

function constructor(assistant) {
    let post = new Post(), 
    feed = post.getFeed();
    assistant.tell("<speak>".concat(feed).concat("</speak>"));
    LogUtil.log(IntentID);
    Analytics.event(IntentID, assistant.getConversationId(), Session.time()).send();

}

module.exports = constructor;

let LogUtil = require('../../utils/logger');
let Session = require('../../utils/session');
let Strings = require('../../res/strings');
let Jokes = require('../../res/jokes');
let Analytics = require('../../utils/analytics');
let _ = require('underscore');
let memeID;
try {
	let Meme = require('../../models/meme');
	Meme.findOne({}, {}, {
		sort: {
			'created_at': -1
		}
	}, function(err, id) {
		if(id) memeID = id.url.replace(/"/g, '').replace(/'/g, '');
	});
} catch (e) {
	console.log(e);
}

function constructor(assistant) {	
	if(!memeID) memeID = "https://s3.amazonaws.com/weedbuddy/meme613.png";
	LogUtil.log(memeID);
	let token = "googleApp.joke",
    success = _.shuffle(Strings.listening).pop(),
	j = _.shuffle(Jokes).pop();
	Analytics.event(token, assistant.getConversationId(), Session.time()).send();
	let speech = success + '<break time=\'500ms\'/>' + j;
	assistant.tell("<speak>".concat(speech).concat("</speak>"));
	LogUtil.log(token);
	LogUtil.log(j);
}
module.exports = constructor;

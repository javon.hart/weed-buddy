let _ = require('underscore'),
Analytics = require('../../utils/analytics'),
Strings = require('../../res/strings'),
Jokes = require('../../res/jokes'),
filters = require('../../utils/filters'),
Session = require('../../utils/session'),
Strain = require('../../models/strain'),
StrainUtil = require('../../utils/strain'),
LogUtil = require('../../utils/logger'),
app = require('../../controllers/google-controller');

function fufill(assistant) {
	let state = assistant.data = { intent: 'launchIntent' };
    Analytics.event("googleApp.launch", assistant.getConversationId(), Session.time()).send();
    LogUtil.log("App Launched: ", new Date().toString());
    let reprompt = function(){ return _.shuffle(Jokes).pop() + "<break time='1000ms'/> for help with what you can say, say help."; },
    start = '<speak>'+ Strings.shoutout + "<break time='1000ms'/> For help with what you can say, say help.<break time='1000ms'/>" + Strings.start + '</speak>',
    inputPrompt = assistant.buildInputPrompt(true, start, [reprompt(), reprompt(), reprompt()]);
    assistant.ask(inputPrompt, state);
}

function handle(assistant,q) {
	if (q) {
    	q = q.capitalize();
    	q = filters.name(q);
    	if(StrainUtil.hasStrain(q)){
			Analytics.event("googleApp.search# " + q, assistant.getConversationId(), Session.time()).send();
            let x = new Strain(q);
            x.setAssistant(assistant);
            x.info();
    	} else {
    		fufill(assistant);
    	}
	} else {
		fufill(assistant);
	}
}

function constructor(assistant) {
    let q = assistant.getArgument('trigger_query');
	let intent = filters.intent(assistant.getRawInput());
	if(intent) {
		app.action(assistant,intent);
	} else {
		handle(assistant,q);
	}


   
}

module.exports = constructor;
let LogUtil = require('../../utils/logger'),
StrainUtil = require('../../utils/strain'),
Session = require('../../utils/session'),
Analytics = require('../../utils/analytics'),
 _ = require('underscore');

function reduce(array) {
   let out = "";
   _.each(array, function(str){
        out += str + ", ";
   });
   return out;
}

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

let listIntent = function(assistant){
    let attrs = assistant.getDialogState();
    let i = parseInt(attrs.index)+1, list, type = attrs.type;
    assistant.data = {};
    if(type) {
        list = StrainUtil.listOfStrains(type);
        assistant.data.type = type;
    } else if(type =='random'){
        list = _.shuffle(StrainUtil.allStrains());
    }else {
        list = StrainUtil.allStrains();
    }
    let strains = StrainUtil.strains(list), 
    prompt = reduce(strains[i]);
    let init = '<speak>Okay!<break time=\'300ms\'/>';
    assistant.data.intent = 'listIntent';
    assistant.data.index = i;
    Analytics.event("googleApp.list#" + type, assistant.getConversationId(), Session.time()).send();    
    if(Object.keys(strains).length == i) {
        assistant.tell("<speak>".concat(init) + prompt + '<break time=\'300ms\'/> All finished. Thank you. Goodbye.</speak>');
        LogUtil.log("completed list");
    } else {
        let reprompt = ' would you like to continue?';
        let inputPrompt = assistant.buildInputPrompt(true, init + prompt + reprompt.concat("</speak>"), [reprompt, reprompt, reprompt]);
        assistant.ask(inputPrompt, assistant.data);
        LogUtil.log("continue list");
    }
};

function search(assistant) {
  	let intent = assistant.getDialogState().intent,
  	input = assistant.getRawInput(),
    yes = (input == 'yes');
  	if(yes && intent == 'listIntent'){
        listIntent(assistant);
    } else {
  	    assistant.tell('<speak>It was great catching up! <break time=\'350ms\'/>Later.</speak>');
        LogUtil.log("finished list");
  	}
}

module.exports = search;

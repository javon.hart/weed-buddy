let Analytics = require('../../utils/analytics'),
LogUtil = require('../../utils/logger'),
Strings = require('../../res/strings'),
_ = require('underscore'),
RecipeUtil = require('../../utils/recipe'),
Session = require('../../utils/session');
String.prototype.toProperCase = function () {
    return this.split(' ').map(i => i[0].toUpperCase() + i.substr(1).toLowerCase()).join(' ');
};

function constructor(assistant) {
    let r = assistant.getArgument('text');
    if(r) {
        r = RecipeUtil.getRealRecipe(r);
        LogUtil.log(r);
        if(RecipeUtil.hasRecipe(r)) {
            Analytics.event("googleApp.recipe# " + r, assistant.getConversationId(), Session.time()).send();
            let speech = RecipeUtil.main(r);
            speech = "<speak>".concat(speech).concat(_.shuffle(Strings.leaving).pop()).concat("</speak>");
            assistant.tell(speech);
        }
        else {
            let token = "googleApp.recipe#fail?" + r;
            Analytics.event(token, assistant.getConversationId(), Session.time()).send();
            LogUtil.log(token);
            let speech = "I couldn't find a recipe by the name of "+r+". Please try that one again later.<break time='900ms'/>";
            speech = "<speak>".concat(speech).concat(_.shuffle(Strings.leaving).pop()).concat("</speak>");
            assistant.tell(speech);
        }
    } else {
        let token = "googleApp.recipe#all?" + r;
        Analytics.event(token, assistant.getConversationId(), Session.time()).send();
        let speech = "Ask me how to make ".concat(RecipeUtil.randomName());
        assistant.tell(speech);
    }
}

module.exports = constructor;

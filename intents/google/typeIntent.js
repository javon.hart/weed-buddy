let LogUtil = require('../../utils/logger'),
	Analytics = require('../../utils/analytics'),
	StrainUtil = require('../../utils/strain'),
	Session = require('../../utils/session'),
	Strain = require('../../models/strain'),
	filters = require('../../utils/filters'),
	Strings = require('../../res/strings');

function search(assistant) {
	let q = assistant.getArgument('text'),
		token;

	if (q) {
		q = filters.name(q);
		if (StrainUtil.hasStrain(q)) {
			token = "googleApp.type#" + q;
			Analytics.event(token, assistant.getConversationId(), Session.time()).send();
			let x = new Strain(q);
			x.setAssistant(assistant);
			x.type();
		} else {
			token = "googleApp.type#fail?" + q;
			Analytics.event(token, assistant.getConversationId(), Session.time()).send();
			assistant.tell("<speak>".concat(Strings.notFound) + q + ". But I am always learning. Please try again later! <break time='500ms'/> For all strains say, list all strains.</speak>");
		}
		LogUtil.log(token);
	}
}
module.exports = search;
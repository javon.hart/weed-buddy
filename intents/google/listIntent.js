let LogUtil = require('../../utils/logger'),
  StrainUtil = require('../../utils/strain'),
  Analytics = require('../../utils/analytics'),
  Session = require('../../utils/session'),
  _ = require('underscore');

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

function reduce(array) {
  let out = "";
  _.map(array, function(str) {
    out += (str + ", ");
  });
  return out;
}

function constructor(assistant) {
    // res.clearSession();
    let state = assistant.data = {};
    let type = assistant.getArgument('text');
    let list;
    if (type == undefined) type = 'all';    
    if (type.indexOf('random') != -1) type = 'random';    
    if (type.capitalize() == 'Hybrid' || type.capitalize() == 'Sativa' || type.capitalize() == 'Indica') {
      list = StrainUtil.listOfStrains(type);
      assistant.data.type = type;
    } else if (type == 'random') {
      list = _.shuffle(StrainUtil.allStrains());
    } else if (type == 'all') {
      list = StrainUtil.allStrains();
    }
    assistant.data.intent = 'listIntent';
    assistant.data.index = 0;
    let index = StrainUtil.strains(list),
    prompt = reduce(index[0]);
    let init = "<speak>Listing " + type + ' <break time=\'300ms\'/>';
    LogUtil.log(type);
    if (prompt) {
      Analytics.event("googleApp.list#" + type, assistant.getConversationId(), Session.time()).send();
      if (Object.keys(index).length == 1) {
        assistant.tell(init + prompt + ' All finished, goodbye');
        return;
      } else {
        let reprompt = ' would you like to continue?';
        let inputPrompt = assistant.buildInputPrompt(true, init + prompt + reprompt.concat("</speak>"), [reprompt, reprompt, reprompt]);
        assistant.ask(inputPrompt, state);
      }
    } else {
      Analytics.event("googleApp.list#fail?" + type, assistant.getConversationId(), Session.time()).send();
      assistant.tell('<speak>Sorry, I couldn\'t quite hear you.<break time=\'300ms\'/>Come again?</speak>');
    }

}
module.exports = constructor;

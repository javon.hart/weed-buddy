let LogUtil = require('../../utils/logger'),
Analytics = require('../../utils/analytics'),
Session = require('../../utils/session'),
Strings = require('../../res/strings');

function search(assistant) {
    let token = "googleApp.help";
    assistant.tell(Strings.googlePrompt);
    Analytics.event(token, assistant.getConversationId(), Session.time()).send();
    LogUtil.log(token);
}

module.exports = search;

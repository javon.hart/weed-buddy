let LogUtil = require('../../utils/logger'),
	Analytics = require('../../utils/analytics'),
	StrainUtil = require('../../utils/strain'),
	Session = require('../../utils/session'),
	filters = require('../../utils/filters'),
    Strain = require('../../models/strain'),
    app = require('../../controllers/google-controller'),
	Strings = require('../../res/strings');
String.prototype.capitalize = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
};

exports.googleAction = function(assistant) {
    let intent = filters.intent(assistant.getRawInput());
    app.action(assistant,intent);
};

function fail(assistant,q) {
    let token = "googleApp.search#fail?" + q;
    Analytics.event(token, assistant.getConversationId(), Session.time()).send();
    LogUtil.log(token);
    q = q.toLowerCase();    
    if (q.includes('the flavor')) {
        assistant.tell("For help say help. For flavors, ask me to describe the flavor of strain.");
    } else if (q.includes('the benefits')) {
        assistant.tell("For help say help. For flavors, ask me to describe the benefits of strain.");
    } else {
        assistant.tell("<speak>".concat(Strings.notFound) + q + ". But I am always learning. Please try again later! <break time='500ms'/> For all strains say, list all strains.</speak>");
    }
}

exports.search = function(assistant) {
    let q = assistant.getArgument('text') || assistant.getArgument('trigger_query');
    if (q) {
        q = StrainUtil.match(q);            
        q = filters.name(q);
        if (StrainUtil.hasStrain(q)) {
            LogUtil.log(q);
            Analytics.event("googleApp.search# " + q, assistant.getConversationId(), Session.time()).send();
            let x = new Strain(q);
            x.setAssistant(assistant);
            x.info();
        } else {
            let intent = filters.intent(q);
            if(intent) {
                app.action(assistant,intent);
            } else {
                fail(assistant, q);
            }
        }
    }
};

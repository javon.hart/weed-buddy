let _ = require('underscore'),
Analytics = require('../../utils/analytics'),
Facts = require('../../res/facts'),
Strings = require('../../res/strings'),
LogUtil = require('../../utils/logger'),
Session = require('../../utils/session');

function constructor(assistant) {
    let token = "googleApp.fact", 
    success = _.shuffle(Strings.listening).pop(),
    f = _.shuffle(Facts).pop();
    let speech = success + '<break time=\'500ms\'/>' + f;
    assistant.tell("<speak>".concat(speech).concat("</speak>"));
 //    .card({
 //        type: "Standard",
 //        title: "Fact",
 //        text: f,
 //        image: {
 //            smallImageUrl: "https://i1.wp.com/ganjly.com/wp-content/uploads/2017/02/business.png?resize=413%2C500&ssl=1",
 //        }
	// })

	LogUtil.log(token);
    Analytics.event(token, assistant.getConversationId(), Session.time()).send();
}

module.exports = constructor;

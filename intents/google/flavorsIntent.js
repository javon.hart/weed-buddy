let Analytics = require('../../utils/analytics'),
  filters = require('../../utils/filters'),
  LogUtil = require('../../utils/logger'),
  Session = require('../../utils/session'),
  Strain = require('../../models/strain'),
  StrainUtil = require('../../utils/strain'),
  Strings = require('../../res/strings');

function constructor(assistant) {
  let q = assistant.getArgument('text'), token;
  if (q) {
    q = StrainUtil.match(q);    
    q = filters.name(q);
    if (StrainUtil.hasStrain(q)) {
      let x = new Strain(q);
      token = "googleApp.flavors#" + q;
      x.setAssistant(assistant);
      x.flavors();
      Analytics.event(token, "Admin", Session.time()).send();
      LogUtil.log(token);
    } else {
      token = "alexaApp.flavors#fail?" + q;
      assistant.tell(Strings.notFound + q + ". But I am always learning. Please try again later! <break time='500ms'/> For all strains say, list all strains.");
      Analytics.event(token, assistant.getConversationId(), Session.time()).send();
      LogUtil.log(token);
    }
  }
}
module.exports = constructor;
const IntentID = 'googleApp.business';
let Analytics = require('../../utils/analytics'),
Session = require('../../utils/session');

function constructor(assistant) {
    let state = assistant.data = { intent: 'searchIntent' };
    Analytics.event(IntentID, assistant.getConversationId(), Session.time()).send();
    let business = '<speak>Google Home assistant can now power customer interaction. Storefronts, seed banks, medical doctors and delivery services! Take advantage of special early bird pricing and preorder today at weedbuddy<break time=\'150ms\'/>.io. version 1 coming february 2017.</speak>';
    assistant.tell(business,state);
}

module.exports = constructor;

let Analytics = require('../../utils/analytics'),
  filters = require('../../utils/filters'),
  LogUtil = require('../../utils/logger'),
  Session = require('../../utils/session'),
  Strain = require('../../models/strain'),
  StrainUtil = require('../../utils/strain'),
  Strings = require('../../res/strings');

function constructor(assistant) {
  let q = assistant.getArgument('text'), token;
  if (q) {
    q = StrainUtil.match(q);
    q = filters.name(q);
    if (StrainUtil.hasStrain(q)) {
      token = "googleApp.medical#" + q;
      let x = new Strain(q);
      x.setAssistant(assistant);
      x.medical();
      LogUtil.log(token);
      Analytics.event(token, assistant.getConversationId(), Session.time()).send();
    } else {
      token = "googleApp.medical#fail?" + q;
      Analytics.event(token, assistant.getConversationId(), Session.time()).send();
      assistant.tell(Strings.notFound + q + ". But I am always learning. Please try again later! <break time='500ms'/> For all strains say, list all strains.");
      LogUtil.log(token);
    }
  }
}
module.exports = constructor;
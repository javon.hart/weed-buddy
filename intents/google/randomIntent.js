let LogUtil = require('../../utils/logger'),
StrainUtil = require('../../utils/strain'),
Session = require('../../utils/session'),
Analytics = require('../../utils/analytics'),
Strain = require('../../models/strain'),
token = "googleApp.random",
_ = require('underscore');

function constructor(assistant) {
	let strain = _.shuffle(StrainUtil.allStrains()).pop(), 
	x = new Strain(strain);
	x.setAssistant(assistant);
    Analytics.event(token, assistant.getConversationId(), Session.time()).send();
    x.info();
	LogUtil.log(token);
}

module.exports = constructor;

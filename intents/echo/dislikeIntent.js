let Analytics = require('../../utils/analytics');
let Filters = require('../../utils/filters');
let StrainUtil = require('../../utils/strain');
let Session = require('../../utils/session');
let Strings = require('../../res/strings');
let User = require('../../models/user');
let _ = require('underscore');
let token = 'alexaApp.dislike'

function action(alexa, app) {
	alexa.intent("DislikeIntent", function(req, res) {
		try {
			let dislike = (req, res, user) => {
				let currentUser = user;
				let q = req.slot('q');
				let u = q;
				q = Filters.name(q, app);
				if (StrainUtil.hasStrain(q)) {
					User.findOne({
						amzn_id: currentUser.amzn_id
					}, function(err, user) {
						if (user) {
							console.log(q)
							user.likes = _.filter(user.likes, function(item) {
								return item != q;
							});
							user.save();
							console.log(200)
						}
						return false;
					});
					let script = `I've removed ${q} from your likes`;
					res.say(script).shouldEndSession(true);
					app.track(req, token, q);
				} else {
					res.say("I can't do that").shouldEndSession(true);
					app.fail(req, res, token, Strings.noQuery);
				}
			}
			return app.initialize(req, res, dislike);
		} catch (e) {
			console.log(e)
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}
module.exports = action;

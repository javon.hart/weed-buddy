let _ = require('underscore'),
	LogUtil = require('../../utils/logger'),
	Analytics = require('../../utils/analytics'),
	token = "alexaApp.stop",
	Strings = require('../../res/strings');

function constructor(alexa, app) {
	alexa.intent("AMAZON.StopIntent", function(req, res) {
		let doStop = (req, res, user) => {
			let attrs = req.sessionDetails.attributes;
			if (!(attrs.lastIntent == 'launch')) {
				let prompt = _.shuffle(Strings.out).pop();
				res.say(prompt).clearSession().shouldEndSession(true);
			}
			Analytics.event(token, app.id(req, res), app.time()).send();
			LogUtil.log(token);
		}
		return app.initialize(req, res, doStop);
	});
	return alexa;
}
module.exports = constructor;
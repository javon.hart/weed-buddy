let filters = require('../../utils/filters'),
	request = require('request-promise'),
	Session = require('../../utils/session'),
	Strain = require('../../models/strain'),
	StrainUtil = require('../../utils/strain'),
	Strings = require('../../res/strings'),
	token = 'alexaApp.cancelsub';

function action(alexa, app) {
	alexa.intent("CancelPremiumSubscriptionIntent", function(req, res) {
		try {

					let upsell = {
						type: 'Connections.SendRequest',
						name: 'Cancel',
						payload: {
							InSkillProduct: { productId: 'amzn1.adg.product.ea8da109-c1bb-4247-8d99-37099396f57c' }
						},
					    token: 'correlationToken'
					}
				 	res.directive(upsell).shouldEndSession(true);					
			
		} catch (e) {
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}
module.exports = action;

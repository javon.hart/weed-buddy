let Session = require('../../utils/session');
let Strings = require('../../res/strings');
let Word = require('../../models/word');
let Token = 'alexaApp.wordOfDay';

function action(alexa, app) {
	alexa.intent("WordIntent", function(req, res) {
		try {


            var promise = Word.findOne({}).exec();
            return promise.then(function(word) {

				app.track(req, Token);
				console.log(word)
				let script = ("Todays word of the day is " + word.title.replace(new RegExp('\t', 'g'), '').concat(Strings.oneSecondBreak).concat(word.content)).replace(new RegExp('\t', 'g'), '').replace('Back to Glossary Index Page', '');
				res.say(script).shouldEndSession(true);

            })

		} catch (e) {
			console.log(e)
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}
module.exports = action;

let Analytics = require('../../utils/analytics');
let UsersUtil = require('../../utils/users');
let Strings = require('../../res/strings');
let _ = require('underscore');
let token = 'alexaApp.findBuddy';

function action(alexa,session) {
    alexa.intent("FindBuddy", function(req, res) {
        try {
            let currentUser = session.currentUser(req);
            // has address
            if (!session.addressConsent(currentUser)) {
                try {
                    res.say(Strings.allowAddress.concat('<break time=\"500ms\"/>If you\'ve already done so please refresh the application by saying <break time=\"300ms\"/> open Weed Buddy.')).shouldEndSession(true);
                } catch (e) {
                    console.log(e);
                    Analytics.event('CONSENT_ASK_FAIL', new Date()).send();
                }
                return;
            }
            let linked = session.isLinked(req);
            if (linked) { // facebook is linked
                if (currentUser.minimumAge && currentUser.maximumAge && currentUser.genderBuddy) {
                    res.say("Okay!");
                    console.log('finding weed buddy');
                    console.log('checking discovery');
                    if (!currentUser.discovery) {
                        res.session('action', 'discovery');
                        res.say(Strings.halfSecondBreak + 'You are not discoverable. would you like to be?').shouldEndSession(false)
                        return true;
                    }
                    // let adr = {
                    //     postalCode: '98109',
                    //     city: 'Seattle',
                    //     districtOrCounty: 'WA'
                    //
                    // };
                    let matches = UsersUtil.getMatches(currentUser, session);
                    if (matches.length > 0) {
                        let firstBuddy = matches[0],
                        buddies = matches.slice(0,20).map(function(item){
                            return { name: item.name, amzn_id: item.amzn_id }
                        });
                        res.session('buddies', JSON.stringify(buddies));
                        // let pry = require('pryjs')
                        // eval(pry.it)
                        res.card({
                            type: "Standard",
                            title: firstBuddy.name,
                            text: 'user profile information',
                            image: {
                                smallImageUrl: firstBuddy.fb_image || 'https://s3.amazonaws.com/weedbuddy/logo.png' // required
                            }
                        });
                        res.session('action', 'findBuddy');
                        res.say('<audio src="https://s3.amazonaws.com/weedbuddy/connect.mp3" /> I just sent a potential buddy to the alexa app.<break time="1000ms"/> Say next to indicate you\'re not interested in ' + firstBuddy.name.trim().split(/\s+/)[0] + ' <break time="500ms"/> or connect to indicate your interest.<break time="250ms"/>').reprompt('You can say pass or connect. Do you need more time?').shouldEndSession(false)
                    } else {
                        res.say('<break time="2100ms"/>I could not find any potential buddies in your area; try adjusting your discovery settings for more results.').shouldEndSession(true)
                        return true;
                    }
                } else {
                    res.say('please say edit discovery settings').shouldEndSession(false)
                }
            } else {
                res.say(Strings.link).shouldEndSession(true);
            }
            session.track(req, token, Strings.noQuery);
        } catch (e) {
            console.log(e);
            session.fail(req, res, token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = action;
let _ = require('underscore'),
	Facts = require('../../res/facts'),
	Images = require('../../res/images'),
	Strings = require('../../res/strings'),
	Session = require('../../utils/session'),
	token = "alexaApp.fact",
	success = _.shuffle(Strings.listening).pop(),
	f = _.shuffle(Facts).pop();

function action(alexa, app) {
	alexa.intent("FactIntent", function(req, res) {
		try {
			let fact = (req, res, user) => {
				let currentUser = user;
				res.say(success).say(f).card({
					type: "Standard",
					title: "Fact",
					text: f
				}).shouldEndSession(true);
				app.track(req, token);
			}
            return app.initialize(req,res, fact);            
		} catch(e) {
			app.fail(req,res,token,Strings.criticalError,e);
		}
	});
	return alexa;
}

module.exports = action;

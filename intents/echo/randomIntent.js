let StrainUtil = require('../../utils/strain'),
	Analytics = require('../../utils/analytics'),
	Strings = require('../../res/strings'),
	Strain = require('../../models/strain'),
	token = "alexaApp.random",
	_ = require('underscore');

function constructor(alexa, app) {
	alexa.intent("RandomIntent", function(req, res) {
		try {
			let random = (req, res, user) => {
				let strain = _.shuffle(StrainUtil.allStrains()).pop(),
					x = new Strain(strain);
				res.say('I have something in mind<break time="200ms"/>').say(strain + "<break time='500ms'/>");
				x.info(req, res, user);
				app.track(req, token);
			}
			return app.initialize(req, res, random);
		} catch (e) {
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}
module.exports = constructor;

let Session = require('../../utils/session');
let Legal = require('../../utils/legal');
let Core = require('../../utils/core');
let token = 'alexaApp.legalIntent';

function action(alexa, app) {
	alexa.intent("LegalIntent",
  		function(req,res) {
			try {
				let legal = (req, res, user) => {
					let currentUser = user;
					let s = req.slot('s');
					let state = Core.capitalize(s);
					console.log(state)
					let prompt = Legal(state);
				  	res.say(prompt)
			  	    .shouldEndSession(true);				
					 app.track(req, token);
				}
				return app.initialize(req,res, legal);
			} catch(e) {
				console.log(e)
				app.fail(req,res,token,Strings.criticalError,e);
			}
		  }
	);
	return alexa;
}

module.exports = action;



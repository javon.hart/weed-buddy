let Analytics = require('../../utils/analytics');

function action(alexa, app) {
	alexa.intent("ProfileIntent",
	  function(req,res) {
	  	let user = app.currentUser(req);
        Analytics.event("alexaApp.profile", app.id(req,res), app.time()).send();
	  	res.say('Okay!');
  	    res.session('action', 'profileIntent');
	  	if(!user.country) {
  	    	res.session('step', 'country')
	  		.say('Please say your country name.')
  	    	.shouldEndSession(false);	  		
	  	} 
	  	if(user.country && !user.zip_code) {
  	    	res.session('step', 'zip_code')
	  		.say('Please say your postal code.')
  	    	.shouldEndSession(false);	  		
	  	}
	  	else if(user.zip_code && user.country) {
            res.say('Thank you for completing your profile, enjoy!')
            .shouldEndSession(true);
	  	}
	  }
	);
	return alexa;
}

module.exports = action;

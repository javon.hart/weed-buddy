let ResponseUtil = require('../../utils/response'),
	Core = require('../../utils/core'),
	Search = require('../../utils/search'),
	Strings = require('../../res/strings'),
	fuzzy = require('fuzzy'),
	_ = require('underscore');
var WooCommerceAPI = require('../../vendor/woocommerce-api');
let request = require("request-promise")

var WooCommerce = new WooCommerceAPI({
  url: 'http://box5665.temp.domains/~weedbud1/',
  consumerKey: 'ck_c880232126cfbf9ac404af79b339d2fc40a1184c',
  consumerSecret: 'cs_415f6ab4ec002a09f6def05f7fa5840397eef52f',
  wpAPI: true,
  version: 'wc/v1'
});



function search(alexa, session) {
	alexa.intent("ResponseIntent", function(req, res) {
		try {
			let attrs = req.sessionDetails.attributes

			let theResponse = (req, res, user) => {
				let attrs = req.sessionDetails.attributes,
					input = req.slot('Response'),
					q = input,
					yes = (input === 'yes' || input === 'yes I am' || input === 'yea'),
					no = (input === 'no'),
					currentUser = user,
					token = 'alexaApp.response';					
				ResponseUtil.response(currentUser, req, res, attrs, input, yes, no, session, user);
				if (attrs) {
					return ResponseUtil.router()
				} else if (q) { //  likely randomly invoked response intent handle
					// Freelance response
					console.log('handling unmarked response...........');
					Search.search(req, res, q, session);
					return;
				} else {
					console.log('come again...........?');
					token = 'endOfResponse';
					res.say(_.shuffle(Strings.leaving).pop()).shouldEndSession(true);
					session.track(req, token);
				}
			}

		    if(attrs && attrs.action == 'alexaApp.shopping') {
		        let q = req.slot('q') || req.slot('Response');
		        console.log(q)
		        return WooCommerce.get('products', function(err, data, response) {
		            var out = JSON.parse(response)
		            bigData = out.filter(function(o){ if(o.status == 'publish') return o  })
					out = out.map((o) => { return o.name })
		            let searchResults = fuzzy.filter(q, out)
		            let best = (data) => {
		            	if(!data) return false
		            	let matches = data.map((it) => { if(it.score > 50) return it })
		            	console.log('matches', matches)
		            	if(!matches) return false 
	            		return matches[0].string
		            }
		            let match = (data, q) => {
		            	if(!q) return false
						let it = _.find(data, function(item) {
							return item.name == q
						});
						console.log(it)
						if(it) return it
						else return false
		            }
		        	let get_price = (data) => {}
		        	let describe = (data, res, q) => {
						res
						.session('cart', data)
						.say('the top search result for ' + q + ' is ' + data.name + '. It\'s ' + Core.num_to_word(data.price) + ' total. Would you like to buy it?')
						.session('action', 'alexaApp.purchaseDecision')
						.shouldEndSession(false)

		        	}
		            searchResults = best(searchResults)
		            searchResults = match(bigData, searchResults)
		            console.log(searchResults)
		            if(!searchResults) 	res.say('it doesn\'t look like there are any ' + q + ' products available right now. Please try that again later.')
		        	else describe(searchResults, res, q)
		        });
		    } 

			if(attrs && attrs.action == 'alexaApp.bitcoinPayment') {
					let input = req.slot('Response'),
				 	yes = (input === 'yes' || input === 'yes I am' || input === 'yea'),
					no = (input === 'no')

			        if(no) {
			            res.say('This skill only supports bitcoin payments')
			            .shouldEndSession(true)
			        } else if(yes) {
			        	let total = attrs.cart.sale_price || attrs.cart.price

						return request({
						    method: 'POST',
						    body: {
						    	total: total,
						    	id: req.userId
						    },
						    url: 'https://blooming-oasis-15324.herokuapp.com/transaction',
						    json: true
						}, function (error, response, body) {
							console.log('body:', body); 


		    				User.findOne({
		    					amzn_id: req.userId
		    				}, function(err, user) {
		 						user.pending_transaction = body._id;
		 						user.save()
		    				});

				            res.say('I sent an invoice to the alexa app. When you\'ve sent your payment say confirm my payment to complete checkout.').card({
				                type: "Standard",
				                title: "Invoice",
				                text: "Pay "+ parseFloat(body.total).toFixed(5) + " BTC to: " + body.address,
				                image: { 
				                    smallImageUrl: `https://s3.amazonaws.com/echobuddy/${body.address}.png` 
				                }
				            })
				            .clearSession()
				            .shouldEndSession(true)

						});			        	

			        }
			        return false;
			    }
		    else return session.initialize(req, res, theResponse);
		} catch (e) {
			session.fail(req, res, 'response', Strings.criticalError, e);
		}
	});
	return alexa;
}
module.exports = search;

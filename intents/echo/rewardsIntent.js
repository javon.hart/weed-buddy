let Analytics = require('../../utils/analytics');
let Session = require('../../utils/session');
let Strings = require('../../res/strings');

function action(alexa) {
	alexa.intent("RewardsIntent", function(req, res) {
		try {
            Session.initialize(req,res);

            let currentUser = Session.currentUser(req);

			let x = req.slot('x');
			if (x) {
				doubleCheck(req, res);
			} else {
				noInput(req, res);
			}
		} catch (e) {
			Session.fail(req, res, 'rewards', Strings.criticalError, e);
		}
	});
	return alexa;
}

function noInput(req, res) {
	res.session('action', 'rewardIntent');
	Analytics.event("alexaApp.rewards#1", Session.id(req, res), Session.time()).send();
	res.say('what is your reward code?').shouldEndSession(false);
}

function doubleCheck(req, res) {
	let x = req.slot('x');
	x = x.replace(/\s/g, '').toLowerCase().replace(/(.{1})/g, "<break time='200ms'/>$1"); // split break
	res.session('action', 'rewardIntent');
	Analytics.event("alexaApp.rewards#1", Session.id(req, res), Session.time()).send();
	res.say('Did you say ' + x + "?").shouldEndSession(false);
}

exports.action = action;

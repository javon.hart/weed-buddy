let Business = require('../../utils/business');
let Strings = require('../../res/strings');
let _ = require('underscore');
let token = "alexaApp.reserveIntent";

function action(alexa, app) {
	alexa.intent("ReserveIntent", function(req, res) {
	    try {
            let reserve = (req, res, user) => {
                let currentUser = user;
                Business.reserve(req,res);
                app.track(req, token);
            }
            return app.initialize(req, res, reserve);
	    } catch(e) {
			app.fail(req,res,token,Strings.criticalError,e);
	    }
	});
	return alexa;
}

module.exports = action;

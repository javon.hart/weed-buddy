let Session = require('../../utils/session');
let Business = require('../../utils/business');
let Analytics = require('../../utils/analytics');
let Strings = require('../../res/strings');
let _ = require('underscore');
let token = "alexaApp.dispensaryList";

function action(alexa, app) {
    alexa.intent("DispensaryListIntent", function(req, res) {
        try {
            let dispensaries = (req, res, user) => {
                let currentUser = user;
                if (!app.addressConsent(currentUser)) {
                    try {
                        res.say(Strings.allowAddress.concat('<break time=\"500ms\"/>If you\'ve already done so please refresh the application by saying <break time=\"300ms\"/> open Weed Buddy.')).shouldEndSession(true);
                    } catch (e) {
                        console.log(e)
                        Analytics.event('CONSENT_ASK_FAIL', new Date()).send();
                    }
                    return;
                }
                let address = (currentUser.address)
                let noAddress = (address == undefined || address.type == 'FORBIDDEN')
                if (noAddress) {
                    res.say(Strings.allowAddress).shouldEndSession(true);
                    return true;
                    Session.track(req, token, 'noAddress');
                }
                let dispensaries = Business.localDispensaries(address);
                if (!dispensaries.length) {
                    res.say("I could not find any dispensaries in your area. Please try again later.")
                } else {
                    let script = "";
                    _.each(dispensaries, function(it) {
                        it.distance = Math.abs(parseInt(it.zip) - parseInt(address.postalCode))
                    })
                    _.each(dispensaries, function(it) {
                        if (it.distance < 50) {
                            script += it.name.concat('<break time=\"500ms\"/>')
                        }
                    })
                    if (script.length == 0) {
                        res.say('I could not find that information. Please try again later.');
                        return;
                    }
                    res.say(script)
                }
            }
            return app.initialize(req, res, dispensaries);
        } catch (e) {
            app.fail(req, res, token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = action;

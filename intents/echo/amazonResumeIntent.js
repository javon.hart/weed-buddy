'use strict';

function action(alexa, app) {
    alexa.intent("AMAZON.ResumeIntent", (req, res) => {
            try {
                res.audioPlayerPlay()
            }
            catch (e) {
                console.log("Amazon Resume Intent: " + e);
            }
    });
    return alexa;
}

module.exports = action;

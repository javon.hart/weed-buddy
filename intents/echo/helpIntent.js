let Session = require('../../utils/session'),
	_ = require('underscore'),
	token = "alexaApp.help",
	Strings = require('../../res/strings');

function action(alexa, app) {
	alexa.intent("HelpIntent", function(req, res) {
		let attrs = (req.sessionDetails.attributes)
		if (attrs) {
			if (attrs.action == 'alexaApp.like' && (attrs.lastIntent == 'launch') || attrs.lastIntent == 'search') {
				res.clearSession().session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Okay. Will that be all?').shouldEndSession(false);
				return
			}
		}
		try {
			let help = (req, res, user) => {
				res
				.say(Strings.allCommands)
				.reprompt(Strings.allCommands)
				.shouldEndSession(false);
				app.track(req, token);
			}
			return app.initialize(req, res, help);
		} catch (e) {
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}
module.exports = action;

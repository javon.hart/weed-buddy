'use strict';

let Strings = require('../../res/strings');
let token = "alexaApp.anyConnections";

function action(alexa, app) {
	alexa.intent("ViewProfile",
	  function(req,res) {
	    try {
	        app.initialize(req,res);
            let currentUser = app.currentUser(req);

            // has address
            if(!app.addressConsent(currentUser)){
                try {
                    res.say(Strings.allowAddress.concat('<break time=\"500ms\"/>If you\'ve already done so please refresh the application by saying <break time=\"300ms\"/> open Weed Buddy.'))
                    .shouldEndSession(true);
                } catch(e) {
                    console.log(e)
                    Analytics.event('CONSENT_ASK_FAIL', new Date()).send();
                }
                return;
            }

            let address = JSON.parse(currentUser.address)



            let linked = app.isLinked(req);
            if(linked){ // facebook is linked
                let bio = (currentUser.bio || String.editProfile);
                res.say('I sent your profile to the alexa app')
                .card({
                    type: "Standard",
                    title: "Viewing Your Profile",
                    text: "Login to www.weedbuddy.io/signup to edit your profile",
                    image: { // image is optional
                        smallImageUrl: currentUser.fb_image // required
                    }
                })
                .shouldEndSession(true);
            } else {
                res.say(Strings.link).shouldEndSession(true);
            }
	    } catch(e) {
	        console.log(e);
            app.fail(req,res, token,Strings.criticalError,e);
	    }
	  }
	);
	return alexa;
}

module.exports = action;

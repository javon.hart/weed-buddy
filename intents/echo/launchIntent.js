let _ = require('underscore'),
    Analytics = require('../../utils/analytics'),
    Session = require('../../utils/session'),
    Core = require('../../utils/core'),
    Dice = require('../../models/dice'),
    User = require('../../models/user'),
    mongoose = require('mongoose'),
    Jokes = require('../../res/jokes'),
    Strings = require('../../res/strings');

function action(alexa, app) {
    alexa.launch(function(req, res) {
        var promise = User.findOne({
            amzn_id: req.userId
        }).exec();
        return promise.then(function(user) {
            console.log('This is what I found!')
            console.log(user)
            if (user) {
                try {
                    console.log('and again: ', user)
                    let currentUser = user;
                    let consent = undefined;
                    if (req.context.System.user.permissions) {
                        consent = req.context.System.user.permissions.consentToken
                    }
                    if (!consent) {
                        if (user.activity && user.activity.launch) {
                            if (Core.isOdd(user.activity.launch) && user.activity.launch < 3) {
                                try {
                                    res.say(Strings.consent);
                                    res.card({
                                        type: "AskForPermissionsConsent",
                                        permissions: ["read::alexa:device:all:address"] // full address
                                    });
                                } catch (e) {
                                    Analytics.event('CONSENT_ASK_FAIL', new Date()).send();
                                }
                            }
                        }
                    }
                    // Greetings
                    res.say(Strings.oneSecondBreak)
                    res.say(_.shuffle(Strings.launchGreeting).pop());
                    // Say help
                    if (user) {
                        if (user.activity && user.activity.launch) {
                            if (Core.isOdd(user.activity.launch) && user.activity.launch < 5) {
                                res.say(Strings.help);
                            }
                        }
                    } else {
                        res.say(Strings.help);
                    }

                    // Request Query
                    res.say(Strings.prompt).session('lastIntent', 'launch').shouldEndSession(false).reprompt(Strings.prompt).shouldEndSession(false);
                    app.track(req, 'launch');
                    return;
                } catch (e) {
                    console.log(e)
                    app.fail(req, res, 'launch', Strings.criticalError, e, user);
                }
            } else {

                let newPerson = new User();
                newPerson.amzn_id = req.userId;
                newPerson._id = mongoose.Types.ObjectId();
                newPerson.save()
                res.say("I'm getting all setup. Please try your request again in a few seconds.") 
            }
        })
        // user promise
    });
}
module.exports = action;
'use strict';
let Search = require('../../utils/search');

function action(alexa, app) {
	alexa.intent("AboutIntent", (req, res) => {
		try {
			let about = (req, res) => Search.businessSearch(req, res, app)
			return app.initialize(req, res, about);
		} catch(e) {
		    console.log(e);
		}
	});
	return alexa;
}

module.exports = action;
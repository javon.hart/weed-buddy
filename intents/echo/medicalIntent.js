let StrainUtil = require('../../utils/strain'),
	request = require('request-promise'),
	Session = require('../../utils/session'),
	Search = require('../../utils/search'),
	Strain = require('../../models/strain'),
	filters = require('../../utils/filters'),
	token = 'alexaApp.medical',
	Strings = require('../../res/strings');

function search(alexa, app) {
	alexa.intent("MedicalIntent", function(req, res) {
		try {
			let q = req.slot('q');
			let options = {
			    uri: 'https://api.amazonalexa.com/v1/users/~current/skills/~current/inSkillProducts',
			    headers: {
			        'authorization': 'Bearer ' + req.context.System.apiAccessToken,
			        'Accept-Language': req.data.request.locale
			    },
			};
			return request(options, function (error, response, body) {
				let data = JSON.parse(body);

				let priv = undefined;
				if(data.inSkillProducts.length) {
					priv = data.inSkillProducts[0].entitled;
				}

				if(priv && priv == 'NOT_ENTITLED') {
					let upsell = {
						type: 'Connections.SendRequest',
						name: 'Upsell',
						payload: {
							InSkillProduct: { productId: 'amzn1.adg.product.ea8da109-c1bb-4247-8d99-37099396f57c' },
			           		upsellMessage: 'only Users who have subscribed to the Premium service have access to this feature. Would you like to learn more about upgrading?'
						},
					    token: 'correlationToken'
					}
				 	res.directive(upsell).shouldEndSession(true);					
				} else if(priv == 'ENTITLED' || !priv) {
					if (q) {
						let u = q;
						q = filters.name(q);
						if (StrainUtil.hasStrain(q, app)) {
							let x = new Strain(q);
							x.medical(res);
							app.track(req, token, q);
						} else {
							Search.search(req, res, u, app);
						}
					} else {
						res.say("I didn't get that, please try that again.").shouldEndSession(false);
						app.trackFail(req, res, token, q);
					}
				}
			})
		} catch (e) {
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}
module.exports = search;

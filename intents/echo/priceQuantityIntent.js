let Session = require('../../utils/session');
let Business = require('../../utils/business');
let Strings = require('../../res/strings');
let _ = require('underscore');
let Filters = require('../../utils/filters');
let token = "alexaApp.priceQuantity";

function action(alexa, app) {
    alexa.intent("PriceQuantityIntent", function(req, res) {
        try {
            let price = (req, res, user) => {
                let currentUser = user;
                let n = req.slot('n');
                let q = req.slot('q');
                let company = Business.getBusiness(n);
                if (!company) {
                    res.say('I could not find ' + n + '. Please try again later.');
                    return;
                }
                if (!company.menu) {
                    res.say('I could not find that information. Reach out to your local dispensary and tell them about weed buddy!');
                    return;
                }
                let strain = Filters.name(q, app);
                let menu = company.menu[strain];
                if (!menu) {
                    res.say(n + ' does not have that strain available at this time.');
                    return;
                } else {
                    console.log('here')
                    let script = "";
                    _.each(Object.keys(menu), function(it) {
                        script += menu[it] + ' dollars for one ' + it + '<break time=\"500ms\"/>';
                    })
                    res.say(script)
                }
                // address check
                app.track(req, token);
            }
            return app.initialize(req, res, price);
        } catch (e) {
            console.log(e)
            app.fail(req, res, token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = action;
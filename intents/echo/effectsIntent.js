let filters = require('../../utils/filters'),
	request = require('request-promise'),
	Session = require('../../utils/session'),
	Strain = require('../../models/strain'),
	StrainUtil = require('../../utils/strain'),
	Strings = require('../../res/strings'),
	token = 'alexaApp.effects';
let fuzzy = require('fuzzy');
let all = require('../../res/all_strains');
let didyoumean = require('didyoumean');


function action(alexa, app) {
	alexa.intent("EffectsIntent", function(req, res) {
		try {
			let q = req.slot('q');
			let options = {
			    uri: 'https://api.amazonalexa.com/v1/users/~current/skills/~current/inSkillProducts',
			    headers: {
			        'authorization': 'Bearer ' + req.context.System.apiAccessToken,
			        'Accept-Language': req.data.request.locale
			    },
			};
			return request(options, function (error, response, body) {
				let data = JSON.parse(body);

				let priv = undefined;
				if(data.inSkillProducts.length) {
					priv = data.inSkillProducts[0].entitled;
				}

				if(priv && priv == 'NOT_ENTITLED') {
					let upsell = {
						type: 'Connections.SendRequest',
						name: 'Upsell',
						payload: {
							InSkillProduct: { productId: 'amzn1.adg.product.7f0a0074-2a41-47ad-9554-c15fec34500a' },
			           		upsellMessage: 'only Users who have subscribed to the Premium service have access to this feature. Would you like to learn more about upgrading?'
						},
					    token: 'correlationToken'
					}
				 	res.directive(upsell).shouldEndSession(true);					
				} else if(priv == 'ENTITLED' || !priv) {
					if (q) {
						q = filters.name(q, app);
						if (StrainUtil.hasStrain(q)) {
							let x = new Strain(q);
							x.effects(res);
							app.track(req, token, q);
						} else {

							let nextClosest = didyoumean(q, all)
							let fuzz = fuzzy.filter(q, all)
							let closest;
							if (fuzz.length) {
								closest = fuzz[0].string
							} else {
								fuzz = fuzzy.filter(q.split(' ')[0], all)
								if (fuzz.length) {
									closest = fuzz[0].string
								}
							}
							console.log('closest',closest)
							console.log('nextClosest',nextClosest)


							if(closest != nextClosest && closest && nextClosest) {
								closest = "the closest I could find to " + q + " is " + closest + " and " + nextClosest 
							} else if(closest && closest != nextClosest) {
								closest = "the closest I could find to " + q + " is " + closest 
							} else if(nextClosest && !closest) {
								closest = "the closest I could find to " + q + " is " + nextClosest
							} else if((closest && nextClosest) && closest == nextClosest) {
								closest = "the closest I could find to " + q + " is " + closest 
							} else {
								closest = Strings.notFound + q
							}

							res
							.say(closest + ". Please try that one again later!")
			                .session('action', 'alexaApp.shouldEndSession')
			                .say(Strings.halfSecondBreak + 'Will that be all?')
			                .shouldEndSession(false);

							// app.trackFail(req, res, token, q, user);
						}
					} else {
						app.fail(req, res, token, Strings.noQuery);
					}
				}
			})
		} catch (e) {
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}
module.exports = action;

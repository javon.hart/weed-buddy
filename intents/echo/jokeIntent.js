let Strings = require('../../res/strings');
let Jokes = require('../../res/jokes');
let Images = require('../../res/images');
let _ = require('underscore');
let token = "alexaApp.joke";

function action(alexa, app) {
	alexa.intent("JokeIntent", function(req, res) {
	    try {
            let joke = (req, res, user) => {
                let currentUser = user;
                let success = _.shuffle(Strings.listening).pop(),
                    j = _.shuffle(Jokes).pop();
                res.say(success).say(j).card({
                    type: "Standard",
                    title: "Jokes",
                    text: j,
                }).card({
                    type: "Standard",
                    title: "For your entertainment!",
                    text: j
                }).shouldEndSession(true);
                app.track(req, token);
            }
            return app.initialize(req,res,joke);
	    } catch(e) {
			app.fail(req,res,token,Strings.criticalError,e);
	    }
	});
	return alexa;
}

module.exports = action;

let Analytics = require('../../utils/analytics');
let Session = require('../../utils/session');
let Strings = require('../../res/strings');
let _ = require('underscore');
let token = 'alexaApp.editDiscovery'

function action(alexa, app) {
	alexa.intent("EditDiscovery", function(req, res) {
		try {
			app.initialize(req, res);
			let currentUser = app.currentUser(req);

            // has address
            if(!app.addressConsent(currentUser)){
                try {
                    res.say(Strings.allowAddress.concat('<break time=\"500ms\"/>If you\'ve already done so please refresh the application by saying <break time=\"300ms\"/> open Weed Buddy.'))
                    .shouldEndSession(true);
                } catch(e) {
                    console.log(e)
                    Analytics.event('CONSENT_ASK_FAIL', new Date()).send();
                }
                return;
            }

            let address = currentUser.address

            let linked = app.isLinked(req);
            if(linked){ // facebook is linked
                res
                    .say("What's your minimum aged buddy")
                    .session('action', 'editDiscovery')
                    .session('index', 0)
                    .shouldEndSession(false);

            } else {
                res.say(Strings.link).shouldEndSession(true);
            }

            app.track(req, token, Strings.noQuery);
		} catch (e) {
		    console.log(e)
            app.fail(req,res,token,Strings.criticalError,e);
		}
	});
	return alexa;
}
module.exports = action;

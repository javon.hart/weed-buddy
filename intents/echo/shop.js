let Session = require('../../utils/session'),
	_ = require('underscore'),
token = "alexaApp.shop",
	Strings = require('../../res/strings');

function action(alexa, app) {
	alexa.intent("Shop", function(req, res) {
		try {
			let shop = (req, res, user) => {
				res.say("What would you like to order?")
	            .session('shopping', true)
	            .session('action', 'alexaApp.shopping')
	            .shouldEndSession(false);
				app.track(req, token);				
			}
            return app.initialize(req,res,shop);
		} catch (e) {
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}

module.exports = action;

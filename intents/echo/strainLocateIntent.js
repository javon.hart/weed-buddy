let Business = require('../../utils/business');
let Strings = require('../../res/strings');
let _ = require('underscore');
let token = "alexaApp.strainLocate";

function action(alexa, app) {
	alexa.intent("StrainLocateIntent", function(req, res) {
	    try {
	    	let locate = (req, res, user) => {
	            let currentUser = user;
	            Business.locate(req,res,token, user);
	    	}
            return app.initialize(req, res, locate);
	    } catch(e) {
			app.fail(req,res,token,Strings.criticalError,e);
	    }
	});
	return alexa;
}

module.exports = action;

console.log(Strings.criticalError)
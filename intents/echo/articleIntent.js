'use strict';
const Token = 'alexaApp.article';
let Strings = require('../../res/strings'),
    Session = require('../../utils/session'),
    User = require('../../models/user'),
    Update = require('../../models/update'),
    moment = require('moment'),
    _ = require('underscore'),
    Updates = require('../../res/community');

function script(user, req) {
    let out = '';
    let now = moment(new Date()); //todays date
    let ids = [];
    _.each(Updates, function(update) {
        update
        let end = moment(update.date); // another date
        let duration = moment.duration(now.diff(end));
        let days = duration.asDays();
        console.log('update', update)
        // console.log(end)
        // console.log(duration)
        console.log('days', days)
        if (days < 1.5) {
            out += update.text += Strings.halfSecondBreak;
            ids.push(update.id)
        }
    })
    if (!out.length) {
        let stop = false;
        let count = 0;
        _.each(Updates, function(update) {
            if (!stop) {
                let my_updates = user.updates || []
                if (!my_updates.includes(update.id)) {
                    out += update.text += Strings.halfSecondBreak
                    ids.push(update.id)
                    count += 1
                    if (count == 7) stop = true
                }
            }
        })
    }
    Session.saveUpdates(req, ids);
    return out
}
let community = (req, res, user) => {
    let words = script(user, req);
    res.say(words)
}

function action(alexa, app) {
    alexa.intent("ArticleIntent", function(req, res) {
        try {
            app.track(req, Token);
            return app.initialize(req, res, community);
        } catch (e) {
            console.log(e)
            res.say('<break time=\"500ms\"/>Follow me on twitter at alexa, weed, buddy!').shouldEndSession(true);
            // app.trackFail(req, res, Token, Strings.criticalError, e);
        }
    });
    alexa.intent("Community", function(req, res) {
        try {
            app.track(req, Token);
            return app.initialize(req, res, community);
        } catch (e) {
            console.log(e)
            res.say('<break time=\"500ms\"/>Follow me on twitter at alexa, weed, buddy!').shouldEndSession(true);
            // app.trackFail(req, res, Token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = action;

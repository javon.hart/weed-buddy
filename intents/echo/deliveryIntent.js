let Session = require('../../utils/session');
let Business = require('../../utils/business');
let Strings = require('../../res/strings');
let _ = require('underscore');
let token = "alexaApp.delivery";

function action(alexa, app) {
    alexa.intent("DeliveryIntent", function(req, res) {
        try {
            let deliver = (req, res, user) => {
                let currentUser = user;
                let q = req.slot('n');
                let company = Business.getBusiness(q);
                if (!company) {
                    res.say('I could not find ' + q + '. Please try again later.');
                    return;
                }
                if (company.deliver == undefined) {
                    res.say('I could not find that information. Please try again later.');
                    return;
                }
                if (!company.deliver) {
                    res.say('no')
                } else {
                    res.say('yes')
                }
                app.track(req, token);
            }
            return app.initialize(req, res, deliver);
        } catch (e) {
            app.fail(req, res, token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = action;

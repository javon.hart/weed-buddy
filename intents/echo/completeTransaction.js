'use strict';
let Search = require('../../utils/search');
let request = require('request-promise')
function action(alexa, app) {
	alexa.intent("CompleteTransaction", (req, res) => {
		try {
			return request({
			    method: 'GET',
			    url: "https://blooming-oasis-15324.herokuapp.com/confirm_payment/" + req.userId,
			    json: true
			}, function (error, response, body) {
				console.log(body)
				if(body.progress == 'pending') res.say('Bitcoin miners are processing your payment. Check back soon.')
	            else res.say('okay')
	            res.clearSession()
	            .shouldEndSession(true)
			});
		} catch(e) {
		    console.log(e);
		}
	});
	return alexa;
}

module.exports = action;
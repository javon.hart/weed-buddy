let Session = require('../../utils/session');
let Strings = require('../../res/strings');
let _ = require('underscore');
let strainUtil = require('../../utils/strain');

function action(alexa, app) {
    alexa.intent("PopularIntent", function(req, res) {
        try {
            var promise = User.find({}).exec();
            return promise.then(function(items) {
                let mostPopular = undefined;
                let strains = [];
                let token = "alexaApp.popular";
                // Found all strains
                _.each(items, function(it) {
                    if (it.likes) {
                        _.each(it.likes, function(q) {
                            strains.push(q);
                        });
                    }
                });
                strains = _.flatten(strains);
                // Found most popular
                _.each(strains, function(strain) {
                    let r = new RegExp(strain, "g")
                    let query = strains.toString().match(r).length
                    if (!mostPopular) mostPopular = strain;
                    else if (strains.toString().match(mostPopular).length < query) {
                        mostPopular = strain;
                    }
                })
                if (mostPopular) {
                    console.log(mostPopular)
                    res.say('The most popular strain today is '.concat(mostPopular))
                    .session('action', 'alexaApp.shouldEndSession')
                    .say("<break time=\'200ms\'/>Will that be all?")                    
                    .shouldEndSession(false)
                } else res.say('Please try that again later.');
                app.track(req, token);
            })
        } catch (e) {
            app.fail(req, res, token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = action;
let Search = require('../../utils/search'),
Session = require('../../utils/session');


function search(alexa, app) {
	alexa.intent("SearchIntent", function(req, res) {
		let attrs = req.sessionDetails.attributes
		let q = req.slot('q');

		let doSearch = (req, res, user) => {
			let currentUser = user;
			Search.search(req, res, q, app, user);	
		}

 		return app.initialize(req,res,doSearch);
	});
	return alexa;
}

module.exports = search;


let Session = require('../../utils/session');
let Business = require('../../utils/business');
let Analytics = require('../../utils/analytics');
let Strings = require('../../res/strings');
let _ = require('underscore');
let token = "alexaApp.dealsList";

function action(alexa, app) {
    alexa.intent("DealsListIntent", function(req, res) {
        try {
            let deals = (req, res, user) => {
                let currentUser = user
                if (!app.addressConsent(currentUser)) {
                    try {
                        res.say(Strings.allowAddress.concat('<break time=\"500ms\"/>If you\'ve already done so please refresh the application by saying <break time=\"300ms\"/> open Weed Buddy.')).shouldEndSession(true);
                    } catch (e) {
                        console.log(e)
                        Analytics.event('CONSENT_ASK_FAIL', new Date()).send();
                    }
                    return;
                }
                let q = req.slot('n');
                let company = Business.getBusiness(q);
                if (!company) {
                    app.trackFail(req, res, token, q, user);
                    res.say('I could not find ' + q + '. Please try again later.');
                    return;
                }
                let deals = company.deals;
                if (deals == undefined) {
                    app.trackFail(req, res, token, q, user);
                    res.say(company.name + ' hasn\'t posted that information yet. Please try again later.');
                    return;
                }
                if (deals.length) {
                    let script = "";
                    _.each(deals, function(it) {
                        script += it;
                        script += '<break time=\"500ms\"/>'
                    })
                    res.say(script)
                    app.track(req, token, q);
                } else {
                    app.trackFail(req, res, token, q, user);
                    res.say('No deals available at this time. Please try again later.')
                }
            }
            return app.initialize(req, res, deals);
        } catch (e) {
            app.fail(req, res, token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = action;

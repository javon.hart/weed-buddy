let Session = require('../../utils/session');
let Events = require('../../utils/event');
let Core = require('../../utils/core');
let token = "alexaApp.events"

function action(alexa, app) {
	alexa.intent("EventIntent", function(req, res) {
		try {
			let event = (req, res, user) => {
				let currentUser = user
				req.getSession().clear('ecomm');
				req.getSession().clear('qIndex');
				let list = Events.listAll();
				let script = list[0]
				res.session('action', 'eventIntent');
				res.session('index', '0');
				res.say(script)
				let i = 0;
				// 	  	if(i < list.length - 1) {
				//  		res.say('Would you like to buy a ticket?')
				//        res.session('ecomm', 'ticketInquiry');
				//        res.session('event', JSON.stringify(list[i]))
				//  		// res.say('Would you like to continue')
				//     .shouldEndSession(false);
				// }  else {
				// 	res.say('Thank you, goodbye.')
				// 	.shouldEndSession(false);
				// }
				res.say('Thank you, goodbye.').shouldEndSession(true);
			}
			return app.initialize(req, res, event);
		} catch (e) {
			console.log(e)
		}
	});
	return alexa;
}
module.exports = action;
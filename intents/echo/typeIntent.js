let StrainUtil = require('../../utils/strain'),
	Search = require('../../utils/search'),
	Strain = require('../../models/strain'),
	filters = require('../../utils/filters'),
	token = 'alexaApp.type',
	Strings = require('../../res/strings');

function search(alexa, app) {
	alexa.intent("TypeIntent", function(req, res) {
		try {
			let doType = (req, res, user) => {
				let currentUser = user;
				let q = req.slot('q');
				if (q) {
					q = filters.name(q, app);
					if (StrainUtil.hasStrain(q)) {
						let x = new Strain(q);
						x.type(res);
						app.track(req, token, q);
					} else {
						Search.search(req, res, q, app, user);
					}
				} else {
					res.say("I didn't get that, please try that again.").shouldEndSession(false);
					app.fail(req, res, token, Strings.noQuery);
				}
			}
			return app.initialize(req, res, doType);
		} catch (e) {
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}
module.exports = search;

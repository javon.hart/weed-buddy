let _ = require('underscore'),
	StrainUtil = require('../../utils/strain'),
	Core = require('../../utils/core'),
	Images = require('../../res/images'),
	Strings = require('../../res/strings'),
	token = "alexaApp.searchBy",
	Session = require('../../utils/session');

function searchBy(alexa, app) {
	alexa.intent("SearchByIntent", function(req, res) {
		try {
			let doSearch = (req, res, user) => {
				let currentUser = user;
				let benefits = req.slot('b'),
					effects = req.slot('e'),
					flavors = req.slot('f'),
					query = benefits || effects || flavors,
					results = "";
				if (!query) {
					res.say("Try searching strains good for " + _.shuffle(['pain', 'insomnia', 'nausea', 'appetite']).pop() + ". For all effects say list all effects. for all flavors say list all flavors.").shouldEndSession(false);
					app.trackFail(req, res, token, Strings.noQuery);
					return;
				}
				let term = StrainUtil.colloquial(query);
				// Collect a list of strain names given a filter mechanism (via a benefit, effect, or flavor)
				if (benefits) {
					results = StrainUtil.byIncludes("medical", term);
					if (!results.length) {
						results = StrainUtil.byIncludes("effects", term);
					}
				} else if (effects) {
					results = StrainUtil.byIncludes("effects", term);
					if (!results.length) {
						results = StrainUtil.byIncludes("medical", term);
					}
				} else if (flavors) {
					results = StrainUtil.byIncludes("flavors", term);
				}
				if (results.length) {
					let suggestions = StrainUtil.suggest(results);
					let strain = suggestions.split(',').pop();
					app.track(req, token, term);
					res.say(query.concat(". Got it! Check your alexa app for more suggestions like ".concat(strain).concat('.'))).card({
						type: "Standard",
						title: Core.capitalize(query),
						text: suggestions
					})
      .session('action', 'alexaApp.shouldEndSession')
                        .say("<break time=\'200ms\'/> Will that be all?")
					.shouldEndSession(false);
				} else {
					res.say("Sorry. No results were found for " + query + ". But I am always learning. Please try again later.");
					app.trackFail(req, res, token, query);
				}
			}
			return app.initialize(req, res, doSearch);
		} catch (e) {
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}
module.exports = searchBy;

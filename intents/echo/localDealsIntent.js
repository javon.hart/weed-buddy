let Analytics = require('../../utils/analytics');
let Strings = require('../../res/strings');
let Business = require('../../utils/business');
let _ = require('underscore');
let token = "alexaApp.dealsList";

function action(alexa, app) {
    alexa.intent("LocalDealsIntent", function(req, res) {
        try {
            let local = (req, res, user) => {
                let currentUser = user;
                if (!app.addressConsent(currentUser)) {
                    try {
                        res.say(Strings.allowAddress.concat('<break time=\"500ms\"/>If you\'ve already done so please refresh the application by saying <break time=\"300ms\"/> open Weed Buddy.')).shouldEndSession(true);
                    } catch (e) {
                        console.log(e)
                        Analytics.event('CONSENT_ASK_FAIL', new Date()).send();
                    }
                    return;
                }
                let address = (currentUser.address)
                let noAddress = (address == undefined || address.type == 'FORBIDDEN')
                if (noAddress) {
                    res.say(Strings.allowAddress).shouldEndSession(true);
                    return true;
                    Session.track(req, token, 'noAddress');
                }
                let dispensaries = Business.localDispensaries(address);
                let deals = undefined;
                let script = null;
                _.each(dispensaries, function(it) {
                    if (it.deals) {
                        if (!script) script = [];
                        script.push(it.deals);
                    }
                })
                if (script) {
                    script = _.flatten(script).toString().replace(/,/g, "<break time=\'1000ms\'/>");
                    res.say(script)
                    app.track(req, token, address.postalCode);
                } else {
                    app.trackFail(req, res, token, address.postalCode);
                    res.say("I could not find any deals in your area at this time. Please try again later.")
                }
            }
            return app.initialize(req, res, local);
        } catch (e) {
            console.log(e)
            app.fail(req, res, token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = action;
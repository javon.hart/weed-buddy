let StrainUtil = require('../../utils/strain'),
    recipeUtil = require('../../utils/recipe'),
    symptomsUtil = require('../../utils/symptoms'),
    effectsUtil = require('../../utils/effects'),
    flavorsUtil = require('../../utils/flavors'),
    commandsUtil = require('../../utils/commands'),
    Core = require('../../utils/core'),
    Strings = require('../../res/strings'),
    images = require('../../res/images'),
    token = 'alexaApp.list',
    d = require('didyoumean'),
    _ = require('underscore');

function action(alexa, app) {
    alexa.intent("ListIntent", function(req, res) {
        try {
            let attrs = (req.sessionDetails.attributes)
            if (attrs) {
                if (attrs.action == 'alexaApp.like' && attrs.lastIntent == 'launch') {
                    res.clearSession().session('action', 'alexaApp.shouldEndSession').say(Strings.halfSecondBreak + 'Will that be all?').shouldEndSession(false);
                    return
                }
            }
            let theList = (req, res, user) => {
                let currentUser = user;
                let type = req.slot('t'),
                    list, index;
                if (type == 'all' || type == undefined) {
                    type = 'all'
                    console.log('listing all')
                    list = StrainUtil.allStrains();
                    index = StrainUtil.strains(list);
                    //console.log(index)
                    res.session('type', type);
                }
                if (type == 'hybrid' || type == 'sativa' || type == 'indica') {
                    list = StrainUtil.listOfStrains(type);
                    res.session('type', type);
                    index = StrainUtil.strains(list);
                }
                if (type == 'random') {
                    list = _.shuffle(StrainUtil.allStrains());
                    index = StrainUtil.strains(list);
                }
                if (type == 'recipes') {
                    console.log('listing recipes')
                    list = recipeUtil.listOfRecipes();
                    res.session('type', type);
                    index = recipeUtil.recipes(list);
                }
                // type =  d(type,['symptoms']) || type
                if (type == 'symptoms' || type == 'symptons') {
                    console.log('listing symptoms')
                    list = symptomsUtil.listOfSymptoms();
                    res.session('type', type);
                    index = symptomsUtil.symptoms(list);
                }
                if (type == 'effects') {
                    console.log('listing effects')
                    list = effectsUtil.listOfEffects();
                    console.log(list)
                    res.session('type', type);
                    index = effectsUtil.effects(list);
                    console.log(index)
                }
                if (type == 'flavors') {
                    console.log('listing flavors')
                    list = flavorsUtil.listOfFlavors();
                    res.session('type', type);
                    index = flavorsUtil.flavors(list);
                }
                if (type == 'commands') {
                    console.log('listing flavors')
                    list = commandsUtil.listOfCommands();
                    res.session('type', type);
                    index = commandsUtil.commands(list);
                }
                res.session('action', 'listIntent');
                res.session('index', '0');
                let reduce = index[0]
                if (!reduce) {
                    res.clearSession();
                    res.say('Could you repeat that?').shouldEndSession(false)
                }
                let prompt = Core.reduce(reduce);
                res.card({
                    type: "Standard",
                    title: "Listing " + Core.capitalize(type),
                    text: prompt
                });
                res.say("Listing " + type + '<break time=\'300ms\'/>');
                if (prompt) {
                    app.track(req, token, type);
                    res.say(prompt);
                    if (Object.keys(index).length == 1) {
                        res.say(_.shuffle(['All finished, goodbye', 'That is all. Later.']).pop());
                        return;
                    }
                    res.say('would you like to continue?').shouldEndSession(false).reprompt('would you like to continue?');
                } else {
                    app.fail(req, res, token, Strings.noQuery);
                    res.clearSession();
                }
            }
            return app.initialize(req, res, theList);
        } catch (e) {
            app.fail(req, res, token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = action;

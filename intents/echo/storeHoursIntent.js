let Session = require('../../utils/session');
let Business = require('../../utils/business');
let logger = require('../../utils/logger');
let Core = require('../../utils/core');
let Strings = require('../../res/strings');
let _ = require('underscore');
let m = require('moment');
let token = "alexaApp.storeHours";

function action(alexa, app) {
    alexa.intent("StoreHoursIntent", function(req, res) {
        try {
            let doHours = (req, res, user) => {
                let currentUser = user;
                let q = req.slot('n');
                let isOpen = Business.isOpen(q, currentUser);
                if (isOpen == undefined) {
                    let biz = Business.getBusiness(q);
                    if (biz) {
                        res.say(biz.name + ' hasn\'t posted that information yet. Please try again later.')
                    } else {
                        res.say('I couldn\'t find ' + q + '. please try that again later')
                    }
                    app.trackFail(req, res, token, q);
                } else if (isOpen) {
                    res.say('yes')
                    app.track(req, token, q);
                } else {
                    res.say('no')
                    app.track(req, token, q);
                }
            }
            return app.initialize(req, res, doHours);
        } catch (e) {
            console.log(e)
            app.fail(req, res, token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = action;
'use strict';
let Strings = require('../../res/strings'),
    ResponseUtil = require('../../utils/response'),
    Analytics = require('../../utils/analytics');

function action(alexa, app) {
    alexa.intent("AMAZON.NextIntent", function(req, res) {
        try {

            app.initialize(req, res);
            let attrs = req.sessionDetails.attributes,
            currentUser = app.currentUser(req),
            token = 'alexaApp.next';

            // has address
            if (!app.addressConsent(currentUser)) {
                try {
                    res.say(Strings.allowAddress.concat('<break time=\"500ms\"/>If you\'ve already done so please refresh the application by saying <break time=\"300ms\"/> open Weed Buddy.')).shouldEndSession(true);
                } catch (e) {
                    console.log(e);
                    Analytics.event('CONSENT_ASK_FAIL', new Date()).send();
                }
                return;
            }

            if (attrs) {
                if (attrs.action == 'findBuddy') {
                    ResponseUtil.response(currentUser, req, res, attrs);
                    ResponseUtil.passBuddy();
                    return true;
                }
            }

        } catch(e) {
            console.log(e);
        }
    });
    return alexa;
}

module.exports = action;
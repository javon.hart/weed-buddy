'use strict';
const token = 'alexaApp.business';
let Strings = require('../../res/strings'),
	Session = require('../../utils/session');

function action(alexa, app) {
	alexa.intent("InquiryIntent", function(req, res) {
		try {
			let business = (req, res, user) => {
				res.say(Strings.business).shouldEndSession(true);
			}
			app.track(req, token);			
			return app.initialize(req, res, business);			
		} catch (e) {
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}

module.exports = action;

let Business = require('../../utils/business');
let Strings = require('../../res/strings');
let Filters = require('../../utils/filters');

let _ = require('underscore');
let token = "alexaApp.hasStrain";

function action(alexa, app) {
	alexa.intent("HasStrainIntent", function(req, res) {
	    try {
            let has = (req, res, user) => {
                let currentUser = user;
                let q = req.slot('q');
                let n = req.slot('n');
                let company = Business.getBusiness(n);
                if(!company) {
                    res.say('I could not find ' + q + '. Please try again later.');
                    return;
                }

                if(!company.menu) {
                    res.say('I could not find that information. Reach out you your local dispensary and tell them about weed buddy!');
                    return;
                }
                let strain = Filters.name(q, app);
                let menu = company.menu[strain];
                if(!menu) {
                    res.say(n + ' does not have that strain available at this time.');
                    return;
                } else {
                    res.say('Yes');
                }
                // address check
                // Session.track(req, token);
            }
			return app.initialize(req,res,has);
	    } catch(e) {
			// Session.fail(req,res,token,Strings.criticalError,e);
	    }
	});
	return alexa;
}

module.exports = action;

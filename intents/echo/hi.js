let Analytics = require('../../utils/analytics');

function action(alexa, app) {
	alexa.intent("HelloIntent",
	  function(req,res) {
        Analytics.event("alexaApp.hi", app.id(req,res), app.time()).send();
	  	res.say('Hi Instagram.')
  	    .shouldEndSession(true);
	  }
	);
	return alexa;
}

module.exports = action;

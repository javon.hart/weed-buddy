let Analytics = require('../../utils/analytics');
let Filters = require('../../utils/filters');
let StrainUtil = require('../../utils/strain');
let Session = require('../../utils/session');
let Strings = require('../../res/strings');
let User = require('../../models/user');
let _ = require('underscore');
let token = 'alexaApp.like'

function action(alexa, app) {
	alexa.intent("LikeIntent", function(req, res) {
		try {
			let like = (req, res, user) => {
				let currentUser = user;
				let q = req.slot('q');
				console.log(q)
				if (!q) {
					res.say('Could you repeat that?').shouldEndSession(false);
					return true;
				}
				let u = q;
				q = Filters.name(q, app);
				if (StrainUtil.hasStrain(q)) {
					User.findOne({
						amzn_id: currentUser.amzn_id
					}, function(err, user) {
						if (user) {
							console.log(q)
							user.likes.push(q);
							user.likes = _.uniq(user.likes);
							console.log(user)
							user.save();
							console.log(200)
						}
						return false;
					});
					let script = `I've added ${q} to your likes. Will that be all?`;
					res.say(script)
 		    		.session('action', 'alexaApp.shouldEndSession')
					.shouldEndSession(false)
					app.track(req, token, q);
				} else {
					res.say(_.shuffle(['I appreciate you sharing that.', 'Interesting.', `You know who else likes ${q} <break time="1000ms"/> My mom!`]).pop()).shouldEndSession(true);
					// app.fail(req, res, token, Strings.noQuery);
				}
			} 
			return app.initialize(req, res, like);			
		} catch (e) {
			console.log(e)
			app.fail(req, res, token, Strings.criticalError, e);
		}
	});
	return alexa;
}
module.exports = action;
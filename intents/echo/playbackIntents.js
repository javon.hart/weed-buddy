'use strict';
let AudioPlayer = require('../../utils/audioPlayer');

function action(alexa, app) {

    alexa.intent("AMAZON.PauseIntent", {}, function(req, res) {


        AudioPlayer.pause(req,res)
    });

    alexa.intent("AMAZON.ResumeIntent", {}, function(req, res) {

		let resume = (req, res, user) => {
        	AudioPlayer.resume(req,res, user)
		}
        return app.initialize(req,res,resume);

    });


	return alexa;
}

module.exports = action;

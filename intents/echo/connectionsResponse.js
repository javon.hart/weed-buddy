'use strict';
let Search = require('../../utils/search');
let request = require('request-promise')
function action(alexa, app) {
	alexa.on("Connections.Response", (req, res) => {
		try {
			console.log('req', req)
			res
			.say('Will that be all')
		    .session('action', 'alexaApp.shouldEndSession')
			.shouldEndSession(false)
		} catch(e) {
		    console.log(e);
		}
	});
	return alexa;
}

module.exports = action;
'use strict';

const Token = 'alexaApp.community';
let Strings = require('../../res/strings'),
 Interview = require('../../models/interview'),
AudioPlayer = require('../../utils/audioPlayer');

// todo if cso then override
function action(alexa, app) {
    alexa.intent("Interview", function(req, res) {
        try {
            var promise = Interview.findOne({}).exec();
            return promise.then(function(url) {
                if(url) url = url.url 
                url = url || ''
                let temp = "https://s3.amazonaws.com/weedbuddy/Interviewing+High+Times+on+their+Midwest+Cannabis+Cup+2017.mp3";
                AudioPlayer.play(req, res, url);
            })
        } catch (e) {
            console.log(e)
            res.say('<break time=\"500ms\"/>Follow me on twitter at alexa, weed, buddy!').shouldEndSession(true);
            app.trackFail(req,res, Token, Strings.criticalError, e);
        }
    })
    return alexa;
}

module.exports = action;

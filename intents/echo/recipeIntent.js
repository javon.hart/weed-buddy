const IntentID = 'alexaApp.recipe';
let Strings = require('../../res/strings'),
    Images = require('../../res/images'),
    request = require('request-promise'),
    _ = require('underscore'),
    RecipeUtil = require('../../utils/recipe'),
    token = 'alexaApp.recipe',
    Session = require('../../utils/session');

function constructor(alexa, app) {
    alexa.intent("RecipeIntent", function(req, res) {
        try {

            let r = req.slot('r');



            let options = {
                uri: 'https://api.amazonalexa.com/v1/users/~current/skills/~current/inSkillProducts',
                headers: {
                    'authorization': 'Bearer ' + req.context.System.apiAccessToken,
                    'Accept-Language': req.data.request.locale
                },
            };          
            return request(options, function (error, response, body) {
                let data = JSON.parse(body);

                let priv = undefined;
                if(data.inSkillProducts.length) {
                    priv = data.inSkillProducts[0].entitled;
                }

                if(priv && priv == 'NOT_ENTITLED') {
                    let upsell = {
                        type: 'Connections.SendRequest',
                        name: 'Upsell',
                        payload: {
                            InSkillProduct: { productId: 'amzn1.adg.product.ea8da109-c1bb-4247-8d99-37099396f57c' },
                            upsellMessage: 'only Users who have subscribed to the Premium service have access to this feature. Would you like to learn more about upgrading?'
                        },
                        token: 'correlationToken'
                    }
                    res.directive(upsell).shouldEndSession(true);                   
                } else if(priv == 'ENTITLED' || !priv) {
   
                    if (r) {
                        r = RecipeUtil.getRealRecipe(r);
                        if (RecipeUtil.hasRecipe(r)) {
                            let currentRecipe = RecipeUtil.getRecipe(r);
                            let speech = RecipeUtil.main(r);
                            res.say(speech).say(_.shuffle(Strings.leaving).pop()).card({
                                type: "Standard",
                                title: currentRecipe.title,
                                text: RecipeUtil.cardString(currentRecipe)
                            }).shouldEndSession(true);
                            app.track(req, token, r);
                        } else {
                            res.say("I couldn't find a recipe by the name of " + r + ". Please try that one again later.<break time='900ms'/>").say(_.shuffle(Strings.leaving).pop()).shouldEndSession(true);
                            app.trackFail(req, res, IntentID, r);
                        }
                    } else {
                        // triggered with no recipe specified
                        res.say("Try any of the recipes in your alexa app. For all recipes say list all recipes.").card({
                            type: "Standard",
                            title: "Try this.",
                            text: "Ask me to lookup recipe " + _.shuffle(RecipeUtil.allRecipes()).pop()
                        }).shouldEndSession(true);
                        app.track(req, IntentID);
                    }

                }
            })




        } catch (e) {
            app.fail(req, res, token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = constructor;
'use strict';
let Search = require('../../utils/search'),
	User = require('../../models/user');

function action(alexa, app) {
	alexa.intent("EnableDiscovery", function(req, res) {
		try {
			app.initialize(req, res);
			let currentUser = app.currentUser(req);
			// has address
			if (!app.addressConsent(currentUser)) {
				try {
					res.say(Strings.allowAddress.concat('<break time=\"500ms\"/>If you\'ve already done so please refresh the application by saying <break time=\"300ms\"/> open Weed Buddy.')).shouldEndSession(true);
				} catch (e) {
					console.log(e);
					Analytics.event('CONSENT_ASK_FAIL', new Date()).send();
				}
				return;
			}
			let address = (currentUser.address);
			let linked = app.isLinked(req);
			if (linked) { // facebook is linked 
				User.findOne({
					amzn_id: app.currentUser(req).amzn_id
				}, function(err, u) {
					if (err) return false;
					console.log(u)
					if (u) {
						try {
							u.discovery = true;
							u.save(function(err) {
								console.log(err)
								console.log(200)
								return
							});
						} catch (e) {
							console.log(e)
						}
						return;
					} else {
						console.log('no')
					}
				});
				res.say('You are now discoverable')
			} else {
				res.say('please link your account to use this feature.')
			}
		} catch (e) {
			console.log(e);
		}
	});
	return alexa;
}
module.exports = action;
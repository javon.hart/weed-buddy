'use strict';

let Session = require('../../utils/session');
let Strings = require('../../res/strings');
let token = "alexaApp.anyConnections";

function action(alexa, app) {
	alexa.intent("AnyConnections",
	  function(req,res) {
	    try {
	        app.initialize(req,res);
            let currentUser = app.currentUser(req);
            if(!app.addressConsent(currentUser)){
                try {
                    res.say(Strings.allowAddress.concat('<break time=\"500ms\"/>If you\'ve already done so please refresh the application by saying <break time=\"300ms\"/> open Weed Buddy.'))
                    .shouldEndSession(true);
                } catch(e) {
                    console.log(e)
                    Analytics.event('CONSENT_ASK_FAIL', new Date()).send();
                }
                return;
            }

            let address = currentUser.address
            let linked = app.isLinked(req);
            if(linked){ // facebook is linked
                let connections = currentUser.newConnections,
                script;
                if(connections) {
                    if(connections.length > 0) {
                        script = `You have ${connections.length}  new connections. Would you like me to list them`;
                        res.session('action', 'listConnections');
                        res.say(script).shouldEndSession(false);
                    } else {
                        res.say(Strings.noConnections).shouldEndSession(true);
                    }
                } else {
                    res.say(Strings.noConnections).shouldEndSession(true);
                }
            } else {
                res.say(Strings.link).shouldEndSession(true);
            }
	    } catch(e) {
            app.fail(req,res, token,Strings.criticalError,e);
	    }
	  }
	);
	return alexa;
}

module.exports = action;

const Token = 'alexaApp.nearby';
let Strings = require('../../res/strings'),
    Business = require('../../utils/business'),
    Analytics = require('../../utils/analytics'),
    _ = require('underscore'),
    Session = require('../../utils/session');

function action(alexa, app) {
    alexa.intent("NearbyIntent", function(req, res) {
        try {
            let near = (req, res, user) => {
                let id = app.id(req, res);
                if (id == 'amzn1.ask.account.AF52NYJX2H7C5YPD3Q6HS66CO7M5XCBROJXQR2KCSN7VSBNQ23677UGE3JFILQXNWKZENENNKIDCTL7SZCQ26STPX7CIDHWFTZD3MAVC5OTPVHELNC6DJ4LZUE4VAC2EO2SYBSHFI3EJU3BENN2WWXG2EQMDFJOOLZIDXILP6T5ZJLLUT6ZLB5IBA4BBSJ2Q6W3ENGRKFONLCAI') {
                    let company = app.session().getStore().getItem('bpo');
                    res.say("The closest dispensary is ".concat(company)).shouldEndSession(true);
                } else {
                    let currentUser = user;
                    if (!app.addressConsent(currentUser)) {
                        try {
                            res.say(Strings.allowAddress.concat('<break time=\"500ms\"/>If you\'ve already done so please refresh the application by saying <break time=\"300ms\"/> open Weed Buddy.')).shouldEndSession(true);
                        } catch (e) {
                            console.log(e)
                            Analytics.event('CONSENT_ASK_FAIL', new Date()).send();
                        }
                        return;
                    }
                    let address = (currentUser.address)
                    let noAddress = (address == undefined || address.type == 'FORBIDDEN')
                    if (noAddress) {
                        res.say(Strings.allowAddress).shouldEndSession(true);
                        return true;
                        Session.track(req, token, 'noAddress');
                    }
                    let dispensaries = Business.localDispensaries(address);
                    if (!dispensaries.length) {
                        res.say("I could not find any dispensaries in your area at this time. Please try again later.")
                    } else {
                        _.each(dispensaries, function(it) {
                            it.distance = Math.abs(parseInt(it.zip) - parseInt(address.postalCode))
                        })
                        dispensaries = _.sortBy(dispensaries, function(it) {
                            return it.distance;
                        })
                        let d = dispensaries[0];
                        console.log(dispensaries)
                        res.say("The closest dispensary is " + d.name + ' in ' + d.city)
                    }
                }
                app.track(req, Token);
            }
            return app.initialize(req, res, near);
        } catch (e) {
            console.log(e)
            app.fail(req, res, Token, Strings.criticalError, e);
        }
    });
    return alexa;
}
module.exports = action;

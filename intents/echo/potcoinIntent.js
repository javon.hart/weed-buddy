let Session = require('../../utils/session');
let Strings = require('../../res/strings');
let PotCoin = require('../../models/pot');
let Token = 'alexaApp.potCoin';
let request = require('request-promise')
let cheerio = require('cheerio')

function action(alexa,app) {
	alexa.intent("PotCoinIntent", function(req, res) {
		try {
		    return request('https://coinmarketcap.com/currencies/potcoin/', function (error, response, html) {
				app.track(req, Token);
		        if (!error && response.statusCode == 200) {
		            let $ = cheerio.load(html);
		            let $text = $('#quote_price');
		            let str = $text.text().replace(/(\r\n|\n|\r)/gm," ").trim().replace('USD', ' ');
    	            let say = '<say-as interpret-as="unit">$' + str +'</say-as>';
					res.say(say).shouldEndSession(true);
		        }
		    });			
		} catch (e) {
			console.log(e)
			app.fail(req,res,token,Strings.criticalError,e);
		}
	});
	return alexa;
}
module.exports = action;
let mongoose = require('mongoose');
let strainSchema = mongoose.Schema({
    type: String,
    name: String,
    description: String,
    medical: String,
    effects: String,
    flavors: String
});
let Strain = mongoose.model('Strain', strainSchema);
let url = process.env.MONGODB_URI || 'mongodb://localhost:27017/buddy';
mongoose.connect(url);


let type = "Sativa";
let name = "Jack Herer";
let description = "Jack Herer is a sativa-dominant cannabis strain that has gained as much renown as its namesake, the marijuana activist and author of The Emperor Wears No Clothes. Combining a Haze hybrid with a Northern Lights #5 and Shiva Skunk cross, Sensi Seeds created Jack Herer hoping to capture both the cerebral elevation associated with sativas and the heavy resin production of indicas. Its rich genetic background gives rise to several different variations of Jack Herer, each phenotype bearing its own unique features and effects. However, consumers typically describe this 55% sativa hybrid as blissful, clear-headed, and creative. Jack Herer was created in the Netherlands in the mid-1990s where it was later distributed by Dutch pharmacies as a recognized medical-grade strain. Since then, the spicy, pine-scented sativa has taken home numerous awards for its quality and potency. Many breeders have attempted to cultivate this staple strain themselves in sunny or Mediterranean climates, and indoor growers should wait 50 to 70 days for Jack Herer to flower.";
let effects = "Happy Uplifted Energetic Euphoric Creative";
let medical = "Stress Pain Fatigue Depression Appetite";
let flavors = "Earthy Woody and Pungent";


let strain = new Strain();
strain.type = type;
strain.name = name;
strain.description = description;
strain.effects = effects;
strain.flavors = flavors;
strain.medical = medical;
strain.save(function(err){
  if(err) {
    console.log('Issue saving.');
  }
  console.log('Strain added!');
});




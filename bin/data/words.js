var _ = require('underscore')
var words = require('./terms')
var salient = require('salient');
var glossary = new salient.glossary.Glossary();
_.each(words, function(w){
	glossary.parse(w)
	console.log(glossary.toJSON());
	console.log('\n');
})

let MemoryStorage = require('memorystorage');
let waterfall = require('async-waterfall');
let Local = require('../models/local');
let Name = require('../models/name');
let Session = require('../models/session');
let session = new Session();
let mem = session.getStore();
let User = require('../models/user');
let Update = require('../models/update');
let Company = require('../models/company');
let Interview = require('../models/interview');
let Community = require('../models/community');
let Potcoin = require('../models/pot');
let _ = require('underscore');
let mongoose = require("mongoose");
let url = process.env.MONGODB_URI || "mongodb://localhost/buddy";
mongoose.connect(url);
function persistApplication() {
    try {
        waterfall([
            function(callback) {
                let mem = new MemoryStorage('weed-buddy');
                User.find({}, function(err, users) {
                    let c = [];
                    _.each(users, function(o) {
                        mem.setItem(o.amzn_id, o);
                        c.push(o)
                    });
                    mem.setItem('currentUsers', c);
                    callback(null, mem);
                });
            },
            function(mem, callback) {
                Company.findOne({}, function(err, doc) {
                    if (err) {
                        console.log(err);
                    } else {
                        let data;
                        if (!doc) data = 'bar foo';
                        else data = doc.name;
                        mem.setItem('bpo', data);
                    }
                    callback(null, mem);
                });
            },
            function(mem, callback) {
                Interview.findOne({}, function(err, url) {
                    if (err) {
                        console.log(err);
                    } else {
                        if (!url) url = {
                            url: ''
                        };
                        mem.setItem('interview', url.url);
                        callback(null, mem);
                    }
                });
            },
            function(mem, callback) {
                mem.setItem('Updates', [])
                Update.find({}, function(err, upds) {
                    let c = [];
                    _.each(upds, function(o) {
                        mem.getItem('Updates').push(o);
                    });
                    callback(null, mem);
                });
            },            
            function(mem, callback) {
                Potcoin.findOne({}, {}, {
                    sort: {
                        'created_at': -1
                    }
                }, function(err, pot) {
                    if (err) {
                        console.log(err);
                    } else {
                        mem.setItem('pot', pot);
                        callback(null, mem);
                    }
                });
            },
            function(mem, callback) {
                Community.findOne({}, function(err, cso) {
                    if (cso) {
                        mem.setItem('cso', cso.text);
                    } else {
                        mem.setItem('cso', "Follow me on twitter at alexa, weed, buddy!");
                    }
                    callback(null, mem);
                });
            },
            function(mem, callback) {
                Name.findOne({}, function(err, id) {
                    if (!id) {
                        mem.setItem('nameService', false);
                    } else {
                        mem.setItem('nameService', id.index);
                    }
                    callback(null, mem);
                });
            },
            function(mem, callback) {
                let localUsers = mem.getItem('currentUsers');
                _.map(localUsers, function(item) {
                    if (typeof item.address === 'string') {
                        item.address = JSON.parse(item.address)
                    }
                });
                localUsers = _.filter(localUsers, function(item) {
                    return item.address
                });
                mem.setItem('localUsers', localUsers);
                callback(null, mem);
            }
        ], function(err, result) {
            Local.remove({}, function(err) {
                if (err) console.log(err);
            }); 
            let local = new Local();
                local.data = JSON.stringify(result);
                local.created_at = new Date();
                local.save(function(err) {
                    if(err) console.log(err)
                    else console.log('Application state saved.');
                    process.exit()
                }); 
        });
    } catch (e) {
        console.log(e)
    }
}
// setInterval(persistApplication, 3000)
persistApplication()

var Chance = require('chance');
var chance = new Chance();
let stories = require('./ingest.js')
let Strains = require('../res/strains')
var webdriver = require('selenium-webdriver'),
	chrome = require('selenium-webdriver/chrome');
let i = 0;
var fs = require('fs');
var driver = new webdriver.Builder().forBrowser('chrome')
	// .setChromeOptions(/* ... */)
	.build();
var prompt = require('prompt');
var leafly = true; 
var wikileaf = false;
var By = require('selenium-webdriver').By


function readyBrowser() {
	prompt.get(['ready'], function(err, result) {
		// 
		// Log the results. 
		// 
		if (result.ready == 'yes') {
			console.log('open browser')
			openBrowser(Strains[i])
		}
	});
}

function readyUrl() {
	prompt.get(['next'], function(err, result) {
		if (result.next == 'yes') {
			getUrl()
		} else {
			i++;
			openBrowser(Strains[i])
		}
	})
}

function getUrl() {
	// let url = driver.getCurrentUrl().then(function(url) {
	// 	console.log('currentUrl', url)
	// 	stories[i].url = url;
	// 	stories[i].pin = chance.bb_pin();
	// 	fs.appendFile('parsed_stories.txt', JSON.stringify(stories[i]).concat(',\n'), function(err) {
	// 		if (err) throw err;
	// 		console.log('Saved!');
			// driver.quit()
			// increaseI()
			openBrowser(Strains[i])
		//});
	//});
}

function increaseI() {
	i += 1
}

function openBrowser(data) {
	if(leafly) {
		driver.get('https://www.leafly.com/search?q=' + encodeURI(data.name) + "&typefilter=strain")
		leafly = false; 
		wikileaf = true
		readyUrl()
		return		
	}
	if(wikileaf) {
		driver.get('https://www.google.com/search?q=wikileaf+'+ encodeURI(data.name))
		wikileaf = false;
		setTimeout(function(){
		  driver.findElement(By.css('h3 a')).click()
		},1000);
		readyUrl()
		return
	}	
	if(!wikileaf && !leafly) {
		leafly = true
		wikileaf = false
		increaseI()
		openBrowser(Strains[i])
	}
}

function openBrowserTwo(i) {
	// driver.get('https://www.leafly.com/search?q=' + encodeURI(i.name) + "&typefilter=strain")
	// readyUrl()
}

prompt.start();		var data = Strains[i];


prompt.get(['ready'], function(err, result) {
	if (result.ready == 'yes') {
		console.log('open leafly')
		openBrowser(data)
	}
});

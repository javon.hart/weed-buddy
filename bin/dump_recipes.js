let mongoose = require('mongoose');
let _ = require('underscore');
let url = process.env.MONGODB_URI || 'mongodb://localhost:27017/buddy';
mongoose.connect(url);

let fs = require('fs');


let RecipeSchema = mongoose.Schema({
    ingredients: Array,
    title: String,
    directions: String
});

let Recipe = mongoose.model('Recipe', RecipeSchema);

Recipe.find({}, function(err, docs) {
	let recipes = _.uniq(_.pluck(docs, "title"));
	fs.writeFile("../res/recipe_names.json", JSON.stringify(recipes), function(err) {
	    if(err) {
	        return console.log(err);
	    }
	    console.log("The file was saved!");
	});
});

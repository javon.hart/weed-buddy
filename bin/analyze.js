'use strict';

let fs = require("fs");
let clean = require('../res/cleanNumbers');

class AnalyticsReport {

  constructor(){
    this.total = 0;
    let Converter = require("csvtojson").Converter;
    let csvFileName="../res/analyticsData.csv";
    let csvConverter=new Converter({});
    csvConverter.on("end_parsed",function(data){
      fs.writeFile("../res/numbers.json", JSON.stringify(data), function(err) {
          if(err) {
              return console.log(err);
          }
      });
    });
    fs.createReadStream(csvFileName).pipe(csvConverter);
  }

  userGeneratedContent(){
    return require('../res/numbers');
  }

  totalConversationTime(numbers){
    let convert = {};
    let _ = require('underscore');
    _.each(numbers, function(o){
      let keys = Object.keys(o);
      let intentId = (o[keys[0]]);
      if(intentId.indexOf('alexaApp') != -1){    
        if(intentId.indexOf('#') == -1){
          convert[intentId] = o.field2;
        } else if(intentId.indexOf('user') == -1) {
          if(intentId.indexOf('search') != -1 ) {
            convert = search(intentId,convert,o);
          } else if(intentId.indexOf('list') != -1 ) {
            convert = list(intentId,convert,o);
          } else if(intentId.indexOf('flavors') != -1 ) {
            convert = flavors(intentId,convert,o);
          } else if(intentId.indexOf('recipe') != -1 ) {
            convert = recipe(intentId,convert,o);
          } else if(intentId.indexOf('effects') != -1 ) {
            convert = effects(intentId,convert,o);
          } else if(intentId.indexOf('medical') != -1 ) {
            convert = medical(intentId,convert,o);
          } else if(intentId.indexOf('type') != -1 ) {
            convert = type(intentId,convert,o);
          } else {
            console.log(intentId,o);
          }
        } else {
          let add = convert['alexaApp.undefined'];
          add = add || 0; 
          convert['alexaApp.undefined'] = add + Number(o["field2"].replace(',','')); 
        }
      } 
    });
    return convert;
  }
  humanReadableData(data) {
        fs.writeFile("../res/cleanNumbers.json", JSON.stringify(data), function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
  }
  community(){
    let id = "alexaApp.community";
    this.total += time(id,300);
  }
  launch(){
    let id = "alexaApp.launch";
    this.total += time(id,10);
  }
  stop(){
    let id = "alexaApp.stop";
    this.total += time(id,2);
  }
  joke(){
    let id = "alexaApp.joke";
    this.total += time(id,7);
  }
  help(){
    let id = "alexaApp.help";
    this.total += time(id,30);
  }
  recipe(){
    let id = "alexaApp.recipe";
    this.total += time(id,5);
  }
  fact(){
    let id = "alexaApp.fact";
    this.total += time(id,10);
  }
  random(){
    let id = "alexaApp.random";
    this.total += time(id,45);
  }
  business(){
    let id = "alexaApp.business";
    this.total += time(id,30);
  }
  rewards(){
    let id = "alexaApp.rewards";
    this.total += time(id,7);
  }
  searchFail(){
    let id = "alexaApp.search#fail";
    this.total += time(id,10);
  }  
  searchByFail(){
    let id = "alexaApp.searchBy#fail";
    this.total += time(id,7);
  }  
  search(){
    let id = "alexaApp.search";
    this.total += time(id,60);
  }  
  searchBy(){
    let id = "alexaApp.searchBy";
    this.total += time(id,15);
  }  
  list(){
    let id = "alexaApp.list";
    this.total += time(id,60);
  }  
  listFail(){
    let id = "alexaApp.list#fail";
    this.total += time(id,10);
  }  
  flavors(){
    let id = "alexaApp.flavors";
    this.total += time(id,10);
  }  
  flavorsFail(){
    let id = "alexaApp.flavors#fail";
    this.total += time(id,10);
  }  
  recipe(){
    let id = "alexaApp.recipeDesc";
    this.total += time(id,180);
  }  
  recipeFail(){
    let id = "alexaApp.recipeDesc#fail";
    this.total += time(id,10);
  }  
  effects(){
    let id = "alexaApp.effects";
    this.total += time(id,5);
  }  
  effectsFail(){
    let id = "alexaApp.effects#fail";
    this.total += time(id,5);
  }  
  type(){
    let id = "alexaApp.type";
    this.total += time(id,5);
  }  
  typeFail(){
    let id = "alexaApp.type#fail";
    this.total += time(id,5);
  }  
  medical(){
    let id = "alexaApp.medical";
    this.total += time(id,5);
  }  
  medicalFail(){
    let id = "alexaApp.medical#fail";
    this.total += time(id,5);
  }  
  none(){
    let id = "alexaApp.undefined";
    this.total += time(id,60);
  }  

  total() {
    console.log(this.total);
  }

}

function time(id,time){ 
    let events = clean[id];
    if(typeof events == 'string'){
      events = Number(clean[id].replace(',',''));
    }
    let total = ((events*time)/60 )/60;
    console.log((total.toString().concat(' hrs | '.concat(id))));
    return total;
}

function search(intentId,convert,o) {
  if(intentId.indexOf('searchBy') != -1 ) {
    if(intentId.indexOf('fail') != -1 ) {
      let add = convert['alexaApp.searchBy#fail'];
      add = add || 0; 
      convert['alexaApp.searchBy#fail'] = add + Number(o["field2"].replace(',','')); 
    } else {
      let add = convert['alexaApp.searchBy'];
      add = add || 0; 
      convert['alexaApp.searchBy'] = add + Number(o["field2"].replace(',','')); 
    }
  } else {
    if(intentId.indexOf('fail') != -1 ) {
      let add = convert['alexaApp.search#fail'];
      add = add || 0; 
      convert['alexaApp.search#fail'] = add + Number(o["field2"].replace(',','')); 
    } else {
      let add = convert['alexaApp.search'];
      add = add || 0; 
      convert['alexaApp.search'] = add + Number(o["field2"].replace(',','')); 
    }
  }  
  return convert;
}

function flavors(intentId,convert,o) {
  if(intentId.indexOf('fail') != -1){
    let add = convert['alexaApp.flavors#fail'];
    add = add || 0; 
    convert['alexaApp.flavors#fail'] = add + Number(o["field2"].replace(',','')); 
  } else {
    let add = convert['alexaApp.flavors'];
    add = add || 0; 
    convert['alexaApp.flavors'] = add + Number(o["field2"].replace(',','')); 
  }
  return convert;
}

function recipe(intentId,convert,o) {
  if(intentId.indexOf('fail') != -1){
    let add = convert['alexaApp.recipeDesc#fail'];
    add = add || 0; 
    convert['alexaApp.recipeDesc#fail'] = add + Number(o["field2"].replace(',','')); 
  } else {
    let add = convert['alexaApp.recipeDesc'];
    add = add || 0; 
    convert['alexaApp.recipeDesc'] = add + Number(o["field2"].replace(',','')); 
  }
  return convert;
}

function medical(intentId,convert,o) {
  if(intentId.indexOf('fail') != -1){
    let add = convert['alexaApp.medical#fail'];
    add = add || 0; 
    convert['alexaApp.medical#fail'] = add + Number(o["field2"].replace(',','')); 
  } else {
    let add = convert['alexaApp.medical'];
    add = add || 0; 
    convert['alexaApp.medical'] = add + Number(o["field2"].replace(',','')); 
  }
  return convert;
}

function type(intentId,convert,o) {
  if(intentId.indexOf('fail') != -1){
    let add = convert['alexaApp.type#fail'];
    add = add || 0; 
    convert['alexaApp.type#fail'] = add + Number(o["field2"].replace(',','')); 
  } else {
    let add = convert['alexaApp.type'];
    add = add || 0; 
    convert['alexaApp.type'] = add + Number(o["field2"].replace(',','')); 
  }
  return convert;
}

function effects(intentId,convert,o) {
  if(intentId.indexOf('fail') != -1){
    let add = convert['alexaApp.effects#fail'];
    add = add || 0; 
    convert['alexaApp.effects#fail'] = add + Number(o["field2"].replace(',','')); 
  } else {
    let add = convert['alexaApp.effects'];
    add = add || 0; 
    convert['alexaApp.effects'] = add + Number(o["field2"].replace(',','')); 
  }
  return convert;
}

function list(intentId,convert,o) {
  if(intentId.indexOf('fail') != -1){
    let add = convert['alexaApp.list#fail'];
    add = add || 0; 
    convert['alexaApp.list#fail'] = add + Number(o["field2"].replace(',','')); 
  } else {
    let add = convert['alexaApp.list'];
    add = add || 0; 
    convert['alexaApp.list'] = add + Number(o["field2"].replace(',','')); 
  }
  return convert;
}


let u = new AnalyticsReport();
let a = u.userGeneratedContent();
let t = u.totalConversationTime(a);
u.humanReadableData(t);
u.launch();
u.community();
u.recipe();
u.joke();
u.fact();
u.random();
u.business();
u.help();
u.stop();
u.rewards();
u.searchFail();
u.searchByFail();
u.search();
u.searchBy();
u.list();
u.listFail();
u.flavors();
u.flavorsFail();
u.recipe();
u.recipeFail();
u.effects();
u.effectsFail();
u.type();
u.typeFail();
u.medical();
u.medicalFail();
u.none();
// u.total()
module.exports = u;
// module.exports = new AnalyticsReport();



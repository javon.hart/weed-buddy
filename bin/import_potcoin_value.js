let request = require('request'),
cheerio = require('cheerio'),
_ = require('underscore'),
pot = require('../models/pot');

/*** Mongo */
let mongoose = require("mongoose"),
url = process.env.MONGODB_URI || "mongodb://localhost/buddy";
console.log(url)
mongoose.connect(url);

function getPot() {
    request('https://coinmarketcap.com/currencies/potcoin/', function (error, response, html) {
        if (!error && response.statusCode == 200) {
            let $ = cheerio.load(html);
            let $text = $('#quote_price');
            let str = $text.text().replace(/(\r\n|\n|\r)/gm," ").trim().replace('USD', ' ');
            console.log(str)
            pot.remove({}, function(err) {
                if (err) {
                    console.log(err);
                }      
                p = new pot()
                p.prompt = str;
                p.save()     
            });
        } else {
            console.log('fail')
            console.log(error)
            console.log(response.statusCode)
        }
    });
}

getPot()

setInterval(function() {
	try {
		getPot()
	} catch(e) {
		console.log(e)
	}
}, 30000);





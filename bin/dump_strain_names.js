let _ = require('underscore');
let mongoose = require('mongoose');
let url = process.env.MONGODB_URI || 'mongodb://localhost:27017/buddy';
mongoose.connect(url);
let fs = require('fs');
let strainSchema = mongoose.Schema({
    type: String,
    name: String,
    description: String,
    medical: String,
    effects: String,
    flavors: String
});
let Strain = mongoose.model('Strain', strainSchema);
Strain.find({}, function(err, docs) {
	let res = _.uniq(_.pluck(docs, "name"));
	let json = JSON.stringify(res);
	fs.writeFile("../res/all_strains.js", json, function(err) {
	    if(err) {
	        return console.log(err);
	    }
	    console.log("The file was saved!");
	});
});

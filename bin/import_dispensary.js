//let endpoints = require('../res/prospects.js'),
let endpoints = require('../res/new_leads.js'),
cheerio = require('cheerio'),
_ = require('underscore'),
req = require('request'),
count = 0,
model = {},
prettyjson = require('prettyjson'),
options = {
  keysColor: 'rainbow',
  dashColor: 'magenta',
  stringColor: 'white'	
},
fs = require('fs');
endpoints = _.uniq(endpoints);

let counter = setInterval(function(){
	if(count==endpoints.length) {
	    clearInterval(counter);
	    return;
	};
	let url = endpoints[count];

	req(url, function (error, response, html) {
	  	if (!error && response.statusCode == 200) {
	    	let $ = cheerio.load(html);
	    	model.name = $('.listing-name').text();
	    	model.phone = $($('.listing-contacts a')[0]).text();
	    	model.email = $($('.listing-contacts a')[1]).text();
	    	_.each($('.listing-address span'), function(span){
	    		let prop = $(span).attr('itemprop');
	    		if(prop=='postalCode') model.zip = $(span).text();
	    		if(prop=='addressRegion') model.state = $(span).text();
	    		if(prop=='addressLocality') model.city = $(span).text();
	    		if(prop=='streetAddress') model.streetAddress = $(span).text();
	    	});
	    	console.log(url);
	    	console.log(prettyjson.render(model, options));
	    	console.log('\n');
	    	if(model.name){
	    		fs.appendFileSync('../res/new_leads.txt', JSON.stringify(model));
	    		fs.appendFileSync('../res/new_leads.txt', '\n');
	    	}
	  	}
	});
	count += 1;
}, 3000);


let fs = require('fs'),
lineReader = require('readline').createInterface({
  input: fs.createReadStream('../res/new_leads.txt')
}),
out = [];
lineReader.on('line', function (line) {
  out.push(JSON.parse(line));
}).on('close', function (err) {
  	if (err) throw err;
	fs.writeFile("../res/parsed_new_leads.js", JSON.stringify(out), function(err) {
	    if(err) {
	        return console.log(err);
	    }
	    console.log("The file was saved!");
	});

});
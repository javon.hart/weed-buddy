let mongoose = require('mongoose');
let RecipeSchema = mongoose.Schema({
    ingredients: Array,
    title: String,
    directions: String
});
let Recipe = mongoose.model('Recipe', RecipeSchema);
let url = process.env.MONGODB_URI || 'mongodb://localhost:27017/buddy';
mongoose.connect(url);
let request = require('request'),
cheerio = require('cheerio'),
_ = require('underscore'),
thanks = require('./thankyou');
let fs = require('fs');

function load(endpoint) {
	console.log(endpoint, '\n');
	request(endpoint, function (error, response, html) {
	  	if (!error && response.statusCode == 200) {
	    	let $ = cheerio.load(html),
	    	client = $($('.listing-contacts a')[1]).text();
	    	// thanks(client);
	    	saveEmail(client)
	  	}
	});
}

function cannabissearchrecipe(endpoint) {
	console.log(endpoint, '\n');
	request(endpoint, function (error, response, html) {
	  	if (!error && response.statusCode == 200) {
	    	let $ = cheerio.load(html),
	    	title = $('.main_strain_left h1').text(),
	    	ingredients = [];	  	
	    	_.each($('.main_strain_left li'), function(i){ 
	    		ingredients.push(i.children.pop().data); 
	    	});
	    	let directions = $($('.main_strain_left p')[2]).text();
	    	let recipe = new Recipe();
	    	recipe.directions = directions;
	    	recipe.ingredients = ingredients; 
	    	recipe.title = title;
	    	console.log(recipe)
	    	recipe.save();
	    }
	});
}

function saveEmail(client) {
	fs.appendFileSync('../res/emails.txt', client);
	fs.appendFileSync('../res/emails.txt', '\n');
	let out = client || 'ADDRESS_NOT_FOUND' 
	console.log(out)
}


class ImportUtil {
	static load(endpoint) {
		load(endpoint);
	}
}

// cannabissearchrecipe('http://www.cannabissearch.com/edibles/420-fudge/');

module.exports = ImportUtil;
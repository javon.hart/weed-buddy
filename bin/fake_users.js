'use strict';

const uuidv1 = require('uuid/v1');
let User = require('../models/user');
let mongoose = require("mongoose"),
url = process.env.MONGODB_URI || "mongodb://localhost/buddy";
mongoose.connect(url);
let Chance = require('chance'),
	chance = new Chance();
let x = 0; 
do {
	let newPerson = new User();
	newPerson.amzn_id = uuidv1()
	newPerson._id = mongoose.Types.ObjectId();
	newPerson.gender = chance.gender();
	newPerson.name = chance.name({ gender: newPerson.gender });
	newPerson.genderBuddy = chance.gender();
    newPerson.address = { postalCode: 27613 }
    // newPerson.address = { postalCode: chance.zip() }
	newPerson.myAge = Math.floor(Math.random() * 55) + 18
	newPerson.minimumAge = Math.floor(Math.random() * 27) + 18
	newPerson.maximumAge = Math.floor(Math.random() * 27) + 45
	console.log(newPerson)
	newPerson.save(function(err){
	    console.log(err)
	    if(err) process.exit()
	    console.log(200)
	});
	x++; 
} while(x < 30)
let request = require('request'),
cheerio = require('cheerio'),
_ = require('underscore');
require('events').EventEmitter.prototype._maxListeners = 100;

let stories = [];

console.log('loading')
function load(endpoint) {
	console.log(endpoint, '\n');
	request(endpoint, function (error, response, html) {
	  	if (!error && response.statusCode == 200) {
	    	let $ = cheerio.load(html),
	    	links = $('.hll');

	    	_.each(links,function(i){
	    		let title = $(i).text().concat('\n');
	    		console.log(title);
	    		let url = $(i).attr('href').concat('\n').replace('//','https://');
	    		console.log(url);
	    		stories.push({ title: title, url: url })
	    	});
	    	console.log('writing')
	    	let fs = require('fs')
	    	fs.writeFile("./ingest.js", 'module.exports = ' + JSON.stringify(stories) + ';', function(err) {
			    if(err) {
			        return console.log(err);
			    }
			    console.log("The file was saved!");
			});


	  	}
	});
}

load('http://www.newsnow.co.uk/h/Lifestyle/Drugs+&+Alcohol/Cannabis');
// load('http://www.newsnow.co.uk/h/?search=amazon+alexa&lang=en&searchheadlines=1')
module.exports = load;

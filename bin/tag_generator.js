let getval = function() {
	return Math.floor(Math.random() * 847) + 1;
};

let p = require('../res/prospects');

let getTags = function() {
	let d = 0, tags = '';
	while (d <= 5) {
		let id = getval();
		tags += ' ' + ('#' + p[id].split('/').pop().replace(/-/g, ''));
		d += 1;
	}
	return tags;
}

module.exports = getTags;

// Instabot Tags
d = 0, tags = '';
while (d <= 10) {
	let id = getval();
	tags += ' ' + '#' + p[id].split('/').pop().replace(/-/g, '') + '';
	d += 1;
}

console.log(tags)
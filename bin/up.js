let users = []
let _ = require('underscore');
let moment = require('moment');
let StrainUtil = require('../utils/strain');
let RecipeUtil = require('../utils/recipe');
let stories = _.shuffle((require('./stories')));
let fs = require('fs')
let TwitterBot = require("node-twitterbot").TwitterBot
let Bot = new TwitterBot({
	"consumer_secret": 'MneUGJ3Ebp1fT4jsO5KUF8IqWNbBhUgyOfGqGOEX01ZvTsnNG7',
	"consumer_key": "ZxDfXtkjHmKdUJjbWoojmayPQ",
	"access_token": "811244597028540417-NQAFXW3lZQkgYCYHk1zv9BamFZYFuul",
	"access_token_secret": "z3PueGWIRXjwlLazHR513yV03DkOmGZqtPkApoQmWb2SJ"
});
var Twit = require('twit')
let T = new Twit({
	"consumer_secret": 'MneUGJ3Ebp1fT4jsO5KUF8IqWNbBhUgyOfGqGOEX01ZvTsnNG7',
	"consumer_key": "ZxDfXtkjHmKdUJjbWoojmayPQ",
	"access_token": "811244597028540417-NQAFXW3lZQkgYCYHk1zv9BamFZYFuul",
	"access_token_secret": "z3PueGWIRXjwlLazHR513yV03DkOmGZqtPkApoQmWb2SJ"
})
let time = 10;
let flavors = require('../res/flavors');
let effects = require('../res/effects');
let benefits = require('../res/benefits');
let business = require('../res/business');
let CronJob = require('cron').CronJob;
let tags = require('./tags')
let p = require('../res/prospects');

let getval = function() {
	return Math.floor(Math.random() * 847) + 1;
};
let getTags = function() {
	let d = 0, tags = '';
	while (d <= 5) {
		let id = getval();
		tags += ' ' + ('#' + p[id].split('/').pop().replace(/-/g, ''));
		d += 1;
	}
	return tags;
}
let randomTags = function() {
	d = 0, tags = '';
	while (d <= 10) {
		let id = getval();
		tags += ' ' + '#' + p[id].split('/').pop().replace(/-/g, '') + '';
		d += 1;
	}
	return tags	
};
let randomStrain = function() {
	return _.shuffle(StrainUtil.allStrains()).pop().replace(/\s/g,'');
};
let randomRecipe = function() {
	return _.shuffle(RecipeUtil.allRecipes()).pop().replace(/\s/g,'');
};
let randomFlavor = function() {
	return _.shuffle(flavors).pop();
};
let randomEffect = function() {
	return StrainUtil.colloquial(_.shuffle(effects).pop());
};
let randomBenefit = function() {
	return  _.shuffle(benefits).pop();
};
let randomBusiness = function() {
	return  _.shuffle(business).pop().name;
};
let news = function() {
	stories = _.shuffle((require('./stories')))
	console.log(stories.length)
	// if(stories.length < 10) {
	// 	let sys = require('sys');
	// 	let exec = require('child_process').exec;
	// 	function puts(error, stdout) { sys.puts(stdout); };
	// 	let command  = "node news";
	// 	console.log(command.concat('\n'));
	// 	exec(command, puts);
	// }
	let story = stories.pop();
	fs.writeFileSync('./stories.js', 'module.exports = ' + JSON.stringify(stories) + ';');
	return story.title.concat(" ").concat('http://www.weedbuddy.io/news?'+story.pin);
}

let items = {
	review: "#Fans! Help me go viral and #shareme #peaceandlove https://www.amazon.com/.../dp/B01MU1GYOX/",
	viral: "#Fans! Be kind and leave us a review on #Amazon.com https://www.amazon.com/.../dp/B01MU1GYOX/",
	more: "#Alexa ask Weed Buddy for the most #popular #strain http://bit.ly/use-app",
	// popular: "#Alexa ask for information about #" + randomBusiness() + " http://bit.ly/use-app",
	// business: "#Alexa ask Weed Buddy for #Business http://bit.ly/use-app",
	lucky: "#Alexa tell Weed Buddy im feeling lucky! http://bit.ly/use-app",
	recipe: '#Alexa, open Weed Buddy and list all recipes! http://bit.ly/use-app',
	joke: '#Alexa, open Weed Buddy and tell me a joke! http://bit.ly/use-app',
	fact: '#Alexa, open Weed Buddy and tell me a fact http://bit.ly/use-app',
	search: '#Alexa, open Weed Buddy and search #' + randomStrain() + ' http://bit.ly/use-app',
	good: "#Alexa, open Weed Buddy and search whats good for #" + randomBenefit() + " http://bit.ly/use-app",
	helps: "#Alexa, open Weed Buddy and search what strain helps with #" + randomBenefit() + " http://bit.ly/use-app",
	suggest: "#Alexa, open Weed Buddy and suggest weed good for #" + randomBenefit() + " http://bit.ly/use-app",
	effects: '#Alexa, open Weed Buddy and describe the effects of #' + randomStrain() + ' http://bit.ly/use-app',
	effects2: '#Alexa, ask Weed Buddy for strains with #' + randomEffect() + ' effects http://bit.ly/use-app',
	identify: '#Alexa, open Weed Buddy and identify #' + randomStrain() + ' http://bit.ly/use-app',
	research: '#Alexa, open Weed Buddy and research #' + randomStrain() + ' http://bit.ly/use-app',
	flavor: '#Alexa, ask Weed Buddy for the flavor of #' + randomStrain() + ' http://bit.ly/use-app',
	taste: '#Alexa, ask Weed Buddy what does #' + randomStrain() + ' taste like? http://bit.ly/use-app',
	flavored: '#Alexa, ask Weed Buddy for #' + randomFlavor() + ' flavored strains http://bit.ly/use-app',
	make: '#Alexa, open Weed Buddy and search how to make #' + randomRecipe() + ' http://bit.ly/use-app',
	searchBy: '#Alexa, ask Weed Buddy to search strains good for pain. http://bit.ly/use-app',
	all: '#Alexa, ask Weed Buddy for all weed http://bit.ly/use-app',
	list: '#Alexa, ask Weed Buddy to list all strains http://bit.ly/use-app',
	random: '#Alexa, ask Weed Buddy to list random strains http://bit.ly/use-app',
	sativa: '#Alexa, ask Weed Buddy to list all sativa strains http://bit.ly/use-app',
	hybrid: '#Alexa, ask Weed Buddy to list all hybrid strains http://bit.ly/use-app',
	allStrains: '#Alexa, ask Weed Buddy what strains do you know http://bit.ly/use-app',
	medical: '#Alexa, ask Weed Buddy for the benefit of #' + randomStrain() + ' http://bit.ly/use-app',
	feel: '#Alexa, ask Weed Buddy what side effects does #' + randomStrain() + ' have http://bit.ly/use-app',
	sideEffects: '#Alexa, ask Weed Buddy how is #' + randomStrain() + ' going to make me feel http://bit.ly/use-app',
	bestU: '#Alexa, ask Weed Buddy what is #' + randomStrain() + ' best used for http://bit.ly/use-app',
	best: '#Alexa, ask Weed Buddy for the medicinal value of #' + randomStrain() + ' http://bit.ly/use-app',
		//community: '#Alexa, ask Weed Buddy for articles http://bit.ly/use-app',
	wake: '#Alexa, ask Weed Buddy what should I wake and bake http://bit.ly/use-app',
	like: '#Like #Share #Enjoy http://bit.ly/use-app',
	// interview: 'Say #Alexa ask Weed Buddy for interviews, to listen to @Ganjly420 @HIGH_TIMES_Mag #interview.',
	// dispensaryAdd: 'Add your #dispensary at www.weedbuddy.io/signup',
	// deliveryAdd: 'Add your cannabis delivery company to #Alexa at https://www.weedbuddy.io/signup',
	// companyAdd: 'Add your cannabis company to #Alexa at https://www.weedbuddy.io/signup',
	// voice: 'Custom cannabis voice for#alexa at https://www.weedbuddy.io/signup',
	// interview: 'Say #Alexa, ask Weed Buddy for interviews, to listen to @Ganjly420 @HIGH_TIMES_Mag #interview.',
	// news: news(),
	wod: '#Alexa, ask Weed Buddy for the word of the day',
	pot: '#Alexa, ask Weed Buddy for the price of potcoin',
	chatbottle: 'Checkout Weed Buddy on @chatbottle https://chatbottle.co/bots/weed-buddy-amazon-alexa-skill',
	botlist: 'Checkout Weed Buddy, on @botlistco:  https://botlist.co/bots/weed-buddy',
	// reddit: 'Checkout Weed Buddy on @reddit https://www.reddit.com/r/WeedBuddy/',
	number1: '#1 Alexa app for Cannabis. http://bit.ly/use-app',
	randomTags: "Random Shouts: " + randomTags(),
};


Bot.addAction("tweet", function(twitter, action, tweet) {
	let id = _.shuffle(Object.keys(items)).pop()
	let item = items[id];
	if (!item) process.exit();
	console.log(moment().format().concat('\n'));
  	console.log(item)  
	Bot.tweet(item)
});

	Bot.now('tweet')


new CronJob('*/' + (Math.floor(Math.random() * 25) + 8) + ' * * * *', function() {
	Bot.now('tweet')
}, null, true, 'America/Los_Angeles');

// new CronJob('*/' + (Math.floor(Math.random() * 25) + 19) + ' * * * *', function() {
// 	time = Math.floor(Math.random() * 200) + 100;
// 	followBot()
// }, null, true, 'America/Los_Angeles');

let tracked = [];
function followBot () {
	T.get('followers/ids', { screen_name: 'amazonecho' },  function (err, data, response) {
	  	var ids = _.shuffle(data.ids)
	  	if(err) console.log(err.message)
  		_.each(ids,function(it){
  			users.push(it)
  		})
	  	users = _.shuffle(_.uniq(users))
	})
	if(users.length) {
		for (i = 0; i < 24; i++) {
			let id = (users.pop())						 
			console.log(id)
			if(!tracked.includes(id)){
				T.post('friendships/create', { id: id }, function(err, response){
 					if(err)console.log(err.message)
	          			if(!err)console.log(response.screen_name, ': **FOLLOWED**');
	 	 		})
			}
			tracked.push(id);
		}
	}
}

// followBot()


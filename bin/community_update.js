var Chance = require('chance');
// Instantiate Chance so it can be used
var chance = new Chance();
var _ = require('underscore')
let stories = _.uniq(require('./ingest.js'),function(x){
   return x.url;
});
var webdriver = require('selenium-webdriver'),
	chrome = require('selenium-webdriver/chrome');
let i = 0;
var fs = require('fs');
var path = require('chromedriver').path;

var service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);

var driver = new webdriver.Builder()
    .withCapabilities(webdriver.Capabilities.chrome())
    .build();

var prompt = require('prompt');
                                            let pry = require('pryjs')

prompt.start();
prompt.get(['ready'], function(err, result) {
	if (result.ready == 'yes') {
		console.log('open browser')
		openBrowser(i)
	} else {
		console.log('open browser')
		openBrowser(parseInt(result.ready))
    }
});

function getUrl() {
let community = require('../res/community')
	let url = driver.getCurrentUrl().then(function(url) {
		console.log('currentUrl', url)
		stories[i].url = url;
		stories[i].pin = chance.bb_pin();
		fs.appendFile('parsed_stories.txt', JSON.stringify(stories[i]).concat(',\n'), function(err) {
			if (err) throw err;
			console.log('Saved!');
            prompt.get(['community'], function(err, result) {
                if (result.community != 'no') {
                    community.push({ date: new Date(), text: result.community, id: chance.guid() })
                    fs.writeFile('../res/community.json', JSON.stringify(community), function(err) {
                        if (err) throw err;
                        console.log('Saved!');
                    });
                }
                console.log(result.community)
                increaseI()
                openBrowser(i)

            });
		});
	});
}

function increaseI() {
	i += 1
}

function openBrowser(i) {
	let uri = stories[i].url.substring(5, stories[i].url.length).match(/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/g).pop()
	console.log('uri', uri)
	driver.get(uri)
	readyUrl()
}

function readyBrowser() {
	prompt.get(['ready'], function(err, result) {
		//
		// Log the results.
		//
		if (result.ready == 'yes') {
			console.log('open browser')
			openBrowser(i)
		}
	});
}

function readyUrl() {
    let community = require('../res/community')
	prompt.get(['url'], function(err, result) {
		if(result.url == 'exit') {
		    driver.quit
		    console.log('goodbye')
		    setTimeout(function(){
	            		    process.exit()

		  	    },3000)
		}
		if (result.url == 'yes') {
			getUrl()
		} else {
            prompt.get(['community'], function(err, result) {
                if (result.community != 'no') {

                    community.push({ date: new Date(), text: result.community})
                    fs.writeFile('../res/community.json', JSON.stringify(community), function(err) {
                        if (err) throw err;
                        console.log('Saved!');
                    });
                }
                console.log(result.community)
                increaseI()
                openBrowser(i)

            });
		}
	})
}
let request = require('request'),
	cheerio = require('cheerio'),
	_ = require('underscore'),
	Word = require('../models/word'),
	mongoose = require("mongoose"),
	url = process.env.MONGODB_URI || "mongodb://localhost/buddy";

mongoose.connect(url);

try {
	request('https://www.cannainsider.com/reviews/glossary/', function(error, response, html) {
		if (!error && response.statusCode == 200) {
			let $ = cheerio.load(html);
			let target = $('.wpg-list-item')
			_.each(target, function(o) {
				let item = $(o)
				let url = item.find('.wpg-list-item-title').attr('href')
				request(url, function(error, response, html) {
					let $ = cheerio.load(html)('body');
					let word = new Word()
					word.title = $.find('.page-title').text().replace(/(\r\n|\n|\r)/gm, "")
					word.content = $.find('.blog-snippet').text().replace(/(\r\n|\n|\r)/gm, "")
					console.log(word)
					word.save()
				})
			})
		}
	});
} catch (e) {
	console.log(e)
}